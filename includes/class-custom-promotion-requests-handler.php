<?php

/**
 * Posts and Gets requests handler
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/admin
 */

/**
 * class custom promotion requests Handler .
 *
 * Handles Gets and Posts requests verify the legitimity of the requests and assigne each request
 * to the object or function that do the job.
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 * @author     Cumulus Team <mustapha@agencjacumulus.pl>
 */
class Custom_Promotion_Requests_Handler {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
    private $version;
    
    /**
	 * The admin display  of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $adminDisplayController    The admin display controller of this plugin.
	 */
    private $adminDisplayController;

	/**
	 * The data validator  of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $dataValidator    The data validator of this plugin.
	 */
    private $dataValidator;

	/**
	 * The database controller for admin of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbControllerAdmin    The database controller for admin of this plugin.
	 */
	private $dbControllerAdmin;
    

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $adminDisplayController , $dbControllerAdmin) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->adminDisplayController = $adminDisplayController;
		$this->dbControllerAdmin = $dbControllerAdmin;

		$this->load_dependencies();
		$this->dataValidator = new Data_Validator( $plugin_name, $version, $this->dbControllerAdmin );

	}
	
	private function load_dependencies() {

		/**
		 * The class responsable for validating the data.
		 */

        require_once plugin_dir_path( dirname( __FILE__ ) ) .  'includes/class-custom-promotion-data-validator.php';
	}

	/**
	 * Display the main page depending on the request given.
	 *
	 * @since    1.0.0
	 */

	public function setup_plugin_main_page() {

		//Add the menu to the Plugins set of menu items
		add_menu_page(
			'Promocje', 					// The title to be displayed in the browser window for this page.
			'Promocje',					// The text to be displayed for this menu item
			'manage_options',					// Which type of users can see this menu item
			'custom_promotion',			// The unique ID - that is, the slug - for this menu item
			array($this,'bundlerForRequest'), // The name of the function to call when rendering this menu's page
			'dashicons-store'				
		);

    }

	/**
	 * bunddle the request given by the client.
	 *
	 * @since    1.0.0
	 */

	 public function bundlerForRequest(){
		 # code...
		 $this->request_handler();
	 }

	/**
	 * Handle the request given by the client.
	 *
	 * @since    1.0.0
	 */

	private function request_handler(){
		$errors=''; 
		$success=''; 
		$tab='addNew';
		if (isset($_POST['validatesumpromotion'])) {
			if (isset($_POST['allPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['sumexceeded'])) {
					if ($this->dataValidator->checkPositiveNumber($_POST['sumexceeded'])) {
						if (isset($_POST['percentageCheckSum'])) {
							if ($this->dataValidator->checkPromotionPercentage($_POST['percentageCheckSum'])) {
								$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
								if (isset($_POST['startcheckSum']) && isset($_POST['endcheckSum'])) {
									$promotionCheckSumInput->dateStart=$_POST['startcheckSum'];
									$promotionCheckSumInput->dateEnd=$_POST['endcheckSum'];
								}else {
									$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
									$promotionCheckSumInput->dateStart=null;
									$promotionCheckSumInput->dateEnd=null;
								}
								$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
								$promotionCheckSumInput->sum=$_POST['sumexceeded']; // Number contains the minimum sum to have this promotion
								$promotionCheckSumInput->percentage=$_POST['percentageCheckSum']; // Number this is the percentage of the promotion deducted
								$promotionCheckSumInput->typePharmacySelected='All';
								$this->dbControllerAdmin->createCheckSumPromotion($promotionCheckSumInput);
								$success.='<br>'.__('You have succesfully added checkout promotion!','custom-promotion').'';
							}else {
								$errors.='<br>'.__('The persontage written is not valid','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('No persontage was written for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No sum was written for the promotion','custom-promotion').'.';
				}
			}elseif(isset($_POST['customPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'],$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['sumexceeded'])) {
					if ($this->dataValidator->checkPositiveNumber($_POST['sumexceeded'])) {
						if (isset($_POST['percentageCheckSum'])) {
							if ($this->dataValidator->checkPromotionPercentage($_POST['percentageCheckSum'])) {
								$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
								if (isset($_POST['startcheckSum']) && isset($_POST['endcheckSum'])) {
									$promotionCheckSumInput->dateStart=$_POST['startcheckSum'];
									$promotionCheckSumInput->dateEnd=$_POST['endcheckSum'];
								}else {
									$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
									$promotionCheckSumInput->dateStart=null;
									$promotionCheckSumInput->dateEnd=null;
								}
								$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
								$promotionCheckSumInput->sum=$_POST['sumexceeded']; // Number contains the minimum sum to have this promotion
								$promotionCheckSumInput->percentage=$_POST['percentageCheckSum']; // Number this is the percentage of the promotion deducted
								$promotionCheckSumInput->typePharmacySelected='Custom';
								$this->dbControllerAdmin->createCheckSumPromotion($promotionCheckSumInput);
								$success.='<br>'.__('You have succesfully added checkout promotion!','custom-promotion').'';
							}else {
								$errors.='<br>'.__('The persontage written is not valid','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('No persontage was written for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No sum was written for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['validatePromotionProduct'])) {
			if (isset($_POST['allPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['productsPurchased'])) {
					if (sizeof($_POST['productsPurchased'])>0) {
						if (isset($_POST['numberProductPurchased'])) {
								if ($this->dataValidator->checkPositiveNumber($_POST['numberProductPurchased'])) {
									if (isset($_POST['typePromotion'])) {
										if ($_POST['typePromotion']=='product') {
											if (isset($_POST['productGifted'])) {
												if (isset($_POST['promotionProductPurchased'])) {
													if ($this->dataValidator->checkPositiveNumber($_POST['promotionProductPurchased'])) {
														$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['startproductPurchased']) && isset($_POST['endproductPurchased'])) {
															$promotionPerProductInput->dateStart=$_POST['startproductPurchased'];
															$promotionPerProductInput->dateEnd=$_POST['endproductPurchased'];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerProductInput->dateStart=null;
															$promotionPerProductInput->dateEnd=null;
														}
														$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['productsPurchased'] as $key => $value) {
															$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerProductInput->typePromotion=$_POST['typePromotion']; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerProductInput->numberProducts=$_POST['numberProductPurchased']; //Number this is the number of products bought to activate this promotion
														$promotionPerProductInput->promotion=$_POST['promotionProductPurchased']; //Number (number of products given or percentage)
														$promotionPerProductInput->gift=$_POST['productGifted'];
														$promotionPerProductInput->typePharmacySelected='All';
														$this->dbControllerAdmin->createProductPromotion($promotionPerProductInput);
														$success.='<br>'.__('You have succesfully added promotion per product purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No gift product was selected','custom-promotion').'.';
											}
										}elseif ($_POST['typePromotion']=='percentage') {
											if (isset($_POST['promotionProductPurchased'])) {
												if ($this->dataValidator->checkPromotionPercentage($_POST['promotionProductPurchased'])) {
													$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
													if (isset($_POST['startproductPurchased']) && isset($_POST['endproductPurchased'])) {
														$promotionPerProductInput->dateStart=$_POST['startproductPurchased'];
														$promotionPerProductInput->dateEnd=$_POST['endproductPurchased'];
													}else {
														$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
														$promotionPerProductInput->dateStart=null;
														$promotionPerProductInput->dateEnd=null;
													}
													$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
													foreach ($_POST['productsPurchased'] as $key => $value) {
														$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
													}
													$promotionPerProductInput->typePromotion=$_POST['typePromotion']; // String 'product' or 'percentage' this attribute contains type of promotion given
													$promotionPerProductInput->numberProducts=$_POST['numberProductPurchased']; //Number this is the number of products bought to activate this promotion
													$promotionPerProductInput->promotion=$_POST['promotionProductPurchased']; //Number (number of products given or percentage)
													$promotionPerProductInput->typePharmacySelected='All';
													$promotionPerProductInput->gift='NULL';
													$this->dbControllerAdmin->createProductPromotion($promotionPerProductInput);
													$success.='<br>'.__('You have succesfully added promotion per product purchased!','custom-promotion').'';
												}else {
													$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
									}
								}else {
									$errors.='<br>'.__('The number of product purchased is invalid','custom-promotion').'.';
								}
						}else {
							$errors.='<br>'.__('No number of product purchased was set for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
				}
			}elseif(isset($_POST['customPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'],$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['productsPurchased'])) {
					if (sizeof($_POST['productsPurchased'])>0) {
						if (isset($_POST['numberProductPurchased'])) {
								if ($this->dataValidator->checkPositiveNumber($_POST['numberProductPurchased'])) {
									if (isset($_POST['typePromotion'])) {
										if ($_POST['typePromotion']=='product') {
											if (isset($_POST['productGifted'])) {
												
												if (isset($_POST['promotionProductPurchased'])) {
													if ($this->dataValidator->checkPositiveNumber($_POST['promotionProductPurchased'])) {
														$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['startproductPurchased']) && isset($_POST['endproductPurchased'])) {
															$promotionPerProductInput->dateStart=$_POST['startproductPurchased'];
															$promotionPerProductInput->dateEnd=$_POST['endproductPurchased'];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerProductInput->dateStart=null;
															$promotionPerProductInput->dateEnd=null;
														}
														$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['productsPurchased'] as $key => $value) {
															$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerProductInput->typePromotion=$_POST['typePromotion']; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerProductInput->numberProducts=$_POST['numberProductPurchased']; //Number this is the number of products bought to activate this promotion
														$promotionPerProductInput->promotion=$_POST['promotionProductPurchased']; //Number (number of products given or percentage)
														$promotionPerProductInput->gift=$_POST['productGifted'];
														$promotionPerProductInput->typePharmacySelected='Custom';
														$this->dbControllerAdmin->createProductPromotion($promotionPerProductInput);
														$success.='<br>'.__('You have succesfully added promotion per product purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No gift product was selected','custom-promotion').'.';
											}
										}elseif ($_POST['typePromotion']=='percentage') {
											if (isset($_POST['promotionProductPurchased'])) {
												if ($this->dataValidator->checkPromotionPercentage($_POST['promotionProductPurchased'])) {
													$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
													if (isset($_POST['startproductPurchased']) && isset($_POST['endproductPurchased'])) {
														$promotionPerProductInput->dateStart=$_POST['startproductPurchased'];
														$promotionPerProductInput->dateEnd=$_POST['endproductPurchased'];
													}else {
														$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
														$promotionPerProductInput->dateStart=null;
														$promotionPerProductInput->dateEnd=null;
													}
													$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
													foreach ($_POST['productsPurchased'] as $key => $value) {
														$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
													}
													$promotionPerProductInput->typePromotion=$_POST['typePromotion']; // String 'product' or 'percentage' this attribute contains type of promotion given
													$promotionPerProductInput->numberProducts=$_POST['numberProductPurchased']; //Number this is the number of products bought to activate this promotion
													$promotionPerProductInput->promotion=$_POST['promotionProductPurchased']; //Number (number of products given or percentage)
													$promotionPerProductInput->typePharmacySelected='Custom';
													$promotionPerProductInput->gift='NULL';
													$this->dbControllerAdmin->createProductPromotion($promotionPerProductInput);
													$success.='<br>'.__('You have succesfully added promotion per product purchased!','custom-promotion').'';
												}else {
													$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
									}
								}else {
									$errors.='<br>'.__('The number of product purchased is invalid','custom-promotion').'.';
								}
						}else {
							$errors.='<br>'.__('No number of product purchased was set for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['validatePromotionCategory'])) {
			if (isset($_POST['allPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['categoriesPurchased'])) {
					if (sizeof($_POST['categoriesPurchased'])>0) {
						if (isset($_POST['numberCategoryPurchased'])) {
								if ($this->dataValidator->checkPositiveNumber($_POST['numberCategoryPurchased'])) {
									if (isset($_POST['typePromotionCategory'])) {
										if ($_POST['typePromotionCategory']=='product') {
											if (isset($_POST['productGiftedCategory'])) {
												if (isset($_POST['promotionCategoryPurchased'])) {
													if ($this->dataValidator->checkPositiveNumber($_POST['promotionCategoryPurchased'])) {
														$promotionPerCategoryInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['startcategoryPurchased']) && isset($_POST['endcategoryPurchased'])) {
															$promotionPerCategoryInput->dateStart=$_POST['startcategoryPurchased'];
															$promotionPerCategoryInput->dateEnd=$_POST['endcategoryPurchased'];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerCategoryInput->dateStart=null;
															$promotionPerCategoryInput->dateEnd=null;
														}
														$promotionPerCategoryInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['categoriesPurchased'] as $key => $value) {
															$promotionPerCategoryInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerCategoryInput->typePromotion=$_POST['typePromotionCategory']; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerCategoryInput->numberCategories=$_POST['numberCategoryPurchased']; //Number this is the number of products bought to activate this promotion
														$promotionPerCategoryInput->promotion=$_POST['promotionCategoryPurchased']; //Number (number of products given or percentage)
														$promotionPerCategoryInput->gift=$_POST['productGiftedCategory'];
														$promotionPerCategoryInput->typePharmacySelected='All';
														$this->dbControllerAdmin->createCategoryPromotion($promotionPerCategoryInput);
														$success.='<br>'.__('You have succesfully added promotion for products per category purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product per category given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product per category was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No gift product per category was set','custom-promotion').'.';
											}
										}elseif ($_POST['typePromotionCategory']=='percentage') {
											if (isset($_POST['promotionCategoryPurchased'])) {
												if ($this->dataValidator->checkPromotionPercentage($_POST['promotionCategoryPurchased'])) {
													$promotionPerCategoryInput = new \stdClass; //This object contains the data of promotion per product
													if (isset($_POST['startcategoryPurchased']) && isset($_POST['endcategoryPurchased'])) {
														$promotionPerCategoryInput->dateStart=$_POST['startcategoryPurchased'];
														$promotionPerCategoryInput->dateEnd=$_POST['endcategoryPurchased'];
													}else {
														$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
														$promotionPerCategoryInput->dateStart=null;
														$promotionPerCategoryInput->dateEnd=null;
													}
													$promotionPerCategoryInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
													foreach ($_POST['categoriesPurchased'] as $key => $value) {
														$promotionPerCategoryInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
													}
													$promotionPerCategoryInput->typePromotion=$_POST['typePromotionCategory']; // String 'product' or 'percentage' this attribute contains type of promotion given
													$promotionPerCategoryInput->numberCategories=$_POST['numberCategoryPurchased']; //Number this is the number of products bought to activate this promotion
													$promotionPerCategoryInput->promotion=$_POST['promotionCategoryPurchased']; //Number (number of products given or percentage)
													$promotionPerCategoryInput->typePharmacySelected='All';
													$promotionPerCategoryInput->gift='NULL';
													$this->dbControllerAdmin->createCategoryPromotion($promotionPerCategoryInput);
													$success.='<br>'.__('You have succesfully added promotion for products per category purchased!','custom-promotion').'';
												}else {
													$errors.='<br>'.__('The number of product per category given for free is invalid','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No number of free product per category was set','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
									}
								}else {
									$errors.='<br>'.__('The number of product per category purchased is invalid','custom-promotion').'.';
								}
						}else {
							$errors.='<br>'.__('No number of product per category purchased was set for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No category was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No category was selected for the promotion','custom-promotion').'.';
				}
			}elseif(isset($_POST['customPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'],$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['categoriesPurchased'])) {
					if (sizeof($_POST['categoriesPurchased'])>0) {
						if (isset($_POST['numberCategoryPurchased'])) {
								if ($this->dataValidator->checkPositiveNumber($_POST['numberCategoryPurchased'])) {
									if (isset($_POST['typePromotionCategory'])) {
										if ($_POST['typePromotionCategory']=='product') {
											if (isset($_POST['productGiftedCategory'])) {
												if (isset($_POST['promotionCategoryPurchased'])) {
													if ($this->dataValidator->checkPositiveNumber($_POST['promotionCategoryPurchased'])) {
														$promotionPerCategoryInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['startcategoryPurchased']) && isset($_POST['endcategoryPurchased'])) {
															$promotionPerCategoryInput->dateStart=$_POST['startcategoryPurchased'];
															$promotionPerCategoryInput->dateEnd=$_POST['endcategoryPurchased'];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerCategoryInput->dateStart=null;
															$promotionPerCategoryInput->dateEnd=null;
														}
														$promotionPerCategoryInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['categoriesPurchased'] as $key => $value) {
															$promotionPerCategoryInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerCategoryInput->typePromotion=$_POST['typePromotionCategory']; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerCategoryInput->numberCategories=$_POST['numberCategoryPurchased']; //Number this is the number of products bought to activate this promotion
														$promotionPerCategoryInput->promotion=$_POST['promotionCategoryPurchased']; //Number (number of products given or percentage)
														$promotionPerCategoryInput->gift=$_POST['productGiftedCategory'];
														$promotionPerCategoryInput->typePharmacySelected='Custom';
														$this->dbControllerAdmin->createCategoryPromotion($promotionPerCategoryInput);
														$success.='<br>'.__('You have succesfully added promotion for products per category purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product per category given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product per category was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No gift product per category was set','custom-promotion').'.';
											}
										}elseif ($_POST['typePromotionCategory']=='percentage') {
											if (isset($_POST['promotionCategoryPurchased'])) {
												if ($this->dataValidator->checkPromotionPercentage($_POST['promotionCategoryPurchased'])) {
													$promotionPerCategoryInput = new \stdClass; //This object contains the data of promotion per product
													if (isset($_POST['startcategoryPurchased']) && isset($_POST['endcategoryPurchased'])) {
														$promotionPerCategoryInput->dateStart=$_POST['startcategoryPurchased'];
														$promotionPerCategoryInput->dateEnd=$_POST['endcategoryPurchased'];
													}else {
														$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
														$promotionPerCategoryInput->dateStart=null;
														$promotionPerCategoryInput->dateEnd=null;
													}
													$promotionPerCategoryInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
													foreach ($_POST['categoriesPurchased'] as $key => $value) {
														$promotionPerCategoryInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
													}
													$promotionPerCategoryInput->typePromotion=$_POST['typePromotionCategory']; // String 'product' or 'percentage' this attribute contains type of promotion given
													$promotionPerCategoryInput->numberCategories=$_POST['numberCategoryPurchased']; //Number this is the number of products bought to activate this promotion
													$promotionPerCategoryInput->promotion=$_POST['promotionCategoryPurchased']; //Number (number of products given or percentage)
													$promotionPerCategoryInput->typePharmacySelected='Custom';
													$promotionPerCategoryInput->gift='NULL';
													$this->dbControllerAdmin->createCategoryPromotion($promotionPerCategoryInput);
													$success.='<br>'.__('You have succesfully added promotion for products per category purchased!','custom-promotion').'';
												}else {
													$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
									}
								}else {
									$errors.='<br>'.__('The number of product per category purchased is invalid','custom-promotion').'.';
								}
						}else {
							$errors.='<br>'.__('No number of product per category purchased was set for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No category was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No category was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['validateListPrices'])) {
			if (isset($_POST['allPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_FILES['listPrices'])) {
					if ($this->dataValidator->checkValidityOfCsvFile($_FILES['listPrices'])) {
						$listPricesPerPharmacyInput = new \stdClass; //This object contains the list of new prices for pharmacies
						if (isset($_POST['startlistPrices']) && isset($_POST['endlistPrices'])) {
							$listPricesPerPharmacyInput->dateStart=$_POST['startlistPrices'];
							$listPricesPerPharmacyInput->dateEnd=$_POST['endlistPrices'];
						}else {
							$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
							$listPricesPerPharmacyInput->dateStart=null;
							$listPricesPerPharmacyInput->dateEnd=null;
						}
						$listPricesPerPharmacyInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
						$listPricesPerPharmacyInput->file=$_FILES['listPrices']; // CSV file contain list of pharmacies
						$listPricesPerPharmacyInput->typePharmacySelected='All';
						$this->dbControllerAdmin->createlistPricesPromotion($listPricesPerPharmacyInput);
						$success.='<br>'.__('You have succesfully added list of prices per pharmacy!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('The file selected is invalid','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No file was uploaded for the promotion','custom-promotion').'.';
				}
			}elseif(isset($_POST['customPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'],$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_FILES['listPrices'])) {
					if ($this->dataValidator->checkValidityOfCsvFile($_FILES['listPrices'])) {
						$listPricesPerPharmacyInput = new \stdClass; //This object contains the list of new prices for pharmacies
						if (isset($_POST['startlistPrices']) && isset($_POST['endlistPrices'])) {
							$listPricesPerPharmacyInput->dateStart=$_POST['startlistPrices'];
							$listPricesPerPharmacyInput->dateEnd=$_POST['endlistPrices'];
						}else {
							$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
							$listPricesPerPharmacyInput->dateStart=null;
							$listPricesPerPharmacyInput->dateEnd=null;
						}
						$listPricesPerPharmacyInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
						$listPricesPerPharmacyInput->file=$_FILES['listPrices']; // CSV file contain list of pharmacies
						$listPricesPerPharmacyInput->typePharmacySelected='Custom';
						$this->dbControllerAdmin->createlistPricesPromotion($listPricesPerPharmacyInput);
						$success.='<br>'.__('You have succesfully added list of prices per pharmacy!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('The file selected is invalid','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No file was uploaded for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['validateAllPromotion'])) {
			if (isset($_POST['allPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['sumexceeded'])) {
					if ($this->dataValidator->checkPositiveNumber($_POST['sumexceeded'])) {
						if (isset($_POST['percentageCheckSum'])) {
							if ($this->dataValidator->checkPromotionPercentage($_POST['percentageCheckSum'])) {
								$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
								if (isset($_POST['startcheckSum']) && isset($_POST['endcheckSum'])) {
									$promotionCheckSumInput->dateStart=$_POST['startcheckSum'];
									$promotionCheckSumInput->dateEnd=$_POST['endcheckSum'];
								}else {
									$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
									$promotionCheckSumInput->dateStart=null;
									$promotionCheckSumInput->dateEnd=null;
								}
								$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
								$promotionCheckSumInput->sum=$_POST['sumexceeded']; // Number contains the minimum sum to have this promotion
								$promotionCheckSumInput->percentage=$_POST['percentageCheckSum']; // Number this is the percentage of the promotion deducted
								$promotionCheckSumInput->typePharmacySelected='All';
								$this->dbControllerAdmin->createCheckSumPromotion($promotionCheckSumInput);
								$success.='<br>'.__('You have succesfully added checkout promotion!','custom-promotion').'';
							}else {
								$errors.='<br>'.__('The persontage written is not valid','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('No persontage was written for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
					}
				}
				if (isset($_POST['productsPurchased'])) {
					if (sizeof($_POST['productsPurchased'])>0) {
						if (isset($_POST['numberProductPurchased'])) {
								if ($this->dataValidator->checkPositiveNumber($_POST['numberProductPurchased'])) {
									if (isset($_POST['typePromotion'])) {
										if ($_POST['typePromotion']=='product') {
											if (isset($_POST['productGifted'])) {
												if (isset($_POST['promotionProductPurchased'])) {
													if ($this->dataValidator->checkPositiveNumber($_POST['promotionProductPurchased'])) {
														$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['startproductPurchased']) && isset($_POST['endproductPurchased'])) {
															$promotionPerProductInput->dateStart=$_POST['startproductPurchased'];
															$promotionPerProductInput->dateEnd=$_POST['endproductPurchased'];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerProductInput->dateStart=null;
															$promotionPerProductInput->dateEnd=null;
														}
														$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['productsPurchased'] as $key => $value) {
															$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerProductInput->typePromotion=$_POST['typePromotion']; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerProductInput->numberProducts=$_POST['numberProductPurchased']; //Number this is the number of products bought to activate this promotion
														$promotionPerProductInput->promotion=$_POST['promotionProductPurchased']; //Number (number of products given or percentage)
														$promotionPerProductInput->gift=$_POST['productGifted'];
														$promotionPerProductInput->typePharmacySelected='All';
														$this->dbControllerAdmin->createProductPromotion($promotionPerProductInput);
														$success.='<br>'.__('You have succesfully added promotion per product purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No gift product was selected','custom-promotion').'.';
											}
										}elseif ($_POST['typePromotion']=='percentage') {
											if (isset($_POST['promotionProductPurchased'])) {
												if ($this->dataValidator->checkPromotionPercentage($_POST['promotionProductPurchased'])) {
													$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
													$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
													foreach ($_POST['productsPurchased'] as $key => $value) {
														$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
													}
													$promotionPerProductInput->typePromotion=$_POST['typePromotion']; // String 'product' or 'percentage' this attribute contains type of promotion given
													$promotionPerProductInput->numberProducts=$_POST['numberProductPurchased']; //Number this is the number of products bought to activate this promotion
													$promotionPerProductInput->promotion=$_POST['promotionProductPurchased']; //Number (number of products given or percentage)
													$promotionPerProductInput->typePharmacySelected='All';
													$promotionPerProductInput->gift='NULL';
													$this->dbControllerAdmin->createProductPromotion($promotionPerProductInput);
													$success.='<br>'.__('You have succesfully added promotion per product purchased!','custom-promotion').'';
												}else {
													$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
									}
								}else {
									$errors.='<br>'.__('The number of product purchased is invalid','custom-promotion').'.';
								}
						}else {
							$errors.='<br>'.__('No number of product purchased was set for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}
				if (isset($_POST['categoriesPurchased'])) {
					if (sizeof($_POST['categoriesPurchased'])>0) {
						if (isset($_POST['numberCategoryPurchased'])) {
								if ($this->dataValidator->checkPositiveNumber($_POST['numberCategoryPurchased'])) {
									if (isset($_POST['typePromotionCategory'])) {
										if ($_POST['typePromotionCategory']=='product') {
											if (isset($_POST['productGiftedCategory'])) {
												if (isset($_POST['promotionCategoryPurchased'])) {
													if ($this->dataValidator->checkPositiveNumber($_POST['promotionCategoryPurchased'])) {
														$promotionPerCategoryInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['startcategoryPurchased']) && isset($_POST['endcategoryPurchased'])) {
															$promotionPerCategoryInput->dateStart=$_POST['startcategoryPurchased'];
															$promotionPerCategoryInput->dateEnd=$_POST['endcategoryPurchased'];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerCategoryInput->dateStart=null;
															$promotionPerCategoryInput->dateEnd=null;
														}
														$promotionPerCategoryInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['categoriesPurchased'] as $key => $value) {
															$promotionPerCategoryInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerCategoryInput->typePromotion=$_POST['typePromotionCategory']; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerCategoryInput->numberCategories=$_POST['numberCategoryPurchased']; //Number this is the number of products bought to activate this promotion
														$promotionPerCategoryInput->promotion=$_POST['promotionCategoryPurchased']; //Number (number of products given or percentage)
														$promotionPerCategoryInput->gift=$_POST['productGiftedCategory'];
														$promotionPerCategoryInput->typePharmacySelected='All';
														$this->dbControllerAdmin->createCategoryPromotion($promotionPerCategoryInput);
														$success.='<br>'.__('You have succesfully added promotion for products per category purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product per category given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product per category was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No gift product per category was set','custom-promotion').'.';
											}
										}elseif ($_POST['typePromotionCategory']=='percentage') {
											if (isset($_POST['promotionCategoryPurchased'])) {
												if ($this->dataValidator->checkPromotionPercentage($_POST['promotionCategoryPurchased'])) {
													$promotionPerCategoryInput = new \stdClass; //This object contains the data of promotion per product
													if (isset($_POST['startcategoryPurchased']) && isset($_POST['endcategoryPurchased'])) {
														$promotionPerCategoryInput->dateStart=$_POST['startcategoryPurchased'];
														$promotionPerCategoryInput->dateEnd=$_POST['endcategoryPurchased'];
													}else {
														$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
														$promotionPerCategoryInput->dateStart=null;
														$promotionPerCategoryInput->dateEnd=null;
													}
													$promotionPerCategoryInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
													foreach ($_POST['categoriesPurchased'] as $key => $value) {
														$promotionPerCategoryInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
													}
													$promotionPerCategoryInput->typePromotion=$_POST['typePromotionCategory']; // String 'product' or 'percentage' this attribute contains type of promotion given
													$promotionPerCategoryInput->numberCategories=$_POST['numberCategoryPurchased']; //Number this is the number of products bought to activate this promotion
													$promotionPerCategoryInput->promotion=$_POST['promotionCategoryPurchased']; //Number (number of products given or percentage)
													$promotionPerCategoryInput->typePharmacySelected='All';
													$promotionPerCategoryInput->gift='NULL';
													$this->dbControllerAdmin->createCategoryPromotion($promotionPerCategoryInput);
													$success.='<br>'.__('You have succesfully added promotion for products per category purchased!','custom-promotion').'';
												}else {
													$errors.='<br>'.__('The number of product per category given for free is invalid','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No number of free product per category was set','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
									}
								}else {
									$errors.='<br>'.__('The number of product per category purchased is invalid','custom-promotion').'.';
								}
						}else {
							$errors.='<br>'.__('No number of product per category purchased was set for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No category was selected for the promotion','custom-promotion').'.';
					}
				}
				if (isset($_FILES['listPrices'])) {
					if ($this->dataValidator->checkValidityOfCsvFile($_FILES['listPrices'])) {
						$listPricesPerPharmacyInput = new \stdClass; //This object contains the list of new prices for pharmacies
						if (isset($_POST['startlistPrices']) && isset($_POST['endlistPrices'])) {
							$listPricesPerPharmacyInput->dateStart=$_POST['startlistPrices'];
							$listPricesPerPharmacyInput->dateEnd=$_POST['endlistPrices'];
						}else {
							$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
							$listPricesPerPharmacyInput->dateStart=null;
							$listPricesPerPharmacyInput->dateEnd=null;
						}
						$listPricesPerPharmacyInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
						$listPricesPerPharmacyInput->file=$_FILES['listPrices']; // CSV file contain list of pharmacies
						$listPricesPerPharmacyInput->typePharmacySelected='All';
						$this->dbControllerAdmin->createlistPricesPromotion($listPricesPerPharmacyInput);
						$success.='<br>'.__('You have succesfully added list of prices per pharmacy!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('The file selected is invalid','custom-promotion').'.';
					}
				}
			}elseif(isset($_POST['customPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'],$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['sumexceeded'])) {
					if ($this->dataValidator->checkPositiveNumber($_POST['sumexceeded'])) {
						if (isset($_POST['percentageCheckSum'])) {
							if ($this->dataValidator->checkPromotionPercentage($_POST['percentageCheckSum'])) {
								$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
								if (isset($_POST['startcheckSum']) && isset($_POST['endcheckSum'])) {
									$promotionCheckSumInput->dateStart=$_POST['startcheckSum'];
									$promotionCheckSumInput->dateEnd=$_POST['endcheckSum'];
								}else {
									$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
									$promotionCheckSumInput->dateStart=null;
									$promotionCheckSumInput->dateEnd=null;
								}
								$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
								$promotionCheckSumInput->sum=$_POST['sumexceeded']; // Number contains the minimum sum to have this promotion
								$promotionCheckSumInput->percentage=$_POST['percentageCheckSum']; // Number this is the percentage of the promotion deducted
								$promotionCheckSumInput->typePharmacySelected='Custom';
								$this->dbControllerAdmin->createCheckSumPromotion($promotionCheckSumInput);
								$success.='<br>'.__('You have succesfully added checkout promotion!','custom-promotion').'';
							}else {
								$errors.='<br>'.__('The persontage written is not valid','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('No persontage was written for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
					}
				}
				if (isset($_POST['productsPurchased'])) {
					if (sizeof($_POST['productsPurchased'])>0) {
						if (isset($_POST['numberProductPurchased'])) {
								if ($this->dataValidator->checkPositiveNumber($_POST['numberProductPurchased'])) {
									if (isset($_POST['typePromotion'])) {
										if ($_POST['typePromotion']=='product') {
											if (isset($_POST['productGifted'])) {
												if (isset($_POST['promotionProductPurchased'])) {
													if ($this->dataValidator->checkPositiveNumber($_POST['promotionProductPurchased'])) {
														$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['startproductPurchased']) && isset($_POST['endproductPurchased'])) {
															$promotionPerProductInput->dateStart=$_POST['startproductPurchased'];
															$promotionPerProductInput->dateEnd=$_POST['endproductPurchased'];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerProductInput->dateStart=null;
															$promotionPerProductInput->dateEnd=null;
														}
														$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['productsPurchased'] as $key => $value) {
															$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerProductInput->typePromotion=$_POST['typePromotion']; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerProductInput->numberProducts=$_POST['numberProductPurchased']; //Number this is the number of products bought to activate this promotion
														$promotionPerProductInput->promotion=$_POST['promotionProductPurchased']; //Number (number of products given or percentage)
														$promotionPerProductInput->gift=$_POST['productGifted'];
														$promotionPerProductInput->typePharmacySelected='Custom';
														$this->dbControllerAdmin->createProductPromotion($promotionPerProductInput);
														$success.='<br>'.__('You have succesfully added promotion per product purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No gift product was selected','custom-promotion').'.';
											}
										}elseif ($_POST['typePromotion']=='percentage') {
											if (isset($_POST['promotionProductPurchased'])) {
												if ($this->dataValidator->checkPromotionPercentage($_POST['promotionProductPurchased'])) {
													$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
													$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
													foreach ($_POST['productsPurchased'] as $key => $value) {
														$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
													}
													$promotionPerProductInput->typePromotion=$_POST['typePromotion']; // String 'product' or 'percentage' this attribute contains type of promotion given
													$promotionPerProductInput->numberProducts=$_POST['numberProductPurchased']; //Number this is the number of products bought to activate this promotion
													$promotionPerProductInput->promotion=$_POST['promotionProductPurchased']; //Number (number of products given or percentage)
													$promotionPerProductInput->typePharmacySelected='Custom';
													$promotionPerProductInput->gift='NULL';
													$this->dbControllerAdmin->createProductPromotion($promotionPerProductInput);
													$success.='<br>'.__('You have succesfully added promotion per product purchased!','custom-promotion').'';
												}else {
													$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
									}
								}else {
									$errors.='<br>'.__('The number of product purchased is invalid','custom-promotion').'.';
								}
						}else {
							$errors.='<br>'.__('No number of product purchased was set for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}
				if (isset($_POST['categoriesPurchased'])) {
					if (sizeof($_POST['categoriesPurchased'])>0) {
						if (isset($_POST['numberCategoryPurchased'])) {
								if ($this->dataValidator->checkPositiveNumber($_POST['numberCategoryPurchased'])) {
									if (isset($_POST['typePromotionCategory'])) {
										if ($_POST['typePromotionCategory']=='product') {
											if (isset($_POST['productGiftedCategory'])) {
												if (isset($_POST['promotionCategoryPurchased'])) {
													if ($this->dataValidator->checkPositiveNumber($_POST['promotionCategoryPurchased'])) {
														$promotionPerCategoryInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['startcategoryPurchased']) && isset($_POST['endcategoryPurchased'])) {
															$promotionPerCategoryInput->dateStart=$_POST['startcategoryPurchased'];
															$promotionPerCategoryInput->dateEnd=$_POST['endcategoryPurchased'];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerCategoryInput->dateStart=null;
															$promotionPerCategoryInput->dateEnd=null;
														}
														$promotionPerCategoryInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['categoriesPurchased'] as $key => $value) {
															$promotionPerCategoryInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerCategoryInput->typePromotion=$_POST['typePromotionCategory']; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerCategoryInput->numberCategories=$_POST['numberCategoryPurchased']; //Number this is the number of products bought to activate this promotion
														$promotionPerCategoryInput->promotion=$_POST['promotionCategoryPurchased']; //Number (number of products given or percentage)
														$promotionPerCategoryInput->gift=$_POST['productGiftedCategory'];
														$promotionPerCategoryInput->typePharmacySelected='Custom';
														$this->dbControllerAdmin->createCategoryPromotion($promotionPerCategoryInput);
														$success.='<br>'.__('You have succesfully added promotion for products per category purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product per category given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product per category was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No gift product per category was set','custom-promotion').'.';
											}
										}elseif ($_POST['typePromotionCategory']=='percentage') {
											if (isset($_POST['promotionCategoryPurchased'])) {
												if ($this->dataValidator->checkPromotionPercentage($_POST['promotionCategoryPurchased'])) {
													$promotionPerCategoryInput = new \stdClass; //This object contains the data of promotion per product
													if (isset($_POST['startcategoryPurchased']) && isset($_POST['endcategoryPurchased'])) {
														$promotionPerCategoryInput->dateStart=$_POST['startcategoryPurchased'];
														$promotionPerCategoryInput->dateEnd=$_POST['endcategoryPurchased'];
													}else {
														$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
														$promotionPerCategoryInput->dateStart=null;
														$promotionPerCategoryInput->dateEnd=null;
													}
													$promotionPerCategoryInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
													foreach ($_POST['categoriesPurchased'] as $key => $value) {
														$promotionPerCategoryInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
													}
													$promotionPerCategoryInput->typePromotion=$_POST['typePromotionCategory']; // String 'product' or 'percentage' this attribute contains type of promotion given
													$promotionPerCategoryInput->numberCategories=$_POST['numberCategoryPurchased']; //Number this is the number of products bought to activate this promotion
													$promotionPerCategoryInput->promotion=$_POST['promotionCategoryPurchased']; //Number (number of products given or percentage)
													$promotionPerCategoryInput->typePharmacySelected='Custom';
													$promotionPerCategoryInput->gift='NULL';
													$this->dbControllerAdmin->createCategoryPromotion($promotionPerCategoryInput);
													$success.='<br>'.__('You have succesfully added promotion for products per category purchased!','custom-promotion').'';
												}else {
													$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
									}
								}else {
									$errors.='<br>'.__('The number of product per category purchased is invalid','custom-promotion').'.';
								}
						}else {
							$errors.='<br>'.__('No number of product per category purchased was set for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No category was selected for the promotion','custom-promotion').'.';
					}
				} 
				if (isset($_FILES['listPrices'])) {
					if ($this->dataValidator->checkValidityOfCsvFile($_FILES['listPrices'])) {
						$listPricesPerPharmacyInput = new \stdClass; //This object contains the list of new prices for pharmacies
						if (isset($_POST['startlistPrices']) && isset($_POST['endlistPrices'])) {
							$listPricesPerPharmacyInput->dateStart=$_POST['startlistPrices'];
							$listPricesPerPharmacyInput->dateEnd=$_POST['endlistPrices'];
						}else {
							$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
							$listPricesPerPharmacyInput->dateStart=null;
							$listPricesPerPharmacyInput->dateEnd=null;
						}
						$listPricesPerPharmacyInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
						$listPricesPerPharmacyInput->file=$_FILES['listPrices']; // CSV file contain list of pharmacies
						$listPricesPerPharmacyInput->typePharmacySelected='Custom';
						$this->dbControllerAdmin->createlistPricesPromotion($listPricesPerPharmacyInput);
						$success.='<br>'.__('You have succesfully added list of prices per pharmacy!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('The file selected is invalid','custom-promotion').'.';
					}
				}
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['editsumpromotion'])) {
			$tab='listPromotion';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['editsumpromotion'] )) {
				if (isset($_POST['allPharmaciesedit'.$_POST['editsumpromotion']])) {
					$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['sumexceeded'.$_POST['editsumpromotion']])) {
						if ($this->dataValidator->checkPositiveNumber($_POST['sumexceeded'.$_POST['editsumpromotion']])) {
							if (isset($_POST['percentage'.$_POST['editsumpromotion']])) {
								if ($this->dataValidator->checkPromotionPercentage($_POST['percentage'.$_POST['editsumpromotion']])) {
									$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
									if (isset($_POST['starteditCheckSum'.$_POST['editsumpromotion']]) && isset($_POST['endeditCheckSum'.$_POST['editsumpromotion']])) {
										$promotionCheckSumInput->dateStart=$_POST['starteditCheckSum'.$_POST['editsumpromotion']];
										$promotionCheckSumInput->dateEnd=$_POST['endeditCheckSum'.$_POST['editsumpromotion']];
									}else {
										$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
										$promotionCheckSumInput->dateStart=null;
										$promotionCheckSumInput->dateEnd=null;
									}
									$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
									$promotionCheckSumInput->sum=$_POST['sumexceeded'.$_POST['editsumpromotion']]; // Number contains the minimum sum to have this promotion
									$promotionCheckSumInput->percentage=$_POST['percentage'.$_POST['editsumpromotion']]; // Number this is the percentage of the promotion deducted
									$promotionCheckSumInput->ID=$_POST['editsumpromotion'];
									$promotionCheckSumInput->typePharmacySelected='All';
									$this->dbControllerAdmin->updateCheckSumPromotion($promotionCheckSumInput);
									$success.='<br>'.__('You have succesfully Edited the checkout promotion!','custom-promotion').'';
								}else {
									$errors.='<br>'.__('The percentage written is not valid','custom-promotion').'.';
								}
							}else {
								$errors.='<br>'.__('No persontage was written for the promotion','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No promotion sum was selected for this promotion','custom-promotion').'.';
					}
				}elseif(isset($_POST['customPharmacies'.$_POST['editsumpromotion']])) {
					$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'.$_POST['editsumpromotion']],$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['sumexceeded'.$_POST['editsumpromotion']])) {
						if ($this->dataValidator->checkPositiveNumber($_POST['sumexceeded'.$_POST['editsumpromotion']])) {
							if (isset($_POST['percentage'.$_POST['editsumpromotion']])) {
								if ($this->dataValidator->checkPromotionPercentage($_POST['percentage'.$_POST['editsumpromotion']])) {
									$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
									if (isset($_POST['starteditCheckSum'.$_POST['editsumpromotion']]) && isset($_POST['endeditCheckSum'.$_POST['editsumpromotion']])) {
										$promotionCheckSumInput->dateStart=$_POST['starteditCheckSum'.$_POST['editsumpromotion']];
										$promotionCheckSumInput->dateEnd=$_POST['endeditCheckSum'.$_POST['editsumpromotion']];
									}else {
										$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
										$promotionCheckSumInput->dateStart=null;
										$promotionCheckSumInput->dateEnd=null;
									}
									$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
									$promotionCheckSumInput->sum=$_POST['sumexceeded'.$_POST['editsumpromotion']]; // Number contains the minimum sum to have this promotion
									$promotionCheckSumInput->percentage=$_POST['percentage'.$_POST['editsumpromotion']]; // Number this is the percentage of the promotion deducted
									$promotionCheckSumInput->ID=$_POST['editsumpromotion'];
									$promotionCheckSumInput->typePharmacySelected='Custom';
									$this->dbControllerAdmin->updateCheckSumPromotion($promotionCheckSumInput);
									$success.='<br>'.__('You have succesfully Edited the checkout promotion!','custom-promotion').'';
								}else {
									$errors.='<br>'.__('The percentage written is not valid','custom-promotion').'.';
								}
							}else {
								$errors.='<br>'.__('No persontage was written for the promotion','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No promotion sum was selected for this promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('The promotion ID is not valid','custom-promotion').'.';
			}	
		}elseif (isset($_POST['deletesumpromotion'])) {
			$tab='listPromotion';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['deletesumpromotion'] )) {
				$this->dbControllerAdmin->deletePromotion($_POST['deletesumpromotion']);
				$success.='<br>'.__('You have succesfully deleted the checkout promotion!','custom-promotion').'';
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['editlistpricespromotion'])) {
			$tab='listPromotion';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['editlistpricespromotion'] )) {
				if (isset($_POST['allPharmaciesedit'.$_POST['editlistpricespromotion']])) {
					$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_FILES['listPrices'.$_POST['editlistpricespromotion']])) {
						if ($this->dataValidator->checkValidityOfCsvFile($_FILES['listPrices'.$_POST['editlistpricespromotion']])) {
							$listPricesPerPharmacyInput = new \stdClass; //This object contains the list of new prices for pharmacies
							if (isset($_POST['starteditListPrices'.$_POST['editlistpricespromotion']]) && isset($_POST['endeditListPrices'.$_POST['editlistpricespromotion']])) {
								$listPricesPerPharmacyInput->dateStart=$_POST['starteditListPrices'.$_POST['editlistpricespromotion']];
								$listPricesPerPharmacyInput->dateEnd=$_POST['endeditListPrices'.$_POST['editlistpricespromotion']];
							}else {
								$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
								$listPricesPerPharmacyInput->dateStart=null;
								$listPricesPerPharmacyInput->dateEnd=null;
							}
							$listPricesPerPharmacyInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
							$listPricesPerPharmacyInput->file=$_FILES['listPrices'.$_POST['editlistpricespromotion']]; // CSV file contain list of pharmacies
							$listPricesPerPharmacyInput->ID=$_POST['editlistpricespromotion'];
							$listPricesPerPharmacyInput->typePharmacySelected='All';
							$this->dbControllerAdmin->updatelistPricesPromotion($listPricesPerPharmacyInput);
							$success.='<br>'.__('You have succesfully edited the list of prices per pharmacy!','custom-promotion').'';
						}else {
							$errors.='<br>'.__('The file selected is invalid','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No file was uploaded for the promotion','custom-promotion').'.';
					}
				}elseif(isset($_POST['customPharmacies'.$_POST['editlistpricespromotion']])) {
					$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'.$_POST['editlistpricespromotion']],$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_FILES['listPrices'.$_POST['editlistpricespromotion']])) {
						if ($this->dataValidator->checkValidityOfCsvFile($_FILES['listPrices'.$_POST['editlistpricespromotion']])) {
							$listPricesPerPharmacyInput = new \stdClass; //This object contains the list of new prices for pharmacies
							if (isset($_POST['starteditListPrices'.$_POST['editlistpricespromotion']]) && isset($_POST['endeditListPrices'.$_POST['editlistpricespromotion']])) {
								$listPricesPerPharmacyInput->dateStart=$_POST['starteditListPrices'.$_POST['editlistpricespromotion']];
								$listPricesPerPharmacyInput->dateEnd=$_POST['endeditListPrices'.$_POST['editlistpricespromotion']];
							}else {
								$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
								$listPricesPerPharmacyInput->dateStart=null;
								$listPricesPerPharmacyInput->dateEnd=null;
							}
							$listPricesPerPharmacyInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
							$listPricesPerPharmacyInput->file=$_FILES['listPrices'.$_POST['editlistpricespromotion']]; // CSV file contain list of pharmacies
							$listPricesPerPharmacyInput->ID=$_POST['editlistpricespromotion'];
							$listPricesPerPharmacyInput->typePharmacySelected='Custom';
							$this->dbControllerAdmin->updatelistPricesPromotion($listPricesPerPharmacyInput);
							$success.='<br>'.__('You have succesfully edited the list of prices per pharmacy!','custom-promotion').'';
						}else {
							$errors.='<br>'.__('The file selected is invalid','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No file was uploaded for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('The promotion ID is not valid','custom-promotion').'.';
			}
		}elseif (isset($_POST['deletelistpricespromotion'])) {
			$tab='listPromotion';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['deletelistpricespromotion'] )) {
				$this->dbControllerAdmin->deleteListPricesPromotion($_POST['deletelistpricespromotion']);
				$success.='<br>'.__('You have succesfully deleted the list of prices per pharmacy!','custom-promotion').'';
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['editproductperchased'])) {
			$tab='listPromotion';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['editproductperchased'] )) {
				if (isset($_POST['allPharmaciesedit'.$_POST['editproductperchased']])) {
					$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['productsEditPurchased'.$_POST['editproductperchased']])) {
						if (sizeof($_POST['productsEditPurchased'.$_POST['editproductperchased']])>0) {
							if (isset($_POST['editNumberProduct'.$_POST['editproductperchased']])) {
									if ($this->dataValidator->checkPositiveNumber($_POST['editNumberProduct'.$_POST['editproductperchased']])) {
										if (isset($_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']])) {
											if ($_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']]=='product') {
												if (isset($_POST['productGifted'.$_POST['editproductperchased']])) {
													if (isset($_POST['editPromotionProduct'.$_POST['editproductperchased']])) {
														if ($this->dataValidator->checkPositiveNumber($_POST['editPromotionProduct'.$_POST['editproductperchased']])) {
															$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
															if (isset($_POST['starteditProductPurchased'.$_POST['editproductperchased']]) && isset($_POST['endeditProductPurchased'.$_POST['editproductperchased']])) {
																$promotionPerProductInput->dateStart=$_POST['starteditProductPurchased'.$_POST['editproductperchased']];
																$promotionPerProductInput->dateEnd=$_POST['endeditProductPurchased'.$_POST['editproductperchased']];
															}else {
																$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
																$promotionPerProductInput->dateStart=null;
																$promotionPerProductInput->dateEnd=null;
															}
															$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
															foreach ($_POST['productsEditPurchased'.$_POST['editproductperchased']] as $key => $value) {
																$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
															}
															$promotionPerProductInput->typePharmacySelected='All';
															$promotionPerProductInput->ID=$_POST['editproductperchased'];
															$promotionPerProductInput->typePromotion=$_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']]; // String 'product' or 'percentage' this attribute contains type of promotion given
															$promotionPerProductInput->numberProducts=$_POST['editNumberProduct'.$_POST['editproductperchased']]; //Number this is the number of products bought to activate this promotion
															$promotionPerProductInput->promotion=$_POST['editPromotionProduct'.$_POST['editproductperchased']]; //Number (number of products given or percentage)
															$promotionPerProductInput->gift=$_POST['productGifted'.$_POST['editproductperchased']];
															$this->dbControllerAdmin->updateProductPromotion($promotionPerProductInput);
															$success.='<br>'.__('You have succesfully edited the promotion per product purchased!','custom-promotion').'';
														}else {
															$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
														}
													}else {
														$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No gift product was set','custom-promotion').'.';
												}
											}elseif ($_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']]=='percentage') {
												if (isset($_POST['editPromotionProduct'.$_POST['editproductperchased']])) {
													if ($this->dataValidator->checkPromotionPercentage($_POST['editNumberProduct'.$_POST['editproductperchased']])) {
														$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['starteditProductPurchased'.$_POST['editproductperchased']]) && isset($_POST['endeditProductPurchased'.$_POST['editproductperchased']])) {
															$promotionPerProductInput->dateStart=$_POST['starteditProductPurchased'.$_POST['editproductperchased']];
															$promotionPerProductInput->dateEnd=$_POST['endeditProductPurchased'.$_POST['editproductperchased']];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerProductInput->dateStart=null;
															$promotionPerProductInput->dateEnd=null;
														}
														$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['productsEditPurchased'.$_POST['editproductperchased']] as $key => $value) {
															$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerProductInput->typePharmacySelected='All';
														$promotionPerProductInput->gift='NULL';
														$promotionPerProductInput->ID=$_POST['editproductperchased'];
														$promotionPerProductInput->typePromotion=$_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']]; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerProductInput->numberProducts=$_POST['editNumberProduct'.$_POST['editproductperchased']]; //Number this is the number of products bought to activate this promotion
														$promotionPerProductInput->promotion=$_POST['editPromotionProduct'.$_POST['editproductperchased']]; //Number (number of products given or percentage)
														$this->dbControllerAdmin->updateProductPromotion($promotionPerProductInput);
														$success.='<br>'.__('You have succesfully edited the promotion per product purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('The number of product purchased is invalid','custom-promotion').'.';
									}
							}else {
								$errors.='<br>'.__('No number of product purchased was set for the promotion','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}elseif(isset($_POST['customPharmacies'.$_POST['editproductperchased']])) {
					$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'.$_POST['editproductperchased']],$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['productsEditPurchased'.$_POST['editproductperchased']])) {
						if (sizeof($_POST['productsEditPurchased'.$_POST['editproductperchased']])>0) {
							if (isset($_POST['editNumberProduct'.$_POST['editproductperchased']])) {
									if ($this->dataValidator->checkPositiveNumber($_POST['editNumberProduct'.$_POST['editproductperchased']])) {
										if (isset($_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']])) {
											if ($_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']]=='product') {
												if (isset($_POST['productGifted'.$_POST['editproductperchased']])) {
													if (isset($_POST['editPromotionProduct'.$_POST['editproductperchased']])) {
														if ($this->dataValidator->checkPositiveNumber($_POST['editPromotionProduct'.$_POST['editproductperchased']])) {
															$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
															if (isset($_POST['starteditProductPurchased'.$_POST['editproductperchased']]) && isset($_POST['endeditProductPurchased'.$_POST['editproductperchased']])) {
																$promotionPerProductInput->dateStart=$_POST['starteditProductPurchased'.$_POST['editproductperchased']];
																$promotionPerProductInput->dateEnd=$_POST['endeditProductPurchased'.$_POST['editproductperchased']];
															}else {
																$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
																$promotionPerProductInput->dateStart=null;
																$promotionPerProductInput->dateEnd=null;
															}
															$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
															foreach ($_POST['productsEditPurchased'.$_POST['editproductperchased']] as $key => $value) {
																$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
															}
															$promotionPerProductInput->typePharmacySelected='Custom';
															$promotionPerProductInput->ID=$_POST['editproductperchased'];
															$promotionPerProductInput->typePromotion=$_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']]; // String 'product' or 'percentage' this attribute contains type of promotion given
															$promotionPerProductInput->numberProducts=$_POST['editNumberProduct'.$_POST['editproductperchased']]; //Number this is the number of products bought to activate this promotion
															$promotionPerProductInput->promotion=$_POST['editPromotionProduct'.$_POST['editproductperchased']]; //Number (number of products given or percentage)
															$promotionPerProductInput->gift=$_POST['productGifted'.$_POST['editproductperchased']];
															$this->dbControllerAdmin->updateProductPromotion($promotionPerProductInput);
															$success.='<br>'.__('You have succesfully edited the promotion per product purchased!','custom-promotion').'';
														}else {
															$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
														}
													}else {
														$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No gift product was set','custom-promotion').'.';
												}
											}elseif ($_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']]=='percentage') {
												if (isset($_POST['editPromotionProduct'.$_POST['editproductperchased']])) {
													if ($this->dataValidator->checkPromotionPercentage($_POST['editNumberProduct'.$_POST['editproductperchased']])) {
														$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['starteditProductPurchased'.$_POST['editproductperchased']]) && isset($_POST['endeditProductPurchased'.$_POST['editproductperchased']])) {
															$promotionPerProductInput->dateStart=$_POST['starteditProductPurchased'.$_POST['editproductperchased']];
															$promotionPerProductInput->dateEnd=$_POST['endeditProductPurchased'.$_POST['editproductperchased']];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerProductInput->dateStart=null;
															$promotionPerProductInput->dateEnd=null;
														}
														$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['productsEditPurchased'.$_POST['editproductperchased']] as $key => $value) {
															$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerProductInput->typePharmacySelected='Custom';
														$promotionPerProductInput->gift='NULL';
														$promotionPerProductInput->ID=$_POST['editproductperchased'];
														$promotionPerProductInput->typePromotion=$_POST['promotionProductSelectedEdit'.$_POST['editproductperchased']]; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerProductInput->numberProducts=$_POST['editNumberProduct'.$_POST['editproductperchased']]; //Number this is the number of products bought to activate this promotion
														$promotionPerProductInput->promotion=$_POST['editPromotionProduct'.$_POST['editproductperchased']]; //Number (number of products given or percentage)
														$this->dbControllerAdmin->updateProductPromotion($promotionPerProductInput);
														$success.='<br>'.__('You have succesfully edited the promotion per product purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('The number of product purchased is invalid','custom-promotion').'.';
									}
							}else {
								$errors.='<br>'.__('No number of product purchased was set for the promotion','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('The promotion ID is not valid','custom-promotion').'.';
			}
		}elseif (isset($_POST['deleteproductperchased'])) {
			$tab='listPromotion';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['deleteproductperchased'] )) {
				$this->dbControllerAdmin->deletePromotion($_POST['deleteproductperchased']);
				$success.='<br>'.__('You have succesfully deleted the promotion per product purchased!','custom-promotion').'';
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['editcategoriepurchased'])) {
			$tab='listPromotion';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['editcategoriepurchased'] )) {
				
				if (isset($_POST['allPharmaciesedit'.$_POST['editcategoriepurchased']])) {
					$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['categoryEditPurchased'.$_POST['editcategoriepurchased']])) {
						if (sizeof($_POST['categoryEditPurchased'.$_POST['editcategoriepurchased']])>0) {
							if (isset($_POST['editNumberCategory'.$_POST['editcategoriepurchased']])) {
									if ($this->dataValidator->checkPositiveNumber($_POST['editNumberCategory'.$_POST['editcategoriepurchased']])) {
										if (isset($_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']])) {
											if ($_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']]=='product') {
												if (isset($_POST['productGiftedCategory'.$_POST['editcategoriepurchased']])) {
													if (isset($_POST['editPromotionCategory'.$_POST['editcategoriepurchased']])) {
														if ($this->dataValidator->checkPositiveNumber($_POST['editPromotionCategory'.$_POST['editcategoriepurchased']])) {
															$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
															if (isset($_POST['starteditCategoryPurchased'.$_POST['editcategoriepurchased']]) && isset($_POST['endeditCategoryPurchased'.$_POST['editcategoriepurchased']])) {
																$promotionPerProductInput->dateStart=$_POST['starteditCategoryPurchased'.$_POST['editcategoriepurchased']];
																$promotionPerProductInput->dateEnd=$_POST['endeditCategoryPurchased'.$_POST['editcategoriepurchased']];
															}else {
																$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
																$promotionPerProductInput->dateStart=null;
																$promotionPerProductInput->dateEnd=null;
															}
															$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
															foreach ($_POST['categoryEditPurchased'.$_POST['editcategoriepurchased']] as $key => $value) {
																$promotionPerProductInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
															}
															$promotionPerProductInput->typePharmacySelected='All';
															$promotionPerProductInput->ID=$_POST['editcategoriepurchased'];
															$promotionPerProductInput->typePromotion=$_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']]; // String 'product' or 'percentage' this attribute contains type of promotion given
															$promotionPerProductInput->numberCategories=$_POST['editNumberCategory'.$_POST['editcategoriepurchased']]; //Number this is the number of products bought to activate this promotion
															$promotionPerProductInput->promotion=$_POST['editPromotionCategory'.$_POST['editcategoriepurchased']]; //Number (number of products given or percentage)
															$promotionPerProductInput->gift=$_POST['productGiftedCategory'.$_POST['editcategoriepurchased']];
															$this->dbControllerAdmin->updateCategoryPromotion($promotionPerProductInput);
															$success.='<br>'.__('You have succesfully edited promotion for products per category purchased!','custom-promotion').'';
														}else {
															$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
														}
													}else {
														$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No gift product was set','custom-promotion').'.';
												}
											}elseif ($_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']]=='percentage') {
												if (isset($_POST['editPromotionCategory'.$_POST['editcategoriepurchased']])) {
													if ($this->dataValidator->checkPromotionPercentage($_POST['editNumberCategory'.$_POST['editcategoriepurchased']])) {
														$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['starteditCategoryPurchased'.$_POST['editcategoriepurchased']]) && isset($_POST['endeditCategoryPurchased'.$_POST['editcategoriepurchased']])) {
															$promotionPerProductInput->dateStart=$_POST['starteditCategoryPurchased'.$_POST['editcategoriepurchased']];
															$promotionPerProductInput->dateEnd=$_POST['endeditCategoryPurchased'.$_POST['editcategoriepurchased']];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerProductInput->dateStart=null;
															$promotionPerProductInput->dateEnd=null;
														}
														$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['categoryEditPurchased'.$_POST['editcategoriepurchased']] as $key => $value) {
															$promotionPerProductInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerProductInput->typePharmacySelected='All';
														$promotionPerProductInput->gift='NULL';
														$promotionPerProductInput->ID=$_POST['editcategoriepurchased'];
														$promotionPerProductInput->typePromotion=$_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']]; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerProductInput->numberCategories=$_POST['editNumberCategory'.$_POST['editcategoriepurchased']]; //Number this is the number of products bought to activate this promotion
														$promotionPerProductInput->promotion=$_POST['editPromotionCategory'.$_POST['editcategoriepurchased']]; //Number (number of products given or percentage)
														$this->dbControllerAdmin->updateCategoryPromotion($promotionPerProductInput);
														$success.='<br>'.__('You have succesfully edited promotion for products per category purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('The number of product purchased is invalid','custom-promotion').'.';
									}
							}else {
								$errors.='<br>'.__('No number of product purchased was set for the promotion','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}elseif(isset($_POST['customPharmacies'.$_POST['editcategoriepurchased']])) {
					
					$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'.$_POST['editcategoriepurchased']],$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					
					if (isset($_POST['categoryEditPurchased'.$_POST['editcategoriepurchased']])) {
						if (sizeof($_POST['categoryEditPurchased'.$_POST['editcategoriepurchased']])>0) {
							if (isset($_POST['editNumberCategory'.$_POST['editcategoriepurchased']])) {
								
									if ($this->dataValidator->checkPositiveNumber($_POST['editNumberCategory'.$_POST['editcategoriepurchased']])) {
										
										if (isset($_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']])) {
											
											if ($_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']]=='product') {
												if (isset($_POST['productGiftedCategory'.$_POST['editcategoriepurchased']])) {
													if (isset($_POST['editPromotionCategory'.$_POST['editcategoriepurchased']])) {
														if ($this->dataValidator->checkPositiveNumber($_POST['editPromotionCategory'.$_POST['editcategoriepurchased']])) {
															$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
															if (isset($_POST['starteditCategoryPurchased'.$_POST['editcategoriepurchased']]) && isset($_POST['endeditCategoryPurchased'.$_POST['editcategoriepurchased']])) {
																$promotionPerProductInput->dateStart=$_POST['starteditCategoryPurchased'.$_POST['editcategoriepurchased']];
																$promotionPerProductInput->dateEnd=$_POST['endeditCategoryPurchased'.$_POST['editcategoriepurchased']];
															}else {
																$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
																$promotionPerProductInput->dateStart=null;
																$promotionPerProductInput->dateEnd=null;
															}
															$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
															foreach ($_POST['categoryEditPurchased'.$_POST['editcategoriepurchased']] as $key => $value) {
																$promotionPerProductInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
															}
															$promotionPerProductInput->typePharmacySelected='Custom';
															$promotionPerProductInput->ID=$_POST['editcategoriepurchased'];
															$promotionPerProductInput->typePromotion=$_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']]; // String 'product' or 'percentage' this attribute contains type of promotion given
															$promotionPerProductInput->numberCategories=$_POST['editNumberCategory'.$_POST['editcategoriepurchased']]; //Number this is the number of products bought to activate this promotion
															$promotionPerProductInput->promotion=$_POST['editPromotionCategory'.$_POST['editcategoriepurchased']]; //Number (number of products given or percentage)
															$promotionPerProductInput->gift=$_POST['productGiftedCategory'.$_POST['editcategoriepurchased']];
															$this->dbControllerAdmin->updateCategoryPromotion($promotionPerProductInput);
															$success.='<br>'.__('You have succesfully edited promotion for products per category purchased!','custom-promotion').'';
														}else {
															$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
														}
													}else {
														$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No gift product was set','custom-promotion').'.';
												}
											}elseif ($_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']]=='percentage') {
												if (isset($_POST['editPromotionCategory'.$_POST['editcategoriepurchased']])) {
													if ($this->dataValidator->checkPromotionPercentage($_POST['editNumberCategory'.$_POST['editcategoriepurchased']])) {
														$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
														if (isset($_POST['starteditCategoryPurchased'.$_POST['editcategoriepurchased']]) && isset($_POST['endeditCategoryPurchased'.$_POST['editcategoriepurchased']])) {
															$promotionPerProductInput->dateStart=$_POST['starteditCategoryPurchased'.$_POST['editcategoriepurchased']];
															$promotionPerProductInput->dateEnd=$_POST['endeditCategoryPurchased'.$_POST['editcategoriepurchased']];
														}else {
															$errors.='<br>'.__('No date was selected for the promotion','custom-promotion').'.';
															$promotionPerProductInput->dateStart=null;
															$promotionPerProductInput->dateEnd=null;
														}
														$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
														foreach ($_POST['categoryEditPurchased'.$_POST['editcategoriepurchased']] as $key => $value) {
															$promotionPerProductInput->categories[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
														}
														$promotionPerProductInput->typePharmacySelected='Custom';
														$promotionPerProductInput->gift='NULL';
														$promotionPerProductInput->ID=$_POST['editcategoriepurchased'];
														$promotionPerProductInput->typePromotion=$_POST['promotionCategoryEditSelected'.$_POST['editcategoriepurchased']]; // String 'product' or 'percentage' this attribute contains type of promotion given
														$promotionPerProductInput->numberCategories=$_POST['editNumberCategory'.$_POST['editcategoriepurchased']]; //Number this is the number of products bought to activate this promotion
														$promotionPerProductInput->promotion=$_POST['editPromotionCategory'.$_POST['editcategoriepurchased']]; //Number (number of products given or percentage)
														$this->dbControllerAdmin->updateCategoryPromotion($promotionPerProductInput);
														$success.='<br>'.__('You have succesfully edited promotion for products per category purchased!','custom-promotion').'';
													}else {
														$errors.='<br>'.__('The number of product given for free is invalid','custom-promotion').'.';
													}
												}else {
													$errors.='<br>'.__('No number of free product was set','custom-promotion').'.';
												}
											}else {
												$errors.='<br>'.__('The type of promotion is invalid','custom-promotion').'.';
											}
										}else {
											$errors.='<br>'.__('No type was selected for the promotion','custom-promotion').'.';
										}
									}else {
										$errors.='<br>'.__('The number of product purchased is invalid','custom-promotion').'.';
									}
							}else {
								$errors.='<br>'.__('No number of product purchased was set for the promotion','custom-promotion').'.';
							}
						}else {
							$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('The promotion ID is not valid','custom-promotion').'.';
			}
		}elseif (isset($_POST['deletecategoriepurchased'])) {
			$tab='listPromotion';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['deletecategoriepurchased'] )) {
				$this->dbControllerAdmin->deletePromotion($_POST['deletecategoriepurchased']);
				$success.='<br>'.__('You have succesfully deleted the promotion for products per category purchased!','custom-promotion').'';
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['validategroupPharm'])) {
			$tab='settings';
			if (isset($_POST['allPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['groupName'])) {
					if ($this->dataValidator->checkValidityGroupName($_POST['groupName'])) {
						$listPharmaciesGroupInput = new \stdClass; //This object contains the group name of list of pharmacies
						$listPharmaciesGroupInput->pharmacies=$pharmacies; // Object This attribute contains the list of pharmacies with this group name.
						$listPharmaciesGroupInput->groupName=$_POST['groupName']; // String contains the group name of these pharmacies
						$listPharmaciesGroupInput->typePharmacySelected='All';
						$this->dbControllerAdmin->createGroup($listPharmaciesGroupInput);
						$success.='<br>'.__('You have succesfully added new group of pharmacies!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('Group name is invalid or already exists','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No group name was set','custom-promotion').'.';
				}
			}elseif(isset($_POST['customPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'],$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['groupName'])) {
					if ($this->dataValidator->checkValidityGroupName($_POST['groupName'])) {
						$listPharmaciesGroupInput = new \stdClass; //This object contains the group name of list of pharmacies
						$listPharmaciesGroupInput->pharmacies=$pharmacies; // Object This attribute contains the list of pharmacies with this group name.
						$listPharmaciesGroupInput->groupName=$_POST['groupName']; // String contains the group name of these pharmacies
						$listPharmaciesGroupInput->typePharmacySelected='Custom';
						$this->dbControllerAdmin->createGroup($listPharmaciesGroupInput);
						$success.='<br>'.__('You have succesfully added new group of pharmacies!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('Group name is invalid or already exists','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No group name was set','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['editgroupPharm'])) {
			$tab='settings';
			if ($this->dataValidator->checkValidityGroupID( $_POST['editgroupPharm'] )) { 
				if (isset($_POST['allPharmaciesedit'.$_POST['editgroupPharm']])) {
					$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['groupName'.$_POST['editgroupPharm']])) {
						if ($this->dataValidator->checkValidityGroupName($_POST['groupName'.$_POST['editgroupPharm']],false)) {
							$listPharmaciesGroupInput = new \stdClass; //This object contains the group name of list of pharmacies
							$listPharmaciesGroupInput->pharmacies=$pharmacies; // Object This attribute contains the list of pharmacies with this group name.
							$listPharmaciesGroupInput->groupName=$_POST['groupName'.$_POST['editgroupPharm']]; // String contains the group name of these pharmacies
							$listPharmaciesGroupInput->ID=$_POST['editgroupPharm'];
							$listPharmaciesGroupInput->typePharmacySelected='All';
							$this->dbControllerAdmin->updateGroup($listPharmaciesGroupInput);
							$success.='<br>'.__('You have succesfully edited the group of pharmacies!','custom-promotion').'';
						}else {
							$errors.='<br>'.__('Group name is invalid or already exists','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No group name was set','custom-promotion').'.';
					}
				}elseif(isset($_POST['customPharmacies'.$_POST['editgroupPharm']])) {
					$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'.$_POST['editgroupPharm']],$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['groupName'.$_POST['editgroupPharm']])) {
						if ($this->dataValidator->checkValidityGroupName($_POST['groupName'.$_POST['editgroupPharm']])) {
							$listPharmaciesGroupInput = new \stdClass; //This object contains the group name of list of pharmacies
							$listPharmaciesGroupInput->pharmacies=$pharmacies; // Object This attribute contains the list of pharmacies with this group name.
							$listPharmaciesGroupInput->groupName=$_POST['groupName'.$_POST['editgroupPharm']]; // String contains the group name of these pharmacies
							$listPharmaciesGroupInput->ID=$_POST['editgroupPharm'];
							$listPharmaciesGroupInput->typePharmacySelected='Custom';
							$this->dbControllerAdmin->updateGroup($listPharmaciesGroupInput);
							$success.='<br>'.__('You have succesfully edited the group of pharmacies!','custom-promotion').'';
						}else {
							$errors.='<br>'.__('Group name is invalid or already exists','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No group name was set','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No pharmacy was selected for the group','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('The group ID is not valid','custom-promotion').'.';
			}
		}elseif (isset($_POST['deletegroupPharm'])) {
			$tab='settings';
			if ($this->dataValidator->checkValidityGroupID( $_POST['deletegroupPharm'] )) {
				$this->dbControllerAdmin->deleteGroup($_POST['deletegroupPharm']);
				$success.='<br>'.__('You have succesfully deleted the group of pharmacies!','custom-promotion').'';
			}else {
				$errors.='<br>'.__('No group was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['validateminimumsum'])) {
			$tab='settings';
			if (isset($_POST['allPharmacies'])) {
				
				$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['sumlimit'])) {
					if ($this->dataValidator->checkPositiveNumber($_POST['sumlimit'])) {
						$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
						$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
						$promotionCheckSumInput->minimumSum=$_POST['sumlimit']; // Number contains the minimum sum to have this promotion
						$promotionCheckSumInput->typePharmacySelected='All';
						$this->dbControllerAdmin->createMinimimSum($promotionCheckSumInput);
						$success.='<br>'.__('You have succesfully added minimum sum required!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No minimum sum was written for these users','custom-promotion').'.';
				}
			}elseif(isset($_POST['customPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'],$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['sumlimit'])) {
					if ($this->dataValidator->checkPositiveNumber($_POST['sumlimit'])) {
						$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
						$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
						$promotionCheckSumInput->minimumSum=$_POST['sumlimit']; // Number contains the minimum sum to have this promotion
						$promotionCheckSumInput->typePharmacySelected='Custom';
						$this->dbControllerAdmin->createMinimimSum($promotionCheckSumInput);
						$success.='<br>'.__('You have succesfully added minimum sum required!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No minimum sum was written for these users','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['editminimumsum'])) {
			$tab='settings';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['editminimumsum'] )) {
				if (isset($_POST['allPharmaciesedit'.$_POST['editminimumsum']])) {
					$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['sumlimit'.$_POST['editminimumsum']])) {
						if ($this->dataValidator->checkPositiveNumber($_POST['sumlimit'.$_POST['editminimumsum']])) {
									$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
									$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
									$promotionCheckSumInput->minimumSum=$_POST['sumlimit'.$_POST['editminimumsum']]; // Number contains the minimum sum to have this promotion
									$promotionCheckSumInput->ID=$_POST['editminimumsum'];
									$promotionCheckSumInput->typePharmacySelected='All';
									$this->dbControllerAdmin->updateMinimumSum($promotionCheckSumInput);
									$success.='<br>'.__('You have succesfully Edited the minimum sum required!','custom-promotion').'';
						}else {
							$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No minimum sum was selected for these users','custom-promotion').'.';
					}
				}elseif(isset($_POST['customPharmacies'.$_POST['editminimumsum']])) {
					$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'.$_POST['editminimumsum']],$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['sumlimit'.$_POST['editminimumsum']])) {
						if ($this->dataValidator->checkPositiveNumber($_POST['sumlimit'.$_POST['editminimumsum']])) {
									$promotionCheckSumInput = new \stdClass; //This object contains the data of check sum promotion
									$promotionCheckSumInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
									$promotionCheckSumInput->minimumSum=$_POST['sumlimit'.$_POST['editminimumsum']]; // Number contains the minimum sum to have this promotion
									$promotionCheckSumInput->ID=$_POST['editminimumsum'];
									$promotionCheckSumInput->typePharmacySelected='Custom';
									$this->dbControllerAdmin->updateMinimumSum($promotionCheckSumInput);
									$success.='<br>'.__('You have succesfully Edited the minimum sum required!','custom-promotion').'';
						}else {
							$errors.='<br>'.__('The sum written is not valid','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No minimum sum was selected for these users','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('The promotion ID is not valid','custom-promotion').'.';
			}	
		}elseif (isset($_POST['deleteminimumsum'])) {
			$tab='settings';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['deleteminimumsum'] )) {
				$this->dbControllerAdmin->deletePromotion($_POST['deleteminimumsum']);
				$success.='<br>'.__('You have succesfully deleted the minimum sum for these users!','custom-promotion').'';
			}else {
				$errors.='<br>'.__('The ID is not valid','custom-promotion').'.';
			}
		}elseif (isset($_POST['validateProductExclution'])) {
			$tab='settings';
			if (isset($_POST['allPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['productsexcluded'])) {
					if (sizeof($_POST['productsexcluded'])>0) {
						$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
						$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
						foreach ($_POST['productsexcluded'] as $key => $value) {
							$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
						}
						$promotionPerProductInput->typePharmacySelected='All';
						$this->dbControllerAdmin->createProductExclution($promotionPerProductInput);
						$success.='<br>'.__('You have succesfully added exclutions of products per pharmacy!','custom-promotion').'';
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
				}
			}elseif(isset($_POST['customPharmacies'])) {
				$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'],$errors); 
				$pharmacies=$pharmaciesChecked->pharmacies;
				$errors=$pharmaciesChecked->errors;
				if (isset($_POST['productsexcluded'])) {
					if (sizeof($_POST['productsexcluded'])>0) {
						$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
						$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
						foreach ($_POST['productsexcluded'] as $key => $value) {
							$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
						}
						$promotionPerProductInput->typePharmacySelected='Custom';
						$this->dbControllerAdmin->createProductExclution($promotionPerProductInput);
						$success.='<br>'.__('You have succesfully added exclutions of products per pharmacy!','custom-promotion').'';
					
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
		}elseif (isset($_POST['editexcludedproduct'])) {
			$tab='settings';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['editexcludedproduct'] )) {
				if (isset($_POST['allPharmaciesedit'.$_POST['editexcludedproduct']])) {
					$pharmaciesChecked = $this->getPharmacies('All',null,$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['productsexcluded'.$_POST['editexcludedproduct']])) {
						if (sizeof($_POST['productsexcluded'.$_POST['editexcludedproduct']])>0) {
							$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
							$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
							foreach ($_POST['productsexcluded'.$_POST['editexcludedproduct']] as $key => $value) {
								$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
							}
							$promotionPerProductInput->typePharmacySelected='All';
							$promotionPerProductInput->ID=$_POST['editexcludedproduct'];
							$this->dbControllerAdmin->updateProductExclution($promotionPerProductInput);
							$success.='<br>'.__('You have succesfully edited the excluded per pharmacy!','custom-promotion').'';
						}else {
							$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}elseif(isset($_POST['customPharmacies'.$_POST['editexcludedproduct']])) {
					$pharmaciesChecked = $this->getPharmacies('Custom',$_POST['customPharmacies'.$_POST['editexcludedproduct']],$errors); 
					$pharmacies=$pharmaciesChecked->pharmacies;
					$errors=$pharmaciesChecked->errors;
					if (isset($_POST['productsexcluded'.$_POST['editexcludedproduct']])) {
						if (sizeof($_POST['productsexcluded'.$_POST['editexcludedproduct']])>0) {
							$promotionPerProductInput = new \stdClass; //This object contains the data of promotion per product
							$promotionPerProductInput->pharmacies=$pharmacies; // Object This attribute contains the pharmacies concerned about this promotion.
							foreach ($_POST['productsexcluded'.$_POST['editexcludedproduct']] as $key => $value) {
								$promotionPerProductInput->products[]=$value; // array( 'ID'=> ) List of products concerned in this promotion
							}
							$promotionPerProductInput->typePharmacySelected='Custom';
							$promotionPerProductInput->ID=$_POST['editexcludedproduct'];
							$this->dbControllerAdmin->updateProductExclution($promotionPerProductInput);
							$success.='<br>'.__('You have succesfully edited the excluded per pharmacy!','custom-promotion').'';
						}else {
							$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
						}
					}else {
						$errors.='<br>'.__('No product was selected for the promotion','custom-promotion').'.';
					}
				}else {
					$errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$errors.='<br>'.__('The promotion ID is not valid','custom-promotion').'.';
			}
		}elseif (isset($_POST['deleteexcludedproduct'])) {
			$tab='settings';
			if ($this->dataValidator->checkValidityPromotionID( $_POST['deleteexcludedproduct'] )) {
				$this->dbControllerAdmin->deletePromotion($_POST['deleteexcludedproduct']);
				$success.='<br>'.__('You have succesfully deleted the excluded products for these users!','custom-promotion').'';
			}else {
				$errors.='<br>'.__('The ID is not valid','custom-promotion').'.';
			}
		}elseif (isset($_GET['getPharmaciesPerGroup'])) {
			echo json_encode ($this->dbControllerAdmin->getAllPharmaciesWithGroupNames());
			return;
		}elseif (isset($_GET['tab'])) {
			if ($_GET['tab']=='addNew') {
				$tab='addNew';
			}elseif ($_GET['tab']=='listPromotion') {
				$tab='listPromotion';
		   }elseif ($_GET['tab']=='settings') {
				$tab='settings';
		   }
	   }
	   $this->adminDisplayController->display_plugin($tab,$errors,$success);
	}

	/**
	 * Gets the pharmacies Concerned about the promotion.
	 *
	 * @since    1.0.0
	 * @param    object    $typePharmacySelected    The type of promotion if it is.
	 * @param    object    $listPharmacies    		The version of this plugin.
	 * 
	 */

	private function getPharmacies($typePharmacySelected,$listPharmacies,$errors){
		$pharmaciesRequest = new \stdClass;
		$pharmaciesRequest->errors=$errors;
		if ($typePharmacySelected=='All') {
			$pharmacies = new \stdClass; // this object contains the type of selection (All or Custom) and list of pharmacies
			$pharmacies->typePharmacySelected='All'; // String This variable decides if All selected or custom selection : string
			$pharmaciesRequest->pharmacies=$pharmacies;
		}elseif ($typePharmacySelected=='Custom') {
			if (sizeof($listPharmacies)>0) {
				$pharmacies = new \stdClass; // this object contains the type of selection (All or Custom) and list of pharmacies
				$pharmacies->typePharmacySelected='Custom'; // String This variable decides if All selected or custom selection : string
				//$pharmacies->listPharmacies=null; // array('ID'=> ,'name'=> ,'email'=> ) If the type is custom selection this attribute contains list of pharmacies.
				//$pharmacies->listNotValidPharmacies=null; // array('ID'=> ,'name'=> ,'email'=> ) If the type is custom selection this attribute contains list of not approved pharmacies.
				foreach ($listPharmacies as $key => $value) {
					$pharmacy = new \stdClass;
					$pharmacy->id=$value;
					$pharmacies->listPharmacies[]=$pharmacy;
				}
				$checkedPharmacies=$this->dataValidator->checkIfPharmacyApproved( $pharmacies->listPharmacies );
				if ($checkedPharmacies!=null) {
					$pharmacies->listPharmacies=$checkedPharmacies->pharmaciesApproved;
					$pharmacies->listNotValidPharmacies=$checkedPharmacies->pharmaciesNotApproved;
					$pharmaciesRequest->pharmacies=$pharmacies->listPharmacies;
				}else {
					$pharmaciesRequest->errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
				}
			}else {
				$pharmaciesRequest->errors.='<br>'.__('No pharmacy was selected for the promotion','custom-promotion').'.';
			}
			
		}
		return $pharmaciesRequest ;
	}
}