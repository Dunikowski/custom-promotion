<?php

/**
 * This class is the interface to the database it helps extracting  data from database for frontend.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes/database-controllers
 */

class DB_Controller_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
	
	/**
	 * gets all products gifts.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $products    return all products sku and name.
	 */
 
	public function getGifts(){
		$products=null;
		global $wpdb;
		$termTable=$wpdb->prefix.'terms';
		$termIDQuery='SELECT term_id FROM '.$termTable.' WHERE slug="giftproduct"';
		$termID=$wpdb->get_row($termIDQuery);
		if ($termID!=null) {
			$termID=$termID->term_id;
		}else
			return null;
		$termTaxTable=$wpdb->prefix.'term_taxonomy';
		$termTaxIDQuery='SELECT term_taxonomy_id FROM '.$termTaxTable.' WHERE term_id='.$termID.'';
		$termTaxID=$wpdb->get_row($termTaxIDQuery);
		if ($termTaxID!=null) {
			$termTaxID=$termTaxID->term_taxonomy_id;
		}else
			return null;
		$termTaxRelTable=$wpdb->prefix.'term_relationships';
		$termTaxRelQuery='SELECT tt.object_id as ID,p.post_title FROM '.$termTaxRelTable.' as tt,'.$wpdb->prefix.'posts as p WHERE tt.term_taxonomy_id='.$termTaxID.' AND p.ID= tt.object_id';
		$postsID=$wpdb->get_results($termTaxRelQuery);

		foreach ($postsID as $key => $post) {
			$skuPost=get_post_meta($post->ID,"_sku",true);
			if (strcmp(substr($skuPost, 0, 1),'A')!=0) {
				$products[]=$skuPost;
			}elseif (strcmp(substr($skuPost, 0, 1),'A')==0) {
				$argsVar = array(
					'posts_per_page' => -1,
					'post_parent'    => $post->ID,
					'post_status'    => 'publish',
					'post_type'      => 'product_variation'
				);
				$available_variations=get_children( $argsVar );
				if ($available_variations!=null && sizeof($available_variations)>0) {
					foreach ($available_variations as $key => $available_variation) {
						$skuPostVar=get_post_meta($available_variation->ID,"_sku",true);
						$products[]=$skuPostVar;
					}
				}
			}
		}
		return $products;
	}

    /**
	 * gets last check sum promotion by user ID.
	 *
	 * @since    1.0.0
	 * @param     string    $ID_Pharmacy      The ID of user.
	 * 
	 * @return    array     $promotionCheckSum    return last check sum promotion.
	 */

	public function getLastCheckSum($ID_Pharmacy){
		global $wpdb;
		$promotionCheckSum=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$queryPromotions='SELECT Distinct ID,type_promotion,number,promotion,date_promotion FROM '.$promotionsTable.' WHERE (type_promotion='.CHECKSUMPROMOTIONALL.' OR type_promotion='.CHECKSUMPROMOTION.') AND ID_user='.$ID_Pharmacy.' AND ( dateStart=NULL OR dateStart="0000-00-00" OR dateEnd=NULL OR dateEnd="0000-00-00" OR (dateStart <= Convert(NOW(), DATE) AND dateEnd >= Convert(NOW(),DATE)) ) ORDER BY number Desc LIMIT 1;';
		$promotion=$wpdb->get_row($queryPromotions);
		if ($promotion!=null && sizeof(get_object_vars($promotion))>0) {
			$promotionCheckSum = new \stdClass; 
			$promotionCheckSum->sum=$promotion->number;
			$promotionCheckSum->percentage=$promotion->promotion;
			$promotionCheckSum->date=$promotion->date_promotion;
		}
		
		return $promotionCheckSum;
	}

	/**
	 * gets last minimum sum required by user ID.
	 *
	 * @since    1.0.0
	 * @param     string    $ID_Pharmacy      The ID of user.
	 * 
	 * @return    array     $promotionCheckSum    return last check sum promotion.
	 */

	public function getLastMinimumSum($ID_Pharmacy){
		global $wpdb;
		$promotionCheckSum=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$queryPromotions='SELECT Distinct ID,type_promotion,number,date_promotion FROM '.$promotionsTable.' WHERE (type_promotion='.MINIMUMSUMALL.' OR type_promotion='.MINIMUMSUM.') AND ID_user='.$ID_Pharmacy.' ORDER BY number Desc LIMIT 1;';
		$promotion=$wpdb->get_row($queryPromotions);
		if ($promotion!=null && sizeof(get_object_vars($promotion))>0) {
			$promotionCheckSum = new \stdClass; 
			$promotionCheckSum->minimumSum=$promotion->number;
			$promotionCheckSum->date=$promotion->date_promotion;
		}
		
		return $promotionCheckSum;
	}

	/**
	 * Gets list of promotions applied in the current cart.
	 *
	 * @since    1.0.0
	 * 
	 * @param     string    $ID_Pharmacy      The ID of user.
	 * 
	 * @return    array     $listPromotionApplied    return list of promotions applied.
	 */

	public function getListPromotionsApplied($ID_Pharmacy){
		global $wpdb;
		$promotionsAppliedTable=$wpdb->prefix.'cp_tmp_cart_prom';
		$queryPromotions='SELECT * FROM '.$promotionsAppliedTable.' WHERE ID_user='.$ID_Pharmacy.' ;';
		$listPromotionApplied=$wpdb->get_results($queryPromotions);
		
		return $listPromotionApplied;
	}

	/**
	 * Add the promotion applied in the current cart.
	 *
	 * @since    1.0.0
	 * 
	 * @param     string    $ID_Pharmacy      The ID of user.
	 * @param     string    $type_promotion      Type of promotion whether free product or percentage.
	 * @param     string    $ID_promotion      The ID of promotion applied.
	 * @param     string    $ID_product      The ID of the product concerned about the current promotion.
	 * @param     string    $product_category      If the promotion came from product or category.
	 * @param     string    $quantity      The quantity of froducts for free or quantity of total products bought.
	 * 
	 * @return    boolean     $true    return the status of insert query.
	 */

	public function addPromotionApplied($ID_Pharmacy,$type_promotion,$ID_promotion,$ID_product,$ID_product_gift,$product_category,$quantity,$promotion,$intentioned){
		global $wpdb;
		$promotionsAppliedTable=$wpdb->prefix.'cp_tmp_cart_prom';
		$wpdb->insert($promotionsAppliedTable, array(
			'ID_user' => $ID_Pharmacy,
			'type_promotion' => $type_promotion,
			'ID_promotion' => $ID_promotion,
			'ID_product' => $ID_product,
			'ID_product_gift' => $ID_product_gift,
			'product_category' => $product_category,
			'quantity' => $quantity,
			'promotion' => $promotion,
			'intentioned' => $intentioned
		));
		return true;
	}

	/**
	 * Gets the object id by sku.
	 *
	 * @since    1.0.0
	 * 
	 * @param     string    $sku      The sku of product.
	 * 
	 * @return    array     $listPromotionApplied    return list of promotions applied.
	 */

	public function getPostIDBySKU($sku){
		global $wpdb;
		$postMetaTable=$wpdb->prefix.'postmeta';
		$queryPostID='SELECT post_id FROM '.$postMetaTable.' WHERE meta_value='.$sku.' ;';
		$postID=$wpdb->get_row($queryPostID);
		if ($postID!=null) {
			$postID=$postID->post_id;
		}
		return $postID;
	}

	/**
	 * Delete the current cart temporary informations.
	 *
	 * @since    1.0.0
	 * 
	 * @param     string    $ID_Pharmacy      The ID of user.
	 * 
	 * @return    boolean     $true    return the status of delete query.
	 */

	public function deleteTemporaryCart($ID_Pharmacy){
		global $wpdb;
		$promotionsAppliedTable=$wpdb->prefix.'cp_tmp_cart_prom';
		$wpdb->get_results(' DELETE FROM '.$promotionsAppliedTable.' WHERE ID_user='.$ID_Pharmacy.'',OBJECT);
		return true;
	}

	/**
	 * Update the promotion applied.
	 *
	 * @since    1.0.0
	 * 
	 * @param     int    $ID_tmp_app      The ID of promotion to update.
	 * @param     int    $quantity        The new quantity to apply.
	 * 
	 * @return    boolean     $true       return the status of delete query.
	 */

	public function updateTemporaryPromotion($ID_tmp_app,$quantity,$intentioned){
		global $wpdb;
		$promotionsAppliedTable=$wpdb->prefix.'cp_tmp_cart_prom';
		$wpdb->get_results('UPDATE '.$promotionsAppliedTable.' 
							SET quantity = '.$quantity.',intentioned='.$intentioned.'
							WHERE ID='.$ID_tmp_app.';',OBJECT);
		return true;
	}

	/**
	 * Delete promotion of item.
	 *
	 * @since    1.0.0
	 * 
	 * @param     string    $ID_Pharmacy      The ID of user.
	 * @param     string    $ID_product     The ID of product deleted.
	 * 
	 * @return    boolean     $true    return the status of delete query.
	 */

	public function deleteTemporaryItem( $ID_Pharmacy, $ID_product ){
		global $wpdb;
		$promotionsAppliedTable=$wpdb->prefix.'cp_tmp_cart_prom';
		$wpdb->get_results(' DELETE FROM '.$promotionsAppliedTable.' WHERE ID_user='.$ID_Pharmacy.' AND ID_product='.$ID_product.'',OBJECT);
		
		return true;
	}

	/**
	* gets all products promotions by user.
	*
	* @since    1.0.0
	* @param     string    $ID_Pharmacy      The ID of user.
	* 
	* @return    array     $products    return all products promoted.
	*/

	public function getAllProductsPromoted($ID_Pharmacy){
		global $wpdb;
		$products=null;
		$SKUsArray[]=null;
		$productsSKU=null;
		$IDsArray=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$productsQuery='select row_ID,sku,date_promotion
		from '.$promotionsTable.' 
		where sku in( select sku from '.$promotionsTable.'  WHERE ID_user='.$ID_Pharmacy.' AND (type_promotion='.PRODUCTPROMOTIONPRODUCT.' OR type_promotion='.PRODUCTPROMOTIONPRODUCTALL.' OR type_promotion='.PRODUCTPROMOTIONPERCENTAGE.' OR type_promotion='.PRODUCTPROMOTIONPERCENTAGEALL.')) 
		AND ID_user='.$ID_Pharmacy.' AND (type_promotion='.PRODUCTPROMOTIONPRODUCT.' OR type_promotion='.PRODUCTPROMOTIONPRODUCTALL.' OR type_promotion='.PRODUCTPROMOTIONPERCENTAGE.' OR type_promotion='.PRODUCTPROMOTIONPERCENTAGEALL.')
		AND ( dateStart=NULL OR dateStart="0000-00-00" OR dateEnd=NULL OR dateEnd="0000-00-00" OR (dateStart <= Convert(NOW(), DATE) AND dateEnd >= Convert(NOW(),DATE)) )
		ORDER BY number desc';

		$productsSKU=$wpdb->get_results($productsQuery);
		if ($productsSKU!=null && sizeof($productsSKU)>0) {
			foreach ($productsSKU as $key => $item) {
				
					$SKUsArray[]=$item->sku;
					$IDsArray[]=$item->row_ID;
				
			}
		}
		if($IDsArray!=null && sizeof($IDsArray)>0){
			foreach ($IDsArray as $key => $item) {
				if ($item) {
					$sqlForSKU='SELECT * FROM '.$promotionsTable.' WHERE row_ID='.$item.'';
					$products[]=$wpdb->get_row($sqlForSKU);
				}
			}
		}

		return $products;
	}

	/**
	* gets all products by categories promotions by user.
	*
	* @since    1.0.0
	* @param     string    $ID_Pharmacy      The ID of user.
	* 
	* @return    array     $products    return all products by categories promoted.
	*/

	public function getAllCategoriesPromoted($ID_Pharmacy){
		global $wpdb;
		$products=null;
		$SKUsArray[]=null;
		$productsSKU=null;
		$categories=null;
		$IDsArray=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$productsQuery='select row_ID,sku,date_promotion
		from '.$promotionsTable.' 
		where sku in( select distinct sku from '.$promotionsTable.'  WHERE ID_user='.$ID_Pharmacy.' AND (type_promotion='.CATEGORYPROMOTIONPRODUCT.' OR type_promotion='.CATEGORYPROMOTIONPRODUCTALL.' OR type_promotion='.CATEGORYPROMOTIONPERCENTAGE.' OR type_promotion='.CATEGORYPROMOTIONPERCENTAGEALL.')) 
		AND ID_user='.$ID_Pharmacy.' AND (type_promotion='.CATEGORYPROMOTIONPRODUCT.' OR type_promotion='.CATEGORYPROMOTIONPRODUCTALL.' OR type_promotion='.CATEGORYPROMOTIONPERCENTAGE.' OR type_promotion='.CATEGORYPROMOTIONPERCENTAGEALL.')
		AND ( dateStart=NULL OR dateStart="0000-00-00" OR dateEnd=NULL OR dateEnd="0000-00-00" OR (dateStart <= Convert(NOW(), DATE) AND dateEnd >= Convert(NOW(),DATE)) )
		ORDER BY date_promotion desc';
		$productsSKU=$wpdb->get_results($productsQuery);

		if ($productsSKU!=null && sizeof($productsSKU)>0) {
			foreach ($productsSKU as $key => $item) {
				if (!in_array($item->sku,$SKUsArray)) {
					$SKUsArray[]=$item->sku;
					$IDsArray[]=$item->row_ID;
				}
			}
		}

		if($IDsArray!=null && sizeof($IDsArray)>0){
			foreach ($IDsArray as $key => $item) {
				if ($item) {
					$sqlForSKU='SELECT * FROM '.$promotionsTable.' WHERE row_ID='.$item.'';
					$categories[]=$wpdb->get_row($sqlForSKU);
				}
			}
		}
		
		if ($categories!=null && sizeof($categories)>0) {
			foreach ($categories as $key => $category) {
				$args = array(
					'post_type' => 'product',
					'post_status' => 'publish',
					'tax_query' => array(
						array(
						'taxonomy' => 'product_cat',
						'field' => 'term_id',
						'terms' => intval($category->sku)
						 )
					  )
					);
				$posts=get_posts($args );
				
				foreach ($posts as $key => $post) {
					if ($post!=null) {
						$skuPost=get_post_meta($post->ID,"_sku",true);
						if ($skuPost!=false) {
							if (strcmp(substr($skuPost, 0, 1),'A')!=0) {
								$product = new \stdClass;
								$product->row_ID=$category->row_ID;
								$product->sku=$skuPost;
								$product->type_promotion=$category->type_promotion; 
								$product->number=$category->number; 
								$product->promotion=$category->promotion; 
								$product->sku_gift=$category->sku_gift; 
								$product->date_promotion=$category->date_promotion; 
								$products[]=$product;
							}elseif (strcmp(substr($skuPost, 0, 1),'A')==0) {
								$argsVar = array(
									'posts_per_page' => -1,
									'post_parent'    => $post->ID,
									'post_status'    => 'publish',
									'post_type'      => 'product_variation'
								);
								$available_variations=get_children( $argsVar );
								if ($available_variations!=null && sizeof($available_variations)>0) {
									foreach ($available_variations as $key => $available_variation) {
										$skuPostVar=get_post_meta($available_variation->ID,"_sku",true);
										$product = new \stdClass;
										$product->row_ID=$category->row_ID;
										$product->sku=$skuPostVar;
										$product->type_promotion=$category->type_promotion; 
										$product->number=$category->number; 
										$product->promotion=$category->promotion; 
										$product->sku_gift=$category->sku_gift; 
										$product->date_promotion=$category->date_promotion; 
										$products[]=$product;
									}
								}
								
							}
						}
					}
					
				}	
			}
		}
	

		return $products;
	}

	/**
	* gets all list prices of products by user.
	*
	* @since    1.0.0
	* @param     string    $ID_Pharmacy      The ID of user.
	* 
	* @return    array     $products    return all products promoted.
	*/

	public function getAllListPricesByPharmacy($ID_Pharmacy){
		global $wpdb;
		$products=null;
		$SKUsArray[]=null;
		$productsSKU=null;
		$IDsArray=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$productsQuery='select row_ID,sku,date_promotion
		from '.$promotionsTable.' 
		where sku in( select distinct sku from '.$promotionsTable.'  WHERE ID_user='.$ID_Pharmacy.' AND (type_promotion='.LISTPRICES.' OR type_promotion='.LISTPRICESALL.' )) 
		AND ID_user='.$ID_Pharmacy.' AND (type_promotion='.LISTPRICES.' OR type_promotion='.LISTPRICESALL.' )
		AND ( dateStart=NULL OR dateStart="0000-00-00" OR dateEnd=NULL OR dateEnd="0000-00-00" OR (dateStart <= Convert(NOW(), DATE) AND dateEnd >= Convert(NOW(),DATE)) )
		ORDER BY date_promotion desc';

		$productsSKU=$wpdb->get_results($productsQuery);
		if ($productsSKU!=null && sizeof($productsSKU)>0) {
			foreach ($productsSKU as $key => $item) {
				if (!in_array($item->sku,$SKUsArray)) {
					$SKUsArray[]=$item->sku;
					$IDsArray[]=$item->row_ID;
				}
			}
		}
		if($IDsArray!=null && sizeof($IDsArray)>0){
			foreach ($IDsArray as $key => $item) {
				if ($item) {
					$sqlForSKU='SELECT * FROM '.$promotionsTable.' WHERE row_ID='.$item.'';
					$products[]=$wpdb->get_row($sqlForSKU);
				}
			}
		}
		return $products;
	}
    
	/**
	* gets all products excluded by user.
	*
	* @since    1.0.0
	* @param     string    $ID_Pharmacy      The ID of user.
	* 
	* @return    array     $products    return all products excluded.
	*/

	public function getAllProductsExcluded($ID_Pharmacy){
		global $wpdb;
		$products=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$productsQuery='SELECT DISTINCT sku FROM   '.$promotionsTable.' 
        WHERE ID_user='.$ID_Pharmacy.' AND (type_promotion='.PRODUCTLIMITATION.' OR type_promotion='.PRODUCTLIMITATIONALL.' )';
		$products=$wpdb->get_results($productsQuery);
		return $products;
	}

	/**
	* count quantity for gift product.
	*
	* @since    1.0.0
	* @param     string    $ID_Pharmacy      The ID of user.
	* 
	* @return    array     $quantity    return the quantity sum.
	*/

	public function getCountQuantityGift($ID_product_gift,$ID_product){
		global $wpdb;
		$products=null;
		$quantity=0;
		$promotionsCartTable=$wpdb->prefix.'cp_tmp_cart_prom';
		$quantityQuery='SELECT quantity FROM   '.$promotionsCartTable.' 
        WHERE ID_product_gift='.$ID_product_gift.' AND ID_product != '.$ID_product.'';
		$products=$wpdb->get_results($quantityQuery);
		if ($products!=null && sizeof($products)>0) {
			foreach ($products as $key => $item) {
				$quantity+=intval($item->quantity);
			}
		}
		return $quantity;
	}
}