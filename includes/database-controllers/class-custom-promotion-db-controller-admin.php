<?php

/**
 * This class is the interface to the database it helps extracting ,filtering, updating and deleting data from 
 * database for backend.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes/database-controllers
 */

class DB_Controller_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
	
	/**
	 * gets all pharmacies approved with group names.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $pharmacies    return all pharmacies with group names.
	 */

	public function getAllPharmaciesWithGroupNames(){
		$pharmacies=$this->getAllPharmacies();
		global $wpdb;
		$groupsTable=$wpdb->prefix.'cp_groupnames';
		$groupsUsersTable=$wpdb->prefix.'cp_grouplist';
		foreach ($pharmacies->listPharmacies as $key => $pharmacy) {
			$sqlQuery='SELECT gn.name FROM '.$groupsTable.' as gn,
					  '.$groupsUsersTable.' as gl
					   WHERE gn.ID=gl.ID_group AND gl.ID_user='.$pharmacy->id.' ;';
			$pharmacy->groups=$wpdb->get_results($sqlQuery);
		}
		return $pharmacies->listPharmacies;
	}

	/**
	 * gets all group names.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $groups    return all group names.
	 */

	public function getAllGroupNames(){
		global $wpdb;
		$groupsTable=$wpdb->prefix.'cp_groupnames';
		$sqlQuery='SELECT ID as id,name as name FROM '.$groupsTable.' ;';
		$groups=$wpdb->get_results($sqlQuery);
		return $groups;
	}

	/**
	 * gets all pharmacies approved with group names.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $groups    return all group names.
	 */

	public function getAllGroups(){
		global $wpdb;
		$groupsTable=$wpdb->prefix.'cp_groupnames';
		$groupslistTable=$wpdb->prefix.'cp_grouplist';
		$sqlQuery='SELECT ID as id,name as name FROM '.$groupsTable.' ;';
		$groups=$wpdb->get_results($sqlQuery);
		$groupsReturned=null;
		
		foreach ($groups as $key => $group) {
			$groupe = new \stdClass; 
			$groupe->id=$group->id;
			$groupe->name=$group->name;
			$sqlQuery='SELECT ID_user as id FROM '.$groupslistTable.' WHERE ID_group='.$group->id.';';
			$users=$wpdb->get_results($sqlQuery);
			foreach ($users as $key => $user) {
				$groupe->pharmacies[]=$user->id;
			}
			$groupsReturned[]=$groupe;
		}
		return $groupsReturned;
	}

	/**
	 * gets all products sku and names.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $products    return all products sku and name.
	 */
 
	public function getAllProducts(){
		$products=null;
		$posts = get_posts(array(
			'post_type'   => array('product', 'product_variation'),
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'fields' => array('ids', 'post_title')
			)
		);
		foreach ($posts as $key => $post) {
			$skuPost=get_post_meta($post->ID,"_sku",true);
			$giftsProducts=$this->getGiftsAdmin();
			if ($giftsProducts!=null) {
				if (strcmp(substr($skuPost, 0, 1),'A')!=0 && !in_array($skuPost,$giftsProducts)) {
					$product = new \stdClass; 
					$product->id=$skuPost;
					$product->name=$post->post_title; 
					$products[]=$product;
				}
			}
		}
		return $products;
	}

/**
	 * gets all products gifts.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $products    return all products sku and name.
	 */
 
	public function getGiftsAdmin(){
		$products=null;
		global $wpdb;
		$termTable=$wpdb->prefix.'terms';
		$termIDQuery='SELECT term_id FROM '.$termTable.' WHERE slug="giftproduct"';
		$termID=$wpdb->get_row($termIDQuery);
		if ($termID!=null) {
			$termID=$termID->term_id;
		}else
			return null;
		$termTaxTable=$wpdb->prefix.'term_taxonomy';
		$termTaxIDQuery='SELECT term_taxonomy_id FROM '.$termTaxTable.' WHERE term_id='.$termID.'';
		$termTaxID=$wpdb->get_row($termTaxIDQuery);
		if ($termTaxID!=null) {
			$termTaxID=$termTaxID->term_taxonomy_id;
		}else
			return null;
		$termTaxRelTable=$wpdb->prefix.'term_relationships';
		$termTaxRelQuery='SELECT tt.object_id as ID,p.post_title FROM '.$termTaxRelTable.' as tt,'.$wpdb->prefix.'posts as p WHERE tt.term_taxonomy_id='.$termTaxID.' AND p.ID= tt.object_id';
		$postsID=$wpdb->get_results($termTaxRelQuery);

		foreach ($postsID as $key => $post) {
			$skuPost=get_post_meta($post->ID,"_sku",true);
			if (strcmp(substr($skuPost, 0, 1),'A')!=0) {
				$products[]=$skuPost;
			}elseif (strcmp(substr($skuPost, 0, 1),'A')==0) {
				$argsVar = array(
					'posts_per_page' => -1,
					'post_parent'    => $post->ID,
					'post_status'    => 'publish',
					'post_type'      => 'product_variation'
				);
				$available_variations=get_children( $argsVar );
				if ($available_variations!=null && sizeof($available_variations)>0) {
					foreach ($available_variations as $key => $available_variation) {
						$skuPostVar=get_post_meta($available_variation->ID,"_sku",true);
						$products[]=$skuPostVar;
					}
				}
			}
		}
		return $products;
	}

	/**
	 * gets all products gifts.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $products    return all products sku and name.
	 */
 
	public function getproductsGifts(){
		$products=null;
		global $wpdb;
		$termTable=$wpdb->prefix.'terms';
		$termIDQuery='SELECT term_id FROM '.$termTable.' WHERE slug="giftproduct"';
		$termID=$wpdb->get_row($termIDQuery);
		if ($termID!=null) {
			$termID=$termID->term_id;
		}else
			return null;
		$termTaxTable=$wpdb->prefix.'term_taxonomy';
		$termTaxIDQuery='SELECT term_taxonomy_id FROM '.$termTaxTable.' WHERE term_id='.$termID.'';
		$termTaxID=$wpdb->get_row($termTaxIDQuery);
		if ($termTaxID!=null) {
			$termTaxID=$termTaxID->term_taxonomy_id;
		}else
			return null;
		$termTaxRelTable=$wpdb->prefix.'term_relationships';
		$termTaxRelQuery='SELECT tt.object_id as ID,p.post_title FROM '.$termTaxRelTable.' as tt,'.$wpdb->prefix.'posts as p WHERE tt.term_taxonomy_id='.$termTaxID.' AND p.ID= tt.object_id';
		$postsID=$wpdb->get_results($termTaxRelQuery);

		foreach ($postsID as $key => $post) {
			$skuPost=get_post_meta($post->ID,"_sku",true);
			if (strcmp(substr($skuPost, 0, 1),'A')!=0) {
				$product = new \stdClass; 
				$product->id=$skuPost;
				$product->name=$post->post_title; 
				$products[]=$product;
			}elseif (strcmp(substr($skuPost, 0, 1),'A')==0) {
				$argsVar = array(
					'posts_per_page' => -1,
					'post_parent'    => $post->ID,
					'post_status'    => 'publish',
					'post_type'      => 'product_variation'
				);
				$available_variations=get_children( $argsVar );
				if ($available_variations!=null && sizeof($available_variations)>0) {
					foreach ($available_variations as $key => $available_variation) {
						$skuPostVar=get_post_meta($available_variation->ID,"_sku",true);
						$product = new \stdClass;
						$product->id=$skuPostVar;
						$product->name=$available_variation->post_title; 
						$products[]=$product;
					}
				}
				
			}
			
		}

		return $products;
	}

	/**
	 * gets all categories sku and names.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $categories    return all categories sku and name.
	 */

	public function getAllcategories(){
		$terms = get_terms( array(
			'taxonomy' => 'product_cat',
			'hide_empty' => true
		) );
		foreach ($terms as $key => $term) {	
			if ($term->name!='giftproduct') {
				$category = new \stdClass; 
				$category->id=$term->term_id;
				$category->name=$term->name; 
				$categories[]=$category;
			}	
		}
		return $categories;
	}

	/**
	 * gets all pharmacies approved.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $pharmacies    return all pharmacies.
	 */
	public function getAllPharmacies() {

		$pharmacies = new \stdClass;
		$args = array(
			'meta_query' => array(
				
				array(
					'key' => 'account_status',
					'value' => 'approved',
					'compare' => '=='
				),
				array(
					'key' => 'wp_capabilities',
					'value' => 'um_apteka',
					'compare' => 'LIKE'
				)
			)
		); 
		$member_arr = get_users($args); //finds all users with this meta_key == 'member_id' and meta_value == $member_id passed in url
		
		if ($member_arr) {  // any users found?
		  foreach ($member_arr as $user) {  // in my case, there should only be one user with the id
			$pharmacy = new \stdClass;
			$pharmacy->id=$user->ID;
			$pharmacy->email=$user->user_email;
			$pharmacy->name=$user->display_name;
			$pharmacy->company=get_user_meta($user->ID,'billing_company', true );
			$pharmacy->city=get_user_meta($user->ID,'billing_city', true );
			$pharmacy->street=get_user_meta($user->ID,'billing_address_1', true );
			$pharmacy->nip=get_user_meta($user->ID,'nip', true );
			$pharmacy->idAp=get_user_meta($user->ID,'id_wlasne_apteki', true );  
			$pharmacies->listPharmacies[]=$pharmacy;
		  }
		} 
		
		return $pharmacies;
	}

	/**
	 * Creates check sum promotion.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */
	public function createCheckSumPromotion($promotion) {
		global $wpdb;
		$typePromotion=CHECKSUMPROMOTION;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$max_promotionID=$wpdb->get_results('SELECT MAX(ID) as id FROM '.$wpdb->prefix.'cp_promotions',OBJECT);
		$max_promotionID=$max_promotionID[0]->id+1;
		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=CHECKSUMPROMOTIONALL;
		} 
		
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				$wpdb->insert($wpdb->prefix.'cp_promotions', array(
					'ID' => $max_promotionID,
					'ID_user' => $pharmacy->id,
					'type_promotion' => $typePromotion,
					'number' => $promotion->sum,
					'promotion' => $promotion->percentage,
					'dateStart' => $promotion->dateStart,
					'dateEnd' => $promotion->dateEnd,
					'date_promotion' => $current_date_promotion
				));
			}
		}
		return true;
	}

	/**
	 * Creates minimum sum.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */
	public function createMinimimSum($promotion) {
		global $wpdb;
		$typePromotion=MINIMUMSUM;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$max_promotionID=$wpdb->get_results('SELECT MAX(ID) as id FROM '.$wpdb->prefix.'cp_promotions',OBJECT);
		$max_promotionID=$max_promotionID[0]->id+1;
		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=MINIMUMSUMALL;
		}
		
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				$wpdb->insert($wpdb->prefix.'cp_promotions', array(
					'ID' => $max_promotionID,
					'ID_user' => $pharmacy->id,
					'type_promotion' => $typePromotion,
					'number' => $promotion->minimumSum,
					'date_promotion' => $current_date_promotion
				));
			}
		}
		return true;
	}

	/**
	 * Delete promotions.
	 *
	 * @since    1.0.0
	 * @param     object    $ID      The ID of promotion that should be deleted.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfull
	 */
	public function deletePromotion($ID) {
		global $wpdb;
		$deleteQuery=$wpdb->get_results(' DELETE FROM '.$wpdb->prefix.'cp_promotions WHERE ID='.$ID.'',OBJECT);
		return true;
	}

	/**
	 * Delete list prices promotion.
	 *
	 * @since    1.0.0
	 * @param     object    $ID      The ID of promotion that should be deleted.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfull
	 */
	public function deleteListPricesPromotion($ID) {
		global $wpdb;
		$deleteQuery=$wpdb->get_results(' DELETE FROM '.$wpdb->prefix.'cp_promotions WHERE ID='.$ID.'',OBJECT);
		//unlink(plugin_dir_path(dirname(__Dir__)) . 'uploads/custom-promotions/'.$ID.'.csv');
		return true;
	}

	/**
	 * Update check sum promotion.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */
	public function updateCheckSumPromotion($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$this->deletePromotion($promotion->ID);
		$typePromotion=CHECKSUMPROMOTION;

		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=CHECKSUMPROMOTIONALL;
		}
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				$wpdb->insert($wpdb->prefix.'cp_promotions', array(
					'ID' => $promotion->ID,
					'ID_user' => $pharmacy->id,
					'type_promotion' => $typePromotion,
					'number' => $promotion->sum,
					'promotion' => $promotion->percentage,
					'dateStart' => $promotion->dateStart,
					'dateEnd' => $promotion->dateEnd,
					'date_promotion' => $current_date_promotion
				));
			}
			
		}
		return true;
	}

	/**
	 * Update minimum sum required.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */
	public function updateMinimumSum($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$this->deletePromotion($promotion->ID);
		$typePromotion=MINIMUMSUM;

		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=MINIMUMSUMALL;
		}
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				$wpdb->insert($wpdb->prefix.'cp_promotions', array(
					'ID' => $promotion->ID,
					'ID_user' => $pharmacy->id,
					'type_promotion' => $typePromotion,
					'number' => $promotion->minimumSum,
					'date_promotion' => $current_date_promotion
				));
			}
			
		}
		return true;
	}

	/**
	 * Updates promotion by product purchased.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */

	public function updateProductPromotion($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$this->deletePromotion($promotion->ID);

		if ($promotion->typePromotion=='product') {
			$typePromotion=PRODUCTPROMOTIONPRODUCT;
		}else {
			$typePromotion=PRODUCTPROMOTIONPERCENTAGE;
		}

		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=$typePromotion+6;
		}
		
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				foreach ($promotion->products as $key => $product) {
					if ($product!=null) {
						$wpdb->insert($wpdb->prefix.'cp_promotions', array(
							'ID' => $promotion->ID,
							'ID_user' => $pharmacy->id,
							'type_promotion' => $typePromotion,
							'sku' => $product,
							'sku_gift' => $promotion->gift,
							'number' => $promotion->numberProducts,
							'promotion' => $promotion->promotion,
							'dateStart' => $promotion->dateStart,
							'dateEnd' => $promotion->dateEnd,
							'date_promotion' => $current_date_promotion
						));
					}	
				}	
			}
		}
		return true;
	}

	/**
	 * Creates promotion by product purchased.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */

	public function createProductPromotion($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$max_promotionID=$wpdb->get_results('SELECT MAX(ID) as id FROM '.$wpdb->prefix.'cp_promotions',OBJECT);
		$max_promotionID=$max_promotionID[0]->id+1;

		if ($promotion->typePromotion=='product') {
			$typePromotion=PRODUCTPROMOTIONPRODUCT;
		}else {
			$typePromotion=PRODUCTPROMOTIONPERCENTAGE;
		}
		
		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=$typePromotion+6;
		}

		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0){
				foreach ($promotion->products as $key => $product) {
					if ($product!=null) {
						$wpdb->insert($wpdb->prefix.'cp_promotions', array(
							'ID' => $max_promotionID,
							'ID_user' => $pharmacy->id,
							'type_promotion' => $typePromotion,
							'sku' => $product,
							'sku_gift' => $promotion->gift,
							'number' => $promotion->numberProducts,
							'promotion' => $promotion->promotion,
							'dateStart' => $promotion->dateStart,
							'dateEnd' => $promotion->dateEnd,
							'date_promotion' => $current_date_promotion
						));
					}			
				}	
			}
		}
		return true;
	}

	/**
	 * Creates promotion by category purchased.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */

	public function createCategoryPromotion($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$max_promotionID=$wpdb->get_results('SELECT MAX(ID) as id FROM '.$wpdb->prefix.'cp_promotions',OBJECT);
		$max_promotionID=$max_promotionID[0]->id+1;
		if ($promotion->typePromotion=='product') {
			$typePromotion=CATEGORYPROMOTIONPRODUCT;
		}else {
			$typePromotion=CATEGORYPROMOTIONPERCENTAGE;
		}

		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=$typePromotion+6;
		}
		
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				foreach ($promotion->categories as $key => $category) {
					if ($category!=null) {
						$wpdb->insert($wpdb->prefix.'cp_promotions', array(
							'ID' => $max_promotionID,
							'ID_user' => $pharmacy->id,
							'type_promotion' => $typePromotion,
							'sku' => $category,
							'sku_gift' => $promotion->gift,
							'number' => $promotion->numberCategories,
							'promotion' => $promotion->promotion,
							'dateStart' => $promotion->dateStart,
							'dateEnd' => $promotion->dateEnd,
							'date_promotion' => $current_date_promotion
						));
					}	
				}
			}	
		}
		return true;
	}

	/**
	 * Updates promotion by category purchased.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */

	public function updateCategoryPromotion($promotion) {
		global $wpdb;
		
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$this->deletePromotion($promotion->ID);

		if ($promotion->typePromotion=='product') {
			$typePromotion=CATEGORYPROMOTIONPRODUCT;
		}else {
			$typePromotion=CATEGORYPROMOTIONPERCENTAGE;
		}

		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=$typePromotion+6;
		}

		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
			foreach ($promotion->categories as $key => $category) {
				if ($category!=null) {
					$wpdb->insert($wpdb->prefix.'cp_promotions', array(
						'ID' => $promotion->ID,
						'ID_user' => $pharmacy->id,
						'type_promotion' => $typePromotion,
						'sku' => $category,
						'sku_gift' => $promotion->gift,
						'number' => $promotion->numberCategories,
						'promotion' => $promotion->promotion,
						'dateStart' => $promotion->dateStart,
						'dateEnd' => $promotion->dateEnd,
						'date_promotion' => $current_date_promotion
					));
				}	
			}	
		}
		}
		return true;
	}

	/**
	 * Creates list of prices for products.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */

	public function createlistPricesPromotion($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$max_promotionID=$wpdb->get_results('SELECT MAX(ID) as id FROM '.$wpdb->prefix.'cp_promotions',OBJECT);
		$max_promotionID=$max_promotionID[0]->id+1;
		$typePromotion=LISTPRICES;
		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=$typePromotion+6;
		}
		$targetName=plugin_dir_path(dirname(__Dir__))  . 'uploads/custom-promotions/'.$max_promotionID.'.csv';
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				if (($handle = fopen($promotion->file["tmp_name"], "r")) !== FALSE) {
					fgetcsv($handle, 1000, ";");
					while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
						$wpdb->insert($wpdb->prefix.'cp_promotions', array(
							'ID' => $max_promotionID,
							'ID_user' => $pharmacy->id,
							'type_promotion' => $typePromotion,
							'sku' => $data[0],
							'promotion' => $data[1],
							'dateStart' => $promotion->dateStart,
							'dateEnd' => $promotion->dateEnd,
							'date_promotion' => $current_date_promotion
						));
					}
					fclose($handle);    
				}
			}		
		}
		//move_uploaded_file($promotion->file["tmp_name"], $targetName);
		return true;
	}

	/**
	 * Update list of prices for products.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */

	public function updatelistPricesPromotion($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$this->deleteListPricesPromotion($promotion->ID);
		$typePromotion=LISTPRICES; 
		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=$typePromotion+6;
		}

		$targetName=plugin_dir_path(dirname(__Dir__)) . 'uploads/custom-promotions/'.$promotion->ID.'.csv';
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
			if (($handle = fopen($promotion->file["tmp_name"], "r")) !== FALSE) {
				fgetcsv($handle, 1000, ";");
				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
					$wpdb->insert($wpdb->prefix.'cp_promotions', array(
						'ID' => $promotion->ID,
						'ID_user' => $pharmacy->id,
						'type_promotion' => $typePromotion,
						'sku' => $data[0],
						'promotion' => $data[1],
						'dateStart' => $promotion->dateStart,
						'dateEnd' => $promotion->dateEnd,
						'date_promotion' => $current_date_promotion
					));
				}
				fclose($handle);    
			}	
		}
		}
		//move_uploaded_file($promotion->file["tmp_name"], $targetName);
		return true;
	}

    /**
	 * Checks if a pharmacy is approved by the administrators.
	 *
	 * @since    1.0.0
	 * @param    string    $ID       The Id of the pharmacy to check
	 */
	public function isPharmacyApproved( $ID ) {
		if (get_user_meta( $ID, 'account_status', true )=='approved') {
			return true;
		}else {
			return false;
		}	
	}
    
	/**
	 * Checks if a group name alredy exists by pharmacies.
	 *
	 * @since    1.0.0
	 * @param    string    $name       The Id of the pharmacy to check
	 */
	public function existGroupName( $name ) {
		global $wpdb ;

		if (sizeof($wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'cp_groupnames WHERE name="'.$name.'" ',OBJECT))>0) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * Checks if a group name alredy exists by pharmacies.
	 *
	 * @since    1.0.0
	 * @param    string    $name       The Id of the pharmacy to check
	 */
	public function existGroup( $ID ) {
		global $wpdb ;

		if (sizeof($wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'cp_groupnames WHERE ID='.$ID.' ',OBJECT))>0) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * Checks if a group name alredy exists by pharmacies.
	 *
	 * @since    1.0.0
	 * @param    string    $name       The Id of the pharmacy to check
	 */
	public function existPromotion( $ID ) {
		global $wpdb ;
		if (sizeof($wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'cp_promotions WHERE ID="'.$ID.'" '))>0) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * Delete Group.
	 *
	 * @since    1.0.0
	 * @param    string    $ID       The Id of the group to be deleted
	 */
	public function deleteGroup( $ID ) {
		global $wpdb;
		$deleteQuery=$wpdb->get_results(' DELETE FROM '.$wpdb->prefix.'cp_groupnames WHERE ID='.$ID.'',OBJECT);
		return true;
	}

	/**
	 * Creates Group.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */
	public function createGroup($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$max_promotionID=$wpdb->get_results('SELECT MAX(ID) as id FROM '.$wpdb->prefix.'cp_groupnames',OBJECT);
		$max_promotionID=$max_promotionID[0]->id+1;
		$wpdb->insert($wpdb->prefix.'cp_groupnames', array(
			'ID' => $max_promotionID,
			'name' => $promotion->groupName
		));
		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
		}
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				$wpdb->insert($wpdb->prefix.'cp_grouplist', array(
					'ID_group' => $max_promotionID,
					'ID_user' => $pharmacy->id
				));
			}	
		}
		return true;
	}

	/**
	 * update Group.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */
	public function updateGroup($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$this->deleteGroup( $promotion->ID );
		$wpdb->insert($wpdb->prefix.'cp_groupnames', array(
			'ID' => $promotion->ID,
			'name' => $promotion->groupName
		));
		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
		}
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				$wpdb->insert($wpdb->prefix.'cp_grouplist', array(
					'ID_group' => $promotion->ID,
					'ID_user' => $pharmacy->id
					));
			}	
		}
		return true;
	}

	/**
	 * gets all check sum promotions.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $promotions    return all gcheck sum promotions.
	 */

	public function getAllCheckSum(){
		global $wpdb;
		$promotions=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$queryPromotions='SELECT Distinct ID,type_promotion,number,promotion,dateStart,dateEnd,date_promotion FROM '.$promotionsTable.' WHERE type_promotion='.CHECKSUMPROMOTIONALL.' OR type_promotion='.CHECKSUMPROMOTION.' ORDER BY date_promotion Desc ;';
		$Promotions=$wpdb->get_results($queryPromotions);
		foreach ($Promotions as $key => $promotion) {
			$queryPromotions='SELECT ID_user FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			$users=$wpdb->get_results($queryPromotions);
			unset($usersID);
			foreach ($users as $key => $item) {
				$usersID[]=$item->ID_user;
			}
			$promotionCheckSumDisplay = new \stdClass; 
			$promotionCheckSumDisplay->typePromotion=$promotion->type_promotion;
			$promotionCheckSumDisplay->pharmacies=$usersID;
			$promotionCheckSumDisplay->sum=$promotion->number;
			$promotionCheckSumDisplay->percentage=$promotion->promotion;
			$promotionCheckSumDisplay->ID=$promotion->ID;
			$promotionCheckSumDisplay->date=$promotion->date_promotion;
			$promotionCheckSumDisplay->dateStart=$promotion->dateStart;
			$promotionCheckSumDisplay->dateEnd=$promotion->dateEnd;
			$promotions[]=$promotionCheckSumDisplay;
		}
		
		
		return $promotions;
	}

	/**
	 * gets all minimum sums.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $promotions    return all gcheck sum promotions.
	 */

	public function getAllMinimumSum(){
		global $wpdb;
		$promotions=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$queryPromotions='SELECT Distinct ID,type_promotion,number,date_promotion FROM '.$promotionsTable.' WHERE type_promotion='.MINIMUMSUMALL.' OR type_promotion='.MINIMUMSUM.' ORDER BY date_promotion Desc ;';
		$Promotions=$wpdb->get_results($queryPromotions);
		foreach ($Promotions as $key => $promotion) {
			$queryPromotions='SELECT ID_user FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			$users=$wpdb->get_results($queryPromotions);
			unset($usersID);
			foreach ($users as $key => $item) {
				$usersID[]=$item->ID_user;
			}
			$promotionCheckSumDisplay = new \stdClass; 
			$promotionCheckSumDisplay->typePromotion=$promotion->type_promotion;
			$promotionCheckSumDisplay->pharmacies=$usersID;
			$promotionCheckSumDisplay->sum=$promotion->number;
			$promotionCheckSumDisplay->ID=$promotion->ID;
			$promotionCheckSumDisplay->date=$promotion->date_promotion;
			$promotions[]=$promotionCheckSumDisplay;
		}
		
		
		return $promotions;
	}

	/**
	* gets all product purchased promotions.
	*
	* @since    1.0.0
	* 
	* @return    array    $promotions    return all gcheck sum promotions.
	*/

   public function getAllProductPurchased(){
	   global $wpdb;
	   $promotions=null;
	   $promotionsTable=$wpdb->prefix.'cp_promotions';
	   $queryPromotions='SELECT Distinct ID,type_promotion,sku_gift,number,promotion,dateStart,dateEnd,date_promotion FROM '.$promotionsTable.' WHERE type_promotion='.PRODUCTPROMOTIONPRODUCT.' OR type_promotion='.PRODUCTPROMOTIONPRODUCTALL.' OR type_promotion='.PRODUCTPROMOTIONPERCENTAGE.' OR type_promotion='.PRODUCTPROMOTIONPERCENTAGEALL.' ;';
	   $Promotions=$wpdb->get_results($queryPromotions);
	   foreach ($Promotions as $key => $promotion) {
		   $queryPromotions='SELECT ID_user FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
		   $users=$wpdb->get_results($queryPromotions);
		   unset($usersID);
		   foreach ($users as $key => $item) {
			$usersID[]=$item->ID_user;
		}
		   $queryPromotions='SELECT sku FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
		   $products=$wpdb->get_results($queryPromotions);
		   unset($productsSKU);
		   foreach ($products as $key => $item) {
			$productsSKU[]=$item->sku;
		}
		   $promotionProductDisplay = new \stdClass; 
		   $promotionProductDisplay->typePromotion=$promotion->type_promotion;
		   $promotionProductDisplay->pharmacies=$usersID;
		   $promotionProductDisplay->numberProducts=$promotion->number;
		   $promotionProductDisplay->promotion=$promotion->promotion;
		   $promotionProductDisplay->productGifted=$promotion->sku_gift;
		   $promotionProductDisplay->ID=$promotion->ID;
		   $promotionProductDisplay->products=$productsSKU;
		   $promotionProductDisplay->date=$promotion->date_promotion;
		   $promotionProductDisplay->dateStart=$promotion->dateStart;
		   $promotionProductDisplay->dateEnd=$promotion->dateEnd;
		   $promotions[]=$promotionProductDisplay;
	   }
	   
	   
	   return $promotions;
   }

   /**
	* gets all product purchased promotions.
	*
	* @since    1.0.0
	* 
	* @return    array    $promotions    return all gcheck sum promotions.
	*/

	public function getAllCategoryPurchased(){
		global $wpdb;
		$promotions=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$queryPromotions='SELECT Distinct ID,type_promotion,sku_gift,number,promotion,dateStart,dateEnd,date_promotion FROM '.$promotionsTable.' WHERE type_promotion='.CATEGORYPROMOTIONPRODUCT.' OR type_promotion='.CATEGORYPROMOTIONPRODUCTALL.' OR type_promotion='.CATEGORYPROMOTIONPERCENTAGE.' OR type_promotion='.CATEGORYPROMOTIONPERCENTAGEALL.';';
		$Promotions=$wpdb->get_results($queryPromotions);
		foreach ($Promotions as $key => $promotion) {
			$queryPromotions='SELECT ID_user FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			$users=$wpdb->get_results($queryPromotions);
			$queryPromotions='SELECT sku FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			$categories=$wpdb->get_results($queryPromotions);
			unset($usersID);
			foreach ($users as $key => $item) {
				$usersID[]=$item->ID_user;
			}
			   $queryPromotions='SELECT sku FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			   $products=$wpdb->get_results($queryPromotions);
			   unset($productsSKU);
			   foreach ($products as $key => $item) {
				$productsSKU[]=$item->sku;
			}
			$promotionCategoryDisplay = new \stdClass; 
			$promotionCategoryDisplay->typePromotion=$promotion->type_promotion;
			$promotionCategoryDisplay->pharmacies=$usersID;
			$promotionCategoryDisplay->numberProducts=$promotion->number;
			$promotionCategoryDisplay->promotion=$promotion->promotion;
			$promotionCategoryDisplay->productGifted=$promotion->sku_gift;
			$promotionCategoryDisplay->ID=$promotion->ID;
			$promotionCategoryDisplay->categories=$productsSKU;
			$promotionCategoryDisplay->date=$promotion->date_promotion;
			$promotionCategoryDisplay->dateStart=$promotion->dateStart;
			$promotionCategoryDisplay->dateEnd=$promotion->dateEnd;
			$promotions[]=$promotionCategoryDisplay;
		}
		
		
		return $promotions;
	}

	/**
	 * gets all check sum promotions.
	 *
	 * @since    1.0.0
	 * 
	 * @return    array    $promotions    return all gcheck sum promotions.
	 */

	public function getlistPrices(){
		global $wpdb;
		$promotions=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$queryPromotions='SELECT Distinct ID,type_promotion,dateStart,dateEnd,date_promotion FROM '.$promotionsTable.' WHERE type_promotion='.LISTPRICESALL.' OR type_promotion='.LISTPRICES.' ;';
		$Promotions=$wpdb->get_results($queryPromotions);
		foreach ($Promotions as $key => $promotion) {
			$queryPromotions='SELECT ID_user FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			$users=$wpdb->get_results($queryPromotions);
			unset($usersID);
			foreach ($users as $key => $item) {
				$usersID[]=$item->ID_user;
			}
			$queryPrices='SELECT Distinct sku,promotion FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			$prices=$wpdb->get_results($queryPrices);
			unset($pricesList);
			foreach ($prices as $key => $item) {
				$price = new \stdClass; 
				$price->sku=$item->sku;
				$price->price=$item->promotion;
				$pricesList[]=$price;
			}
			$promotionListPricesDisplay = new \stdClass; 
			$promotionListPricesDisplay->typePromotion=$promotion->type_promotion;
			$promotionListPricesDisplay->pharmacies=$usersID;
			$promotionListPricesDisplay->ID=$promotion->ID;
			$promotionListPricesDisplay->prices=$pricesList;
			$promotionListPricesDisplay->date=$promotion->date_promotion;
			$promotionListPricesDisplay->dateStart=$promotion->dateStart;
			$promotionListPricesDisplay->dateEnd=$promotion->dateEnd;
			$promotions[]=$promotionListPricesDisplay;
		}
		
		
		return $promotions;
	}

	/**
	 * Creates exclution of products per user.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */

	public function createProductExclution($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$max_promotionID=$wpdb->get_results('SELECT MAX(ID) as id FROM '.$wpdb->prefix.'cp_promotions',OBJECT);
		$max_promotionID=$max_promotionID[0]->id+1;
		$typePromotion=PRODUCTLIMITATION;
		
		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=PRODUCTLIMITATIONALL;
		}

		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0){
				foreach ($promotion->products as $key => $product) {
					if ($product!=null) {
						$wpdb->insert($wpdb->prefix.'cp_promotions', array(
							'ID' => $max_promotionID,
							'ID_user' => $pharmacy->id,
							'type_promotion' => $typePromotion,
							'sku' => $product,
							'date_promotion' => $current_date_promotion
						));
					}			
				}	
			}
		}
		return true;
	}

	 /**
	* gets all excluded products per users.
	*
	* @since    1.0.0
	* 
	* @return    array    $promotions    return all gcheck sum promotions.
	*/

	public function getAllProductExcluded(){
		global $wpdb;
		$promotions=null;
		$promotionsTable=$wpdb->prefix.'cp_promotions';
		$queryPromotions='SELECT Distinct ID,type_promotion,number,promotion,date_promotion FROM '.$promotionsTable.' WHERE type_promotion='.PRODUCTLIMITATION.' OR type_promotion='.PRODUCTLIMITATIONALL.'  ;';
		$Promotions=$wpdb->get_results($queryPromotions);
		foreach ($Promotions as $key => $promotion) {
			$queryPromotions='SELECT ID_user FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			$users=$wpdb->get_results($queryPromotions);
			unset($usersID);
			foreach ($users as $key => $item) {
			 $usersID[]=$item->ID_user;
		 }
			$queryPromotions='SELECT sku FROM '.$promotionsTable.' WHERE ID='.$promotion->ID.' ;';
			$products=$wpdb->get_results($queryPromotions);
			unset($productsSKU);
			foreach ($products as $key => $item) {
			 $productsSKU[]=$item->sku;
		 }
			$promotionProductDisplay = new \stdClass; 
			$promotionProductDisplay->typePromotion=$promotion->type_promotion;
			$promotionProductDisplay->pharmacies=$usersID;
			$promotionProductDisplay->ID=$promotion->ID;
			$promotionProductDisplay->products=$productsSKU;
			$promotionProductDisplay->date=$promotion->date_promotion;
			$promotions[]=$promotionProductDisplay;
		}
		
		
		return $promotions;
	}

	/**
	 * Updates exclutions of products.
	 *
	 * @since    1.0.0
	 * @param     object    $promotion      The promotion that should be stored.
	 * 
	 * @return    boolean    $querystatus    return that the query was succesfully created.
	 */

	public function updateProductExclution($promotion) {
		global $wpdb;
		$current_date_promotion=date('Y-m-d h:i:s ', time());
		$this->deletePromotion($promotion->ID);
		$typePromotion=PRODUCTLIMITATION;

		if ($promotion->typePharmacySelected=='All') {
			$promotion->pharmacies=$this->getAllPharmacies()->listPharmacies;
			$typePromotion=PRODUCTLIMITATIONALL;
		}
		
		foreach ($promotion->pharmacies as $key => $pharmacy) {
			if ($pharmacy!=null && sizeof(get_object_vars($pharmacy))>0) {
				foreach ($promotion->products as $key => $product) {
					if ($product!=null) {
						$wpdb->insert($wpdb->prefix.'cp_promotions', array(
							'ID' => $promotion->ID,
							'ID_user' => $pharmacy->id,
							'type_promotion' => $typePromotion,
							'sku' => $product,
							'date_promotion' => $current_date_promotion
						));
					}	
				}	
			}
		}
		return true;
	}

}