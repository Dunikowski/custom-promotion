<?php

/**
 * This class is the interface to the database it helps extracting ,filtering, updating and deleting data 
 * from database.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes/database-controllers
 */

class DB_Controller {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The database controller for admin of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbControllerAdmin    the database controller for admin of this plugin.
	 */
	private $dbControllerAdmin;

	/**
	 * The database controller for public of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbControllerPublic    the database controller for Public of this plugin.
	 */
	private $dbControllerPublic;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->load_dependencies();
		$this->dbControllerAdmin = new DB_Controller_Admin( $this->plugin_name, $this->version );
        $this->dbControllerPublic = new DB_Controller_Public( $this->plugin_name, $this->version );
	}
	
    private function load_dependencies() {

		/**
		 * The class responsible for Data base controllers.
		 */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'database-controllers/class-custom-promotion-db-controller-admin.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'database-controllers/class-custom-promotion-db-controller-public.php';  

	}

	/**
	 * Returns the database controller for admin private object for use in other object.
	 *
	 * @since    1.0.0
	 */
	public function getControllerAdmin(){
		# returning the object settings
		return $this->dbControllerAdmin;
	}

	/**
	 * Returns the database controller for public (private) object for use in other object.
	 *
	 * @since    1.0.0
	 */
	public function getControllerPublic(){
		# returning the object settings
		return $this->dbControllerPublic;
	}
    
}