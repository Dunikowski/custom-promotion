<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 * @author     Cumulus Team <mustapha@agencjacumulus.pl>
 */
class Custom_Promotion_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'custom-promotion',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}