<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 * @author     Cumulus Team <mustapha@agencjacumulus.pl>
 */
class Custom_Promotion {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Custom_Promotion_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;
 
	/**
	 * The database controller of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $dbController  the database controller of this plugin.
	 */
	private $dbController;

	

	/**
	 * The admin class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $customPromotionAdmin  the admin of this plugin.
	 */
	private $customPromotionAdmin;

	/**
	 * The public class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      object    $customPromotionPublic  the admin of this plugin.
	 */
	private $customPromotionPublic;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'CUSTOM_PROMOTION_VERSION' ) ) {
			$this->version = CUSTOM_PROMOTION_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'custom-promotion';

		$this->load_dependencies();
		
		//Instanciation of the objects needed in the plugin
		$this->loader = new Custom_Promotion_Loader();
		$this->dbController = new DB_Controller( $this->plugin_name, $this->version );
		$this->customPromotionAdmin = new Custom_Promotion_Admin( $this->get_plugin_name(), $this->get_version(), $this->dbController->getControllerAdmin() );
		$this->customPromotionPublic = new Custom_Promotion_Public( $this->get_plugin_name(), $this->get_version(), $this->dbController->getControllerPublic() );

		//end of instanciations
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_promotion_types();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Custom_Promotion_Loader. Orchestrates the hooks of the plugin.
	 * - Custom_Promotion_i18n. Defines internationalization functionality.
	 * - Custom_Promotion_Admin. Defines all hooks for the admin area.
	 * - Custom_Promotion_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-promotion-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-promotion-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-custom-promotion-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-custom-promotion-public.php';

		/**
		 * The class responsible for interacting with database.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/database-controllers/class-custom-promotion-db-controller.php';

		

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Custom_Promotion_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Custom_Promotion_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Define promotion types.
	 *
	 * Define all the promotion types used by the plugin
	 *
	 * @since    1.0.0
	 * @access   private
	 */

	private function define_promotion_types() {
		//Defines the variables needed in the plugin
		if(!defined('CHECKSUMPROMOTION')){
			define('CHECKSUMPROMOTION', 1);
		}
		if(!defined('PRODUCTPROMOTIONPRODUCT')){
			define('PRODUCTPROMOTIONPRODUCT', 2);
		}
		if(!defined('PRODUCTPROMOTIONPERCENTAGE')){
			define('PRODUCTPROMOTIONPERCENTAGE', 3);
		}
		if(!defined('CATEGORYPROMOTIONPRODUCT')){
			define('CATEGORYPROMOTIONPRODUCT', 4);
		}
		if(!defined('CATEGORYPROMOTIONPERCENTAGE')){
			define('CATEGORYPROMOTIONPERCENTAGE', 5);
		}
		if(!defined('LISTPRICES')){
			define('LISTPRICES', 6);
		}
		if(!defined('CHECKSUMPROMOTIONALL')){
			define('CHECKSUMPROMOTIONALL', 7);
		}
		if(!defined('PRODUCTPROMOTIONPRODUCTALL')){
			define('PRODUCTPROMOTIONPRODUCTALL', 8);
		}
		if(!defined('PRODUCTPROMOTIONPERCENTAGEALL')){
			define('PRODUCTPROMOTIONPERCENTAGEALL', 9);
		}
		if(!defined('CATEGORYPROMOTIONPRODUCTALL')){
			define('CATEGORYPROMOTIONPRODUCTALL', 10);
		}
		if(!defined('CATEGORYPROMOTIONPERCENTAGEALL')){
			define('CATEGORYPROMOTIONPERCENTAGEALL', 11);
		}
		if(!defined('LISTPRICESALL')){
			define('LISTPRICESALL', 12);
		}
		if(!defined('PRODUCTAPPLIED')){
			define('PRODUCTAPPLIED', 13);
		}
		if(!defined('CATEGORYAPPLIED')){
			define('CATEGORYAPPLIED', 14);
		}
		if(!defined('MINIMUMSUM')){
			define('MINIMUMSUM', 15);
		}
		if(!defined('MINIMUMSUMALL')){
			define('MINIMUMSUMALL', 16);
		}
		if(!defined('PRODUCTLIMITATION')){
			define('PRODUCTLIMITATION', 17);
		}
		if(!defined('PRODUCTLIMITATIONALL')){
			define('PRODUCTLIMITATIONALL', 18);
		}
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$this->loader->add_action( 'admin_enqueue_scripts', $this->customPromotionAdmin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $this->customPromotionAdmin, 'enqueue_scripts' );

		$this->loader->add_action( 'admin_menu', $this->customPromotionAdmin->getMainPage(), 'setup_plugin_main_page' );

	}
	

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$this->loader->add_action( 'wp_enqueue_scripts', $this->customPromotionPublic, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $this->customPromotionPublic, 'enqueue_scripts' );

		$this->loader->add_action( 'woocommerce_new_order',$this->customPromotionPublic->getPromotionCalculator(), 'generate_xml_file',10,1 );
		$this->loader->add_filter( 'woocommerce_cart_item_name', $this->customPromotionPublic->getPromotionCalculator(), 'promotion_text_cart_item_name', 11, 3 );
		$this->loader->add_filter( 'woocommerce_cart_item_quantity', $this->customPromotionPublic->getPromotionCalculator(), 'promotion_text_cart_item_quantity', 10, 2 );
		$this->loader->add_filter( 'woocommerce_cart_item_remove_link', $this->customPromotionPublic->getPromotionCalculator(),'customized_cart_item_remove_link', 20, 2 );
		$this->loader->add_action( 'pre_get_posts', $this->customPromotionPublic->getPromotionCalculator(),'exclude_product_category');

		$this->loader->add_filter( 'woocommerce_is_purchasable', $this->customPromotionPublic->getPromotionCalculator(), 'product_is_purchasable', 10, 2);
		$this->loader->add_action( 'wp_login', $this->customPromotionPublic->getPromotionCalculator(), 'cart_contents_check', 10, 2);
		$this->loader->add_action( 'woocommerce_check_cart_items', $this->customPromotionPublic->getPromotionCalculator() , 'set_minimum_checkout_sum',10,1);
		$this->loader->add_action( 'woocommerce_remove_cart_item', $this->customPromotionPublic->getPromotionCalculator(), 'item_removed_from_cart' , 11, 2);
		$this->loader->add_filter( 'woocommerce_get_price_html', $this->customPromotionPublic->getPromotionCalculator(), 'apply_list_prices_html_filter',9999,2 );
		$this->loader->add_action( 'woocommerce_before_calculate_totals', $this->customPromotionPublic->getPromotionCalculator(), 'apply_list_prices' );
		$this->loader->add_action( 'woocommerce_after_cart_item_quantity_update', $this->customPromotionPublic->getPromotionCalculator(), 'apply_promotion_product_update',20, 4 );
		$this->loader->add_action( 'woocommerce_before_cart_item_quantity_zero', $this->customPromotionPublic->getPromotionCalculator(), 'apply_promotion_product_update_zero',10, 2 );
		$this->loader->add_action( 'woocommerce_add_to_cart', $this->customPromotionPublic->getPromotionCalculator(), 'apply_promotion_product', 10, 6 );
		$this->loader->add_action( 'woocommerce_cart_calculate_fees', $this->customPromotionPublic->getPromotionCalculator(), 'apply_fees' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Custom_Promotion_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
} 