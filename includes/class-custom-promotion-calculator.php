<?php

/**
 * Apply and calculates promotions.
 *
 * Contains woocommerce hooks for cart manipulations and calculating promotions.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 */

/**
 * Apply and calculates promotions.
 *
 * Contains woocommerce hooks for cart manipulations and calculating promotions.
 *
 * @since      1.0.0
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 * @author     Cumulus Team <mustapha@agencjacumulus.pl>
 */
class Custom_Promotion_Calculator {

    /**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The database controller for public of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbControllerPublic    the database controller for Public of this plugin.
	 */
	private $dbControllerPublic;

    /**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name           The name of this plugin.
	 * @param      string    $version               The version of this plugin.
     * @param      object    $dbControllerPublic    the database controller for Public of this plugin.
	 */
	public function __construct( $plugin_name, $version , $dbControllerPublic) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->dbControllerPublic = $dbControllerPublic;

	}

	/**
	 * Apply list of prices promotion for html display.
	 *
	 * @since    1.0.0
	 */
	
	public function apply_list_prices_html_filter($price_html, $product){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		//if ( ! wc_current_user_has_role( 'customer' ) ) return;
		$promotionsExisting='';
		$promotionsListed[]=null;
		$excluded=false;
		$listProducts=null;
		$listProductsByCategories=null;
		$ID_Pharmacy =wp_get_current_user()->ID;
		$listPrices = $this->dbControllerPublic->getAllListPricesByPharmacy($ID_Pharmacy);
		$productSKU = $product->get_sku();
		if ($listPrices!=null && sizeof($listPrices)>0) {
			foreach ($listPrices as $key => $item) {
				if ($productSKU==$item->sku) {
					$price_html = wc_price( $item->promotion );
				}
			}
		}

		$listProductsExcluded = $this->dbControllerPublic->getAllProductsExcluded($ID_Pharmacy);
		if ($listProductsExcluded!=null  && sizeof($listProductsExcluded)>0) {
			foreach ($listProductsExcluded as $key => $item) {
				if ($productSKU==$item->sku) {
					$price_html = ' ';
					$promotionsExisting.=__(' Nie możesz kupić tego produktu. ','custom-promotion');
					$excluded=true;
				}
			}
		}

		if (!$excluded) {
			$listProducts = $this->dbControllerPublic->getAllProductsPromoted($ID_Pharmacy);
			$listProductsByCategories = $this->dbControllerPublic->getAllCategoriesPromoted($ID_Pharmacy);
		}
		

		if ($listProducts!=null  && sizeof($listProducts)>0) {
			foreach ($listProducts as $key => $item) {
				if ($productSKU==$item->sku) {
					$result1=__('produktów','custom-promotion');
    					$result2=__('produkty','custom-promotion');
    					$result3=__('produkt','custom-promotion');
						$howManyPosts=$item->number;
						$postsCounter=$howManyPosts;
						if ($howManyPosts<5) {
							# code...
							if ($howManyPosts==0) {
								# code...
								$output = $result1;
							}elseif ($howManyPosts==1) {
								# code...
								$output = $result3;
							}elseif ($howManyPosts==2 || $howManyPosts==3 || $howManyPosts==4) {
								# code...
								$output = $result2;
							}
						}elseif ($howManyPosts>=5 && $howManyPosts<=21) {
							# code...
							$output = $result1;
						}elseif ($howManyPosts>21) {
							# code...
							while ($postsCounter > 31) {
								$postsCounter=$postsCounter-10;
							}
							if ($postsCounter>=25 && $postsCounter<=31) {
								# code...
								$output =$result1;
							}elseif ($postsCounter>=22 && $postsCounter<=24) {
								# code...
								$output = $result2;
							}

						}
					if (intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCT || intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCTALL) {
						$thisPromotion=sprintf(__(' <br> Kup %s %s, a %s otrzymasz za 1 grosz ','custom-promotion'), 
						$item->number,
						$output,
						$item->promotion
						);
						if (!in_array($thisPromotion,$promotionsListed)) {
							$promotionsExisting.='<span class="custom_promotions_item">'.$thisPromotion.'</span>';
							$promotionsListed[]=$thisPromotion;
						}
					}else {
						$thisPromotion=sprintf(__(' <br> Kup %s %s, a otrzymasz rabat w wysokości %s%s ','custom-promotion'), 
						$item->number,
						$output,
						$item->promotion,'%'
						);
						if (!in_array($thisPromotion,$promotionsListed)) {
							$promotionsExisting.='<span class="custom_promotions_item">'.$thisPromotion.'</span>';
							$promotionsListed[]=$thisPromotion;
						}
					}
				}
			}
		}

		if ($listProductsByCategories!=null  && sizeof($listProductsByCategories)>0) {
			foreach ($listProductsByCategories as $key => $item) {
				if ($productSKU==$item->sku) {
					$result1=__('produktów','custom-promotion');
    					$result2=__('produkty','custom-promotion');
    					$result3=__('produkt','custom-promotion');
						$howManyPosts=$item->number;
						$postsCounter=$howManyPosts;
						if ($howManyPosts<5) {
							# code...
							if ($howManyPosts==0) {
								# code...
								$output = $result1;
							}elseif ($howManyPosts==1) {
								# code...
								$output = $result3;
							}elseif ($howManyPosts==2 || $howManyPosts==3 || $howManyPosts==4) {
								# code...
								$output = $result2;
							}
						}elseif ($howManyPosts>=5 && $howManyPosts<=21) {
							# code...
							$output = $result1;
						}elseif ($howManyPosts>21) {
							# code...
							while ($postsCounter > 31) {
								$postsCounter=$postsCounter-10;
							}
							if ($postsCounter>=25 && $postsCounter<=31) {
								# code...
								$output =$result1;
							}elseif ($postsCounter>=22 && $postsCounter<=24) {
								# code...
								$output = $result2;
							}

						}
					if (intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCT || intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCTALL) {
						$thisPromotion=sprintf(__(' <br> Kup %s %s, a %s otrzymasz za 1 grosz ','custom-promotion'), 
						$item->number,
						$output,
						$item->promotion
						);
						if (!in_array($thisPromotion,$promotionsListed)) {
							$promotionsExisting.='<span class="custom_promotions_item">'.$thisPromotion.'</span>';
							$promotionsListed[]=$thisPromotion;
						}
					}else {
						$thisPromotion=sprintf(__(' <br> Kup %s %s, a otrzymasz rabat w wysokości %s%s ','custom-promotion'), 
						$item->number,
						$output,
						$item->promotion,'%'
						);
						if (!in_array($thisPromotion,$promotionsListed)) {
							$promotionsExisting.='<span class="custom_promotions_item">'.$thisPromotion.'</span>';
							$promotionsListed[]=$thisPromotion;
						}
					}
				}
			}
		}

		return $price_html.'<span class="custom_promotions">'.$promotionsExisting.'</span>';
	}

	
 	 /**
	 * Check cart contents.
	 *
	 * @since    1.0.0
	 */
	
	public function cart_contents_check($user_login, $user ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		//if ( ! wc_current_user_has_role( 'customer' ) ) return;

		$ID_Pharmacy =wp_get_current_user()->ID;
		if (WC()->cart->is_empty()) {
			$this->dbControllerPublic->deleteTemporaryCart($ID_Pharmacy);
		}
	}

    /**
	 * Apply list of prices promotion.
	 *
	 * @since    1.0.0
	 */
	
	public function apply_list_prices($cart ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		//if ( ! wc_current_user_has_role( 'customer' ) ) return;

		$ID_Pharmacy =wp_get_current_user()->ID;
		$listPrices = $this->dbControllerPublic->getAllListPricesByPharmacy($ID_Pharmacy);

		// Loop through cart and set new list of prices
		foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {
			$productSKU = $cart_item['data']->get_sku();
			if ($listPrices!=null && sizeof($listPrices)>0) {
				foreach ($listPrices as $key => $item) {
					if ($productSKU==$item->sku) {
						$cart_item['data']->set_price( $item->promotion );
					}
				}
			}
		}
		$listAppliedPromotions = $this->dbControllerPublic->getListPromotionsApplied($ID_Pharmacy);
		if($listAppliedPromotions!=null  && sizeof($listAppliedPromotions)>0) {
			foreach ($listAppliedPromotions as $key => $appliedPromotions) {
				if (intval($appliedPromotions->type_promotion)==CATEGORYPROMOTIONPERCENTAGEALL ||intval($appliedPromotions->type_promotion)== PRODUCTPROMOTIONPERCENTAGEALL||intval($appliedPromotions->type_promotion)==CATEGORYPROMOTIONPERCENTAGE ||intval($appliedPromotions->type_promotion)==PRODUCTPROMOTIONPERCENTAGE ) {
					foreach ($cart->get_cart() as $cart_item_key => $cart_item ) {
						if(intval($cart_item['variation_id'])==0){
			
							$id_product_=intval($cart_item['product_id']);
						}else {
				
							$id_product_=intval($cart_item['variation_id']);
						}
						if ($id_product_==intval($appliedPromotions->ID_product)) {
							$cart_item['data']->set_price($cart_item['data']->get_price()- floatval($cart_item['data']->get_price())*intval($appliedPromotions->promotion)/100 );
						}
					}
				}
			}
		}
	}

	/**
	 * Apply filter of excluded product.
	 *
	 * @since    1.0.0
	 */

	function product_is_purchasable($is_purchasable, $product ) {

		$productSKU = $product->get_sku();
		$ID_Pharmacy =wp_get_current_user()->ID;

		$listProductsExcluded = $this->dbControllerPublic->getAllProductsExcluded($ID_Pharmacy);

		if ($listProductsExcluded!=null  && sizeof($listProductsExcluded)>0) {
			foreach ($listProductsExcluded as $key => $item) {
				if ($productSKU==$item->sku) {
					return false;
				}
			}
		}

		return $is_purchasable;
	}

	/**
	 * Delete promotion applied if the item is deleted.
	 *
	 * @since    1.0.0
	 */
	
	public function item_removed_from_cart($cart_item_key , $instance){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		
		if(intval(WC()->cart->cart_contents[$cart_item_key ]['variation_id'])==0){
			
			$id_product_=intval(WC()->cart->cart_contents[$cart_item_key ]['product_id']);
		}else {

			$id_product_=intval(WC()->cart->cart_contents[$cart_item_key ]['variation_id']);
		}
		$ID_Pharmacy =wp_get_current_user()->ID;

		$listAppliedPromotions = $this->dbControllerPublic->getListPromotionsApplied($ID_Pharmacy);
		if ($listAppliedPromotions!=null  && sizeof($listAppliedPromotions)>0) {
			foreach ($listAppliedPromotions as $key => $appliedPromotions) {
				if ($id_product_==intval($appliedPromotions->ID_product)) {
					foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
						$giftItemKey=$key;
						if(intval($itemMaybeGift['variation_id'])==0){
							$id_product_gift=intval($itemMaybeGift['product_id']);
						}else {
							$id_product_gift=intval($itemMaybeGift['variation_id']);
						}
						if (intval($id_product_gift)==intval($appliedPromotions->ID_product_gift)) {
							$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_ );
							$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($id_product_gift,$id_product_);
							WC()->cart->set_quantity( $giftItemKey,intval($quantityAdded), true );
						} 
					}
				}
			}
		}
	}

	/**
	 * Set minimum checkout sum per user.
	 *
	 * @since    1.0.0
	 */
	
	public function set_minimum_checkout_sum($wccs_custom_checkout_field_pro_process){
		
		$minimum = 250;
		$ID_Pharmacy =wp_get_current_user()->ID;
		$minimSum = $this->dbControllerPublic->getLastMinimumSum($ID_Pharmacy);
		$data_str = print_r( $minimSum , true );
		if ($minimSum!=null) {
			$minimum =intval($minimSum->minimumSum);
		}
		
		$total = round(WC()->cart->total, 2);
		if ($total < $minimum) {
			if (is_cart()) {
				wc_add_notice(
					sprintf(
						__('Zamówienie powinno wynosić minimalnie %s. netto Teraz Twoje zamówienie wynosi %s. netto', 'custom-promotion'),
						wc_price($minimum),
						wc_price($total)
					),
					'error'
				);
			} else {
				wc_add_notice(
					sprintf(
						__('Zamówienie powinno wynosić minimalnie %s. netto Teraz Twoje zamówienie wynosi %s. netto', 'custom-promotion'),
						wc_price($minimum),
						wc_price($total)
					),
					'error'
				);
			}
		}
	}

	/**
	 * Delete promotion applied if the quantity is made zero.
	 *
	 * @since    1.0.0
	 */
	
	public function apply_promotion_product_update_zero($cart_item_key,$cart ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;

		if(intval(WC()->cart->cart_contents[$cart_item_key ]['variation_id'])==0){
			
			$id_product_=intval(WC()->cart->cart_contents[$cart_item_key ]['product_id']);
		}else {

			$id_product_=intval(WC()->cart->cart_contents[$cart_item_key ]['variation_id']);
		}
		$ID_Pharmacy =wp_get_current_user()->ID;
		$listAppliedPromotions = $this->dbControllerPublic->getListPromotionsApplied($ID_Pharmacy);
		if ($listAppliedPromotions!=null  && sizeof($listAppliedPromotions)>0) {
			foreach ($listAppliedPromotions as $key => $appliedPromotions) {
				if ($id_product_==intval($appliedPromotions->ID_product)) {
					foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
						$giftItemKey=$key;
						if(intval($itemMaybeGift['variation_id'])==0){
							$id_product_gift=intval($itemMaybeGift['product_id']);
						}else {
							$id_product_gift=intval($itemMaybeGift['variation_id']);
						}
						if (intval($id_product_gift)==intval($appliedPromotions->ID_product_gift)) {
							$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_ );
							$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($id_product_gift,$id_product_);
							WC()->cart->set_quantity( $giftItemKey,intval($quantityAdded), true );
						} 
					}
				}
			}
		}
	}

	/**
	 * Apply promotions per products to the cart after deleting all promotions added before.
	 *
	 * @since    1.0.0
	 */
	
	public function apply_promotion_product_update($cart_item_key, $quantity, $old_quantity, $cart ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		if ($quantity == $old_quantity) return;
		if ( did_action( 'woocommerce_add_to_cart' ) >= 1 ) return;
		if ( did_action( 'woocommerce_after_cart_item_quantity_update' ) >= 2 ) return;
		//if ( ! wc_current_user_has_role( 'customer' ) ) return;
		$addition=true;
		
		if ($quantity<$old_quantity) $addition=false;
		$ID_Pharmacy =wp_get_current_user()->ID;
		$variable=false;
		$listProducts = $this->dbControllerPublic->getAllProductsPromoted($ID_Pharmacy);
		$listAppliedPromotions = $this->dbControllerPublic->getListPromotionsApplied($ID_Pharmacy);

		$quantityInCart=  $quantity;
		$productSKU = $cart->get_cart_item( $cart_item_key )['data']->get_sku();
		
		$giftExist=false;
		if(intval($cart->cart_contents[$cart_item_key ]['variation_id'])==0){
			$id_product_=intval($cart->cart_contents[$cart_item_key ]['product_id']);
		}else {
			$variable=true;
			$id_product_=intval($cart->cart_contents[$cart_item_key ]['variation_id']);
			$variation_id =  $id_product_;
			$product_id=wp_get_post_parent_id( $id_product_ );
		}
		
		$id_product_added =  $id_product_;
		$giftItemKey=null;
		
		if ($listProducts!=null  && sizeof($listProducts)>0) {
			foreach ($listProducts as $key => $item) {
				$beenInside=false;
				if ($productSKU==$item->sku) {
					if ($listAppliedPromotions!=null  && sizeof($listAppliedPromotions)>0) {
						foreach ($listAppliedPromotions as $key => $appliedPromotions) {
							if ($id_product_added==intval($appliedPromotions->ID_product)) {
								
								if (intval($item->row_ID)==intval($appliedPromotions->ID_promotion)) {
									
									$beenInside=true;
									if (intval($appliedPromotions->product_category)==PRODUCTAPPLIED) {
										
										//check type of promotion
										if (intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCT || intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCTALL) {
											
											//Check to edit quantity
											if ($quantity>=intval($item->number)) {
												
												//get the item key of the gift product
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														
														$giftItemKey=$key;
														$giftExist=true;
														//update in table and add product
														$this->dbControllerPublic->updateTemporaryPromotion($appliedPromotions->ID,intval($quantity/intval($item->number))*intval($item->promotion),$quantity);
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
														return;
													} 
												}	
												if (!$giftExist) {
													$giftExist=true;
													$productSKUToBeAdded=$item->sku_gift;
													$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
													$parent_id = wp_get_post_parent_id( $variationToBeAdded );
													if ($parent_id > 0) {
														$variable=true;
													}else {
														$variable=false;
													}
													if($variable){
														
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
														if (intval($quantityAdded)==0) {
															WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}else {
														$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
														if ($quantityAdded==0) {
															WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}
												}
											}else {
												
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														
														$giftItemKey=$key;
														$giftExist=false;
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ 0, true );
														$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
														if ($quantity<=0) {
															
															return;
														}
													} 
												}	
											}
										}else {
											
											//check if total quantity is more than quantity of promotion update table
											if ($quantity>=intval($item->number)) {
												
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku)) {
														
														$giftItemKey=$key;
														$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
														$this->dbControllerPublic->updateTemporaryPromotion($appliedPromotions->ID,$quantity,$quantity);
														return;
													} 
												}
											}else {
												
												$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
												if ($quantity<=0) {
													
													return;
												}
											}
											
										}
									}else {
										
										//Delete existing line and add new one
										$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
										if (intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCT || intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCTALL) {
											//Check to edit quantity
											
											if ($quantity>=intval($item->number)) {
												
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														$giftItemKey=$key;
														
														$giftExist=true;
														//update in table and add product
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
														$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/intval($item->number))*intval($item->promotion),$item->promotion,$quantity);
														return;
													} 
												}	
												if (!$giftExist) {
													$giftExist=true;
													$productSKUToBeAdded=$item->sku_gift;
													$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
													$parent_id = wp_get_post_parent_id( $variationToBeAdded );
													if ($parent_id > 0) {
														$variable=true;
													}else {
														$variable=false;
													}
													if($variable){
														
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
														if (intval($quantityAdded)==0) {
															WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}else {
														$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
														if ($quantityAdded==0) {
															WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}
												}
											}
										}else {
											
											//check if total quantity is more than quantity of promotion update table
											if ($quantity>=intval($item->number)) {
												
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku)) {
														$giftItemKey=$key;
														
														//update in table and add product
														$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
														$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',PRODUCTAPPLIED,$quantity,$item->promotion,$quantity);	
														return;
													} 
												}	
											}
										}
									}
								}else {
									
									$beenInside=true;
									$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
									if (intval($appliedPromotions->product_category)==PRODUCTAPPLIED) {
										
										//check type of promotion
										if (intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCT || intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCTALL) {
											//Check to edit quantity
											
											if ($quantity>=intval($item->number)) {
												
												//get the item key of the gift product
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														
														$giftItemKey=$key;
														$giftExist=true;
														//update in table and add product
														$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/intval($item->number))*intval($item->promotion),$item->promotion,$quantity);
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
														return;
													} 
												}	
												if (!$giftExist) {
													$giftExist=true;
													$productSKUToBeAdded=$item->sku_gift;
													$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
													$parent_id = wp_get_post_parent_id( $variationToBeAdded );
													if ($parent_id > 0) {
														$variable=true;
													}else {
														$variable=false;
													}
													if($variable){
														
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
														if (intval($quantityAdded)==0) {
															WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}else {
														$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
														if ($quantityAdded==0) {
															WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}
												}
											}else {
												
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														$giftItemKey=$key;
														
														$giftExist=false;
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ 0, true );
														
														if ($quantity<=0) {
															
															return;
														}
													} 
												}	
												continue;
											}
										}else {
											
											//check if total quantity is more than quantity of promotion update table
											if ($quantity>=intval($item->number)) {
												
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku)) {
														
														$giftItemKey=$key;
														$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
														$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',PRODUCTAPPLIED,$quantity,$item->promotion,$quantity);
														return;
													} 
												}
											}else {
												
												$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
												continue;
												if ($quantity<=0) {
													
													return;
												}
											}
											
										}
									}else {
										
										//Delete existing line and add new one
										$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
										if (intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCT || intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCTALL) {
											//Check to edit quantity
											
											if ($quantity>=intval($item->number)) {
												
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														$giftItemKey=$key;
														$giftExist=true;
														
														//update in table and add product
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
														$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/intval($item->number))*intval($item->promotion),$item->promotion,$quantity);
														return;
													} 
												}	
												if (!$giftExist) {
													$giftExist=true;
													$productSKUToBeAdded=$item->sku_gift;
													$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
													$parent_id = wp_get_post_parent_id( $variationToBeAdded );
													if ($parent_id > 0) {
														$variable=true;
													}else {
														$variable=false;
													}
													if($variable){
														
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
														if (intval($quantityAdded)==0) {
															WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}else {
														$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
														if ($quantityAdded==0) {
															WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}
												}
											}
										}else {
											
											//check if total quantity is more than quantity of promotion update table
											if ($quantity>=intval($item->number)) {
												
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku)) {
														
														$giftItemKey=$key;
														//update in table and add product
														$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
														$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',PRODUCTAPPLIED,$quantity,$item->promotion,$quantity);	
														return;
													} 
												}	
											}
										}
									}
									continue;
								}
							}
						}
					}
					if ($beenInside) {
						
						if(intval($item->number)<=$quantity){
							
							if ($item->type_promotion==PRODUCTPROMOTIONPRODUCT || $item->type_promotion==PRODUCTPROMOTIONPRODUCTALL ) {
								$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
								
								$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
								foreach ($cart->get_cart() as $key => $itemMaybeGift) {
									if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
										$giftItemKey=$key;
										
										$giftExist=true;
										//update in table and add product
										$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
										$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/intval($item->number))*intval($item->promotion),$item->promotion,$quantity);
										return;
									} 
								}	
								if (!$giftExist) {
									$giftExist=true;
									$productSKUToBeAdded=$item->sku_gift;
									$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
									$parent_id = wp_get_post_parent_id( $variationToBeAdded );
									if ($parent_id > 0) {
										$variable=true;
									}else {
										$variable=false;
									}
									if($variable){
										
										$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
										if (intval($quantityAdded)==0) {
											WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
											$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										}else {
											foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
												if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
													$giftItemKey=$key;
													$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
													$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
												} 
											}
										}
									}else {
										$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
										$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
										if ($quantityAdded==0) {
											WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
											$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										}else {
											foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
												if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
													$giftItemKey=$key;
													$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
													$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
												} 
											}
										}
									}
								}
							}else {
								
								foreach ($cart->get_cart() as $key => $itemMaybeGift) {
									if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku)) {
										$giftItemKey=$key;
										
										//update in table and add product
										$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
										$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',PRODUCTAPPLIED,$quantity,$item->promotion,$quantity);	
										return;
									} 
								}	
							}
							
						}
					}else {
						
						if(intval($item->number)<=$quantity){
							
							if ($item->type_promotion==PRODUCTPROMOTIONPRODUCT || $item->type_promotion==PRODUCTPROMOTIONPRODUCTALL ) {
								
								
								$productSKUToBeAdded=$item->sku_gift;
								$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
								$parent_id = wp_get_post_parent_id( $variationToBeAdded );
								if ($parent_id > 0) {
									$variable=true;
								}else {
									$variable=false;
								}
								if($variable){
									
									$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
									$parent_id = wp_get_post_parent_id( $variationToBeAdded );
									$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
									if (intval($quantityAdded)==0) {
										
										WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
										$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										return;
									}else {
										
										foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
											if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
												$giftItemKey=$key;
												
												$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
												$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
												return;
											} 
										}
									}
								}else {
									
									$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
									$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
									if ($quantityAdded==0) {
										
										WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
										$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										return;
									}else {
										
										foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
											if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
												$giftItemKey=$key;
												
												$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
												$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
												return;
											} 
										}
									}
								}
							}else {
								
								//update in table and add product
								$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
								$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',PRODUCTAPPLIED,$quantity,$item->promotion,$quantity);			
								return;
							}
						}
					}	
				}
			}
		}
 
		$listAppliedPromotions = $this->dbControllerPublic->getListPromotionsApplied($ID_Pharmacy);
		$listProductsByCategories = $this->dbControllerPublic->getAllCategoriesPromoted($ID_Pharmacy);

		$giftExist=false;
		if ($listProductsByCategories!=null && sizeof($listProductsByCategories)>0) {
			foreach ($listProductsByCategories as $key => $item) {
				$beenInside=false;
				if ($productSKU==$item->sku) {
					if ($listAppliedPromotions!=null  && sizeof($listAppliedPromotions)>0) {
						foreach ($listAppliedPromotions as $key => $appliedPromotions) {
							if ($id_product_added==intval($appliedPromotions->ID_product)) {


								//get the item key of the gift product
								if (intval($item->row_ID)==intval($appliedPromotions->ID_promotion)) {
									$beenInside=true;
									if (intval($appliedPromotions->product_category)==PRODUCTAPPLIED) {
										return;
									}else {
										//check type of promotion
										if (intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCT || intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCTALL) {
											//Check to edit quantity
											if ($quantity>=intval($item->number)) {
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														$giftItemKey=$key;
														$giftExist=true;
														//update in table and add product
														$this->dbControllerPublic->updateTemporaryPromotion($appliedPromotions->ID,intval($quantity/intval($item->number))*intval($item->promotion),$quantity);
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
														return;
													} 
												}
												if (!$giftExist) {
													$giftExist=true;
													$productSKUToBeAdded=$item->sku_gift;
													$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
													$parent_id = wp_get_post_parent_id( $variationToBeAdded );
													if ($parent_id > 0) {
														$variable=true;
													}else {
														$variable=false;
													}
													if($variable){
														
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
														if (intval($quantityAdded)==0) {
															WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}else {
														$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
														if ($quantityAdded==0) {
															WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}
												}
												return;
											}else {
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														$giftItemKey=$key;
														$giftExist=false;
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ 0, true );
														$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
														if ($quantity<=0) {
															return;
														}
													} 
												}
											}	
										}else {
											//check if total quantity is more than quantity of promotion update table
											//check if total quantity is more than quantity of promotion update table
											if ($quantity>=intval($item->number)) {




												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku)) {
														$giftItemKey=$key;
														$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
														$this->dbControllerPublic->updateTemporaryPromotion($appliedPromotions->ID,$quantity,$quantity);
														return;
													} 
												}
											}else {
												$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
												if ($quantity<=0) {
													return;
												}
											}
											
										}
									}
								}else {
									$beenInside=true;
									$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
									if (intval($appliedPromotions->product_category)==PRODUCTAPPLIED) {
										return;
									}else {
										
										//Delete existing line and add new one
										$this->dbControllerPublic->deleteTemporaryItem( $ID_Pharmacy, $id_product_added );
										if (intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCT || intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCTALL) {
											//Check to edit quantity
											
											if ($quantity>=intval($item->number)) {
												
												$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
												$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
														$giftItemKey=$key;
														$giftExist=true;
														
														//update in table and add product
														$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
														$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/intval($item->number))*intval($item->promotion),$item->promotion,$quantity);
														return;
													} 
												}	
												if (!$giftExist) {
													$giftExist=true;
													$productSKUToBeAdded=$item->sku_gift;
													$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
													$parent_id = wp_get_post_parent_id( $variationToBeAdded );
													if ($parent_id > 0) {
														$variable=true;
													}else {
														$variable=false;
													}
													if($variable){
														
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
														if (intval($quantityAdded)==0) {
															WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}else {
														$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
														$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
														if ($quantityAdded==0) {
															WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
															$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
														}else {
															foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
																if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
																	$giftItemKey=$key;
																	$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
																	$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
																} 
															}
														}
													}
												}
												return;
											}
										} else {
											
											//check if total quantity is more than quantity of promotion update table
											if ($quantity>=intval($item->number)) {
												
												foreach ($cart->get_cart() as $key => $itemMaybeGift) {
													if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku)) {
														
														$giftItemKey=$key;
														//update in table and add product
														$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
														$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',CATEGORYAPPLIED,$quantity,$item->promotion,$quantity);	
														return;
													} 
												}	
												return;
											}
										}
									}
									continue;
								}
							}
						}
					}
					if ($beenInside) {
						if(intval($item->number)<=$quantity){
							if (intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCT || intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCTALL ) {
								$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($item->sku_gift);
								$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
								foreach ($cart->get_cart() as $key => $itemMaybeGift) {
									if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
										$giftItemKey=$key;
										$giftExist=true;
										$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
										$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/intval($item->number))*intval($item->promotion),$item->promotion,$quantity);
									} 
								}
								if (!$giftExist) {
									$giftExist=true;
									$productSKUToBeAdded=$item->sku_gift;
									$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
									$parent_id = wp_get_post_parent_id( $variationToBeAdded );
									if ($parent_id > 0) {
										$variable=true;
									}else {
										$variable=false;
									}
									if($variable){
										
										$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
										if (intval($quantityAdded)==0) {
											WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
											$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										}else {
											foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
												if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
													$giftItemKey=$key;
													$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
													$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);

												} 
											}
										}
									}else {
										$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
										$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
										if ($quantityAdded==0) {
											WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
											$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
											
										}else {
											foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
												if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
													$giftItemKey=$key;
													$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
													$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
													
												} 
											}
										}
									}
								}
							}else {
								foreach ($cart->get_cart() as $key => $itemMaybeGift) {
									if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku)) {
										$giftItemKey=$key;
										
										//update in table and add product
										$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
										$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',CATEGORYAPPLIED,$quantity,$item->promotion,$quantity);	
										return;
									} 
								}	





								
								
							}
							return;
						}
					}else {
						if(intval($item->number)<=$quantity){
							if (intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCT || intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCTALL ) {
								$productSKUToBeAdded=$item->sku_gift;
								$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
								$parent_id = wp_get_post_parent_id( $variationToBeAdded );
								if ($parent_id > 0) {
									$variable=true;
								}else {
									$variable=false;
								}
								if($variable){
									$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
									$parent_id = wp_get_post_parent_id( $variationToBeAdded );
									$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
									if(intval($quantityAdded)==0){
										WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
										$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
									}else {
										foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
											if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
												$giftItemKey=$key;
												$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
												$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
											} 
										}
									}	
								}else {
									$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
									$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
									if(intval($quantityAdded)==0){
										WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
										$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
									}else {
										foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
											if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
												$giftItemKey=$key;
												$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
												$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
											} 
										}
									}	
								}
							}else {
								$cart->get_cart_item( $cart_item_key )['data']->set_price($cart->get_cart_item( $cart_item_key )['data']->get_price()- floatval($cart->get_cart_item( $cart_item_key )['data']->get_price())*intval($item->promotion)/100 );
								$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',CATEGORYAPPLIED,$quantity,$item->promotion,$quantity);	
							}
							return;
						}
					}
				}
			}
		}
		
	}

	/**
	 * Apply promotions per products to the cart after deleting all promotions added before.
	 *
	 * @since    1.0.0
	 */
	
	public function apply_promotion_product($cart_item_key,  $product_id,  $quantity,  $variation_id,  $variation,  $cart_item_data ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		if ( did_action( 'woocommerce_add_to_cart' ) >= 2 ) return;
		
		if (WC()->cart->get_cart_item( $cart_item_key )['quantity']!=$quantity) return;
	
		$ID_Pharmacy =wp_get_current_user()->ID;
		$variable=false;
		$listProducts = $this->dbControllerPublic->getAllProductsPromoted($ID_Pharmacy);
		if ($variation_id==0) {
			$id_product_added=$product_id;
			$variable=false;
		}else {
			$id_product_added=$variation_id;
			$variable=true;
		}
		$cart=WC()->cart;
		$productSKU = WC()->cart->get_cart_item( $cart_item_key )['data']->get_sku();

		if ($listProducts!=null  && sizeof($listProducts)>0) {
			foreach ($listProducts as $key => $item) {
				if ( $productSKU == $item->sku ) {
					if(intval($item->number)<=intval($quantity)){
						if (intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCT || intval($item->type_promotion)==PRODUCTPROMOTIONPRODUCTALL ) {
							$productSKUToBeAdded=$item->sku_gift;
							$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
							$parent_id = wp_get_post_parent_id( $variationToBeAdded );
							if ($parent_id > 0) {
								$variable=true;
							}else {
								$variable=false;
							}
							if($variable){
								
								$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
								if (intval($quantityAdded)==0) {
									WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
									$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
								}else {
									foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
										if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
											$giftItemKey=$key;
											$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
											$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										} 
									}
								}
							}else {
								$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
								$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
								if ($quantityAdded==0) {
									WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
									$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
								}else {
									foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
										if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
											$giftItemKey=$key;
											$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
											$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,PRODUCTAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										} 
									}
								}
							}
							return;
						}else {

							$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',PRODUCTAPPLIED,$quantity,$item->promotion,$quantity);
							return;
						}
					}
				}
			}
		}

		$listProductsByCategories = $this->dbControllerPublic->getAllCategoriesPromoted($ID_Pharmacy);

		if ($listProductsByCategories!=null && sizeof($listProductsByCategories)>0) {
			foreach ($listProductsByCategories as $key => $item) {
				if ($productSKU==$item->sku) {
					if(intval($item->number)<=intval($quantity)){
						if (intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCT || intval($item->type_promotion)==CATEGORYPROMOTIONPRODUCTALL ) {
							$productSKUToBeAdded=$item->sku_gift;
							$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
							$parent_id = wp_get_post_parent_id( $variationToBeAdded );
							if ($parent_id > 0) {
								$variable=true;
							}else {
								$variable=false;
							}
							if($variable){
								$variationToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
								$parent_id = wp_get_post_parent_id( $variationToBeAdded );
								$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($variationToBeAdded,$id_product_added);
								if(intval($quantityAdded)==0){
									WC()->cart->add_to_cart( $parent_id,intval($quantity/$item->number)*$item->promotion,$variationToBeAdded );
									$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
								}else {
									foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
										if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
											$giftItemKey=$key;
											$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
											$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$variationToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										} 
									}
								}	
							}else {
								$postToBeAdded=$this->dbControllerPublic->getPostIDBySKU($productSKUToBeAdded);
								$quantityAdded=$this->dbControllerPublic->getCountQuantityGift($postToBeAdded,$id_product_added);
								if(intval($quantityAdded)==0){
									WC()->cart->add_to_cart( $postToBeAdded,intval($quantity/$item->number)*$item->promotion );
									$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
								}else {
									foreach (WC()->cart->get_cart() as $key => $itemMaybeGift) {
										if (intval($itemMaybeGift['data']->get_sku())==intval($item->sku_gift)) {
											$giftItemKey=$key;
											$cart->set_quantity( $giftItemKey,intval($quantityAdded)+ intval($quantity/intval($item->number))*intval($item->promotion), true );
											$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,$postToBeAdded,CATEGORYAPPLIED,intval($quantity/$item->number)*$item->promotion,$item->promotion,$quantity);
										} 
									}
								}	
							}
							return;
						}else {
							$this->dbControllerPublic->addPromotionApplied($ID_Pharmacy,$item->type_promotion,$item->row_ID,$id_product_added,'0000',CATEGORYAPPLIED,$quantity,$item->promotion,$quantity);
							return;
						}
					}
				}
			}
		}	
	}


    /**
	 * Apply the check sum after editing the cart.
	 *
	 * @since    1.0.0
	 */
	
	public function apply_fees($cart_object){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;
		//if ( ! wc_current_user_has_role( 'customer' ) ) return;
		$ID_Pharmacy =wp_get_current_user()->ID;

		if ( !$cart_object->is_empty() ):
		$total=$cart_object->cart_contents_total;

		$sumPromotion = $this->dbControllerPublic->getLastCheckSum($ID_Pharmacy);
		if ($sumPromotion!=null) {
			if ($sumPromotion->sum <= $total) {
				$total=$total*$sumPromotion->percentage/100;
				$cart_object->add_fee( __('Rabat ','custom-promotion'), -$total, false);
			}
		}
    	endif;
	}

	/**
	 * Check cart contents.
	 *
	 * @since    1.0.0
	 */
	
	public function promotion_text_cart_item_name($item_name, $cart_item, $cart_item_key ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		//if ( ! wc_current_user_has_role( 'customer' ) ) return;

		$ID_Pharmacy =wp_get_current_user()->ID;
		$listAppliedPromotions = $this->dbControllerPublic->getListPromotionsApplied($ID_Pharmacy);
		$productSKU = WC()->cart->get_cart_item( $cart_item_key )['data']->get_sku();

		if(intval(WC()->cart->cart_contents[$cart_item_key ]['variation_id'])==0){
			
			$id_product_=intval(WC()->cart->cart_contents[$cart_item_key ]['product_id']);
		}else {

			$id_product_=intval(WC()->cart->cart_contents[$cart_item_key ]['variation_id']);
		}
		
		$id_product_added =  $id_product_;

		if ($listAppliedPromotions!=null  && sizeof($listAppliedPromotions)>0) {
			foreach ($listAppliedPromotions as $key => $item) {
				if ($id_product_added==intval($item->ID_product)) {
					
					if ($item->type_promotion==CATEGORYPROMOTIONPRODUCT || $item->type_promotion==CATEGORYPROMOTIONPRODUCTALL || $item->type_promotion==PRODUCTPROMOTIONPRODUCT || $item->type_promotion==PRODUCTPROMOTIONPRODUCTALL) {
						
					}else {
						$str=$item_name;
						unset($item_name);
						$item_name='SKU: '.$productSKU.'<br>'.$str.'<br>'.sprintf(__(' Otrzymujesz rabat w wysokości %s%s ','custom-promotion'), 
						$item->promotion,'%'
						
						);
					}
					
				}
			}
		}

		return $item_name;
	}

	/**
	 * Check cart contents.
	 *
	 * @since    1.0.0
	 */
	
	public function promotion_text_cart_item_quantity( $quantity, $cart_item_key ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		//if ( ! wc_current_user_has_role( 'customer' ) ) return;

		$productSKU = WC()->cart->get_cart_item( $cart_item_key )['data']->get_sku();
		if(in_array($productSKU,$this->dbControllerPublic->getGifts())){
			$quantity = sprintf( '%2$s <input type="hidden" name="cart[%1$s][qty]" value="%2$s" />', $cart_item_key, WC()->cart->get_cart_item( $cart_item_key )['quantity'] );
		}

		return $quantity;
	}

	/**
	 * Remove item link.
	 *
	 * @since    1.0.0
	 */
	
	public function customized_cart_item_remove_link( $button_link, $cart_item_key ){
		//SET HERE your specific products IDs
		$targeted_products_ids = array( 25, 22 );
	
		// Get the current cart item
		$productSKU = WC()->cart->get_cart_item( $cart_item_key )['data']->get_sku();
		if(in_array($productSKU,$this->dbControllerPublic->getGifts())){
			$button_link = '';
		}
	
		return $button_link;
	}

	/**
	 * Exclude gift products from search.
	 *
	 * @since    1.0.0
	 */
	
	public function exclude_product_category( $query ) {

		if ($query->is_search()) {
			$tax_query = (array) $query->get('tax_query');
	
			$tax_query[] = array(
				   'taxonomy' => 'product_cat',
				   'field' => 'slug',
				   'terms' => array('giftproduct'),
				   'operator' => 'NOT IN'
			);        
			
			$query->set('tax_query', $tax_query);        
	
		}
		return $query;
	}

	/**
	 * Generates xml file
	 *
	 * @since    1.0.0
	 */
	
	public function generate_xml_file(  $order_get_id ){
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
		//if ( ! wc_current_user_has_role( 'customer' ) ) return;

		$order=wc_get_order($order_get_id);
		$user_id=$order->get_user_id();
		$user_meta=get_user_meta($user_id);
		$listAppliedPromotions = $this->dbControllerPublic->getListPromotionsApplied($user_id);
		$position_index=0;
		$extraProducts=false;
		$itemsToRegister=null;

		if ($listAppliedPromotions!=null && sizeof($listAppliedPromotions)>0) {
			foreach ($listAppliedPromotions as $key => $itemPromoted) {
					if ($itemPromoted->type_promotion==CATEGORYPROMOTIONPRODUCT || $itemPromoted->type_promotion==CATEGORYPROMOTIONPRODUCTALL || $itemPromoted->type_promotion==PRODUCTPROMOTIONPRODUCT || $itemPromoted->type_promotion==PRODUCTPROMOTIONPRODUCTALL) {
						$extraProducts=true;
						break;
					}
			}
		}

		$cartObject=WC()->cart->cart_contents;
		foreach ($cartObject as $key=> $item) {

			$position_index++;
			$promoted=false;
			$discount = 0;
			$product_sku=$item['data']->get_sku();
			$price=$item['data']->get_price();

			$product_variation_id = $item['variation_id'];

			if ($product_variation_id==0) {
				$productConcerned=$item['product_id'];
			} else {
				$productConcerned=$item['variation_id'];
			}
			$regularPrice=wc_get_product($productConcerned)->get_regular_price();
			
			if ($listAppliedPromotions!=null && sizeof($listAppliedPromotions)>0) {
				foreach ($listAppliedPromotions as $key => $itemPromoted) {
					if ($itemPromoted->ID_product==$productConcerned) {
						$promoted=true;
						$promotion=new \stdClass; 
						$promotion->type_promotion=intval($itemPromoted->type_promotion);
						$promotion->intentioned=intval($itemPromoted->intentioned);
						$promotion->quantity=intval($itemPromoted->quantity);
						$promotion->promotion=intval($itemPromoted->promotion);
					}
				}
			}
			
			if (!$promoted) {
				
				$order_position = $position_index;
				if (is_numeric($regularPrice) && $regularPrice!=0) {
					if ($price<$regularPrice) {
						$discount = 100 - round(($price*100)/$regularPrice,2);
						$discount_test= 100 - round(($price*100)/$regularPrice);
						if ($discount_test==100) {
							$discount = 0;
						}
					}
				}
				$itemToRegister=new \stdClass; 
				$itemToRegister->ID_POZYCJI_ZAMOWIENIA=$order_position;
				$itemToRegister->ID_PRODUKTU=$product_sku;
				$itemToRegister->LICZBA=$item['quantity'];
				$itemToRegister->RABAT=$discount;
				$itemsToRegister[]=$itemToRegister;
			}else {
				
				if ($promotion->type_promotion==CATEGORYPROMOTIONPRODUCT || $promotion->type_promotion==CATEGORYPROMOTIONPRODUCTALL || $promotion->type_promotion==PRODUCTPROMOTIONPRODUCT || $promotion->type_promotion==PRODUCTPROMOTIONPRODUCTALL) {

					$order_position = $position_index;
					if (is_numeric($regularPrice) && $regularPrice!=0) {
						if ($price<$regularPrice) {
							$discount = 100 - round(($price*100)/$regularPrice,2);
							$discount_test= 100 - round(($price*100)/$regularPrice);
							if ($discount_test==100) {
								$discount = 0;
							}
						}
					}
					$itemToRegister=new \stdClass; 
					$itemToRegister->ID_POZYCJI_ZAMOWIENIA=$order_position;
					$itemToRegister->ID_PRODUKTU=$product_sku;
					$itemToRegister->LICZBA=$item['quantity'];
					$itemToRegister->RABAT=$discount;
					$itemsToRegister[]=$itemToRegister;
				}else {

					$order_position = $position_index;
					if (is_numeric($regularPrice) && $regularPrice!=0) {
						if ($price<$regularPrice) {
							$discount = 100 - round(($price*100)/$regularPrice,2);
							$discount_test= 100 - round(($price*100)/$regularPrice);
							if ($discount_test==100) {
								$discount = 0;
							}
						}
					}
					
					$itemToRegister=new \stdClass; 
					$itemToRegister->ID_POZYCJI_ZAMOWIENIA=$order_position;
					$itemToRegister->ID_PRODUKTU=$product_sku;
					$itemToRegister->LICZBA=$promotion->quantity;
					$itemToRegister->RABAT=$discount;
					$itemsToRegister[]=$itemToRegister;
				}
			}
		}


		
		$date= current_time('mysql');
		$date= new DateTime($date);
		$order_date= date_format($date, "YmdHis");

		$file_name='zamowienie_'.$order_get_id.'_'.$order_date.'.xml';
		$file_path= ABSPATH.'wymiana-danych/zamowienia/';
		$file_path_2= ABSPATH.'wymiana-danych/zamowienia_kopia/';

		$xw = xmlwriter_open_uri ($file_path.$file_name);
		xmlwriter_set_indent($xw, 1);
		$res = xmlwriter_set_indent_string($xw, ' ');
		xmlwriter_start_document($xw, '1.0', 'UTF-8');
		// A first element
		xmlwriter_start_element($xw, 'ZAMOWIENIA');
			// Attribute 'att1' for element 'tag1'
			xmlwriter_start_attribute($xw, 'xmlns:xsi');
			xmlwriter_text($xw, 'http://www.w3.org/2001/XMLSchema-instance');
			xmlwriter_end_attribute($xw);
			xmlwriter_start_attribute($xw, 'xsi:noNamespaceSchemaLocation');
			xmlwriter_text($xw, 'farmaprom_firma_zamowienie.xsd');
			xmlwriter_end_attribute($xw);
			// Start a child element
			xmlwriter_start_element($xw, 'ZAMOWIENIE');
				xmlwriter_start_element($xw, 'NAGLOWEK');

					xmlwriter_start_element($xw, 'NUMER_ZAMOWIENIA');
					xmlwriter_text($xw, ''.$order_get_id);
					xmlwriter_end_element($xw); // NUMER_ZAMOWIENIA

					xmlwriter_start_element($xw, 'ID_ODDZIALU_HURTOWNI');
					xmlwriter_text($xw, '312');
					xmlwriter_end_element($xw); // ID_ODDZIALU_HURTOWNI

					xmlwriter_start_element($xw, 'DATA_ZAMOWIENIA');
					xmlwriter_text($xw, ''.$order->get_date_created());
					xmlwriter_end_element($xw); // DATA_ZAMOWIENIA

					xmlwriter_start_element($xw, 'ID_APTEKI');
					xmlwriter_text($xw, ''.$user_meta['pharmaprom_id'][0]);
					xmlwriter_end_element($xw); // ID_APTEKI

					xmlwriter_start_element($xw, 'ID_REP');
					xmlwriter_text($xw, '149244');
					xmlwriter_end_element($xw); // ID_REP

					xmlwriter_start_element($xw, 'REP_EMAIL');
					xmlwriter_text($xw, 'online@amara.pl');
					xmlwriter_end_element($xw); // REP_EMAIL

					xmlwriter_start_element($xw, 'LICZBA_POZYCJI');
					xmlwriter_text($xw, ''.$position_index);
					xmlwriter_end_element($xw); // LICZBA_POZYCJI

					
					xmlwriter_start_element($xw, 'RABAT_KWOTOWY');
					xmlwriter_text($xw, '0');
					xmlwriter_end_element($xw); // RABAT_KWOTOWY
					
					
				xmlwriter_end_element($xw); // NAGLOWEK
				xmlwriter_start_element($xw, 'POZYCJE');
				
				foreach ($itemsToRegister as $key => $item) {
					if ($item!=null) {
						xmlwriter_start_element($xw, 'POZYCJA');

							xmlwriter_start_element($xw, 'ID_POZYCJI_ZAMOWIENIA');
							xmlwriter_text($xw, ''.$item->ID_POZYCJI_ZAMOWIENIA);
							xmlwriter_end_element($xw); // ID_POZYCJI_ZAMOWIENIA

							xmlwriter_start_element($xw, 'ID_PRODUKTU');
							xmlwriter_text($xw, ''.$item->ID_PRODUKTU);
							xmlwriter_end_element($xw); // ID_PRODUKTU

							xmlwriter_start_element($xw, 'LICZBA');
							xmlwriter_text($xw, ''.$item->LICZBA);
							xmlwriter_end_element($xw); // LICZBA

							xmlwriter_start_element($xw, 'RABAT');
							xmlwriter_text($xw,str_replace(',','.',''.$item->RABAT) );
							xmlwriter_end_element($xw); // RABAT
						
						xmlwriter_end_element($xw); // POZYCJA
					}
				}

				xmlwriter_end_element($xw); // POZYCJE
			xmlwriter_end_element($xw); // ZAMOWIENIE
		xmlwriter_end_element($xw); // ZAMOWIENIA	

		xmlwriter_end_document($xw);
		copy ( $file_path.$file_name, $file_path_2.$file_name );
		
        $order->update_meta_data('created_xml_file', 'yes');
        $order->save();
		$this->dbControllerPublic->deleteTemporaryCart($user_id);
	}
	
}