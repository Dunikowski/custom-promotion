<?php

/**
 * This class is responsable for validating data given in the plugin forms.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 */

class Data_Validator {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The database controller for admin of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbControllerAdmin    The database controller for admin of this plugin.
	 */
	private $dbControllerAdmin;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $dbControllerAdmin ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->dbControllerAdmin = $dbControllerAdmin;

	} 
	
    /**
	 * Checks if the number is positive and valid
	 *
	 * @since    1.0.0
	 * @param    string    $data    the input that should be validated.
	 * @return   boolean   $isValid    The status of the input if valid or not
	 * 
	 */

	public function checkPositiveNumber( $data ) {
		$isValid=false;
		if (is_numeric($data)) {
			if ( $data >= 0 ) {
				$isValid=true;
			}
		}
		return $isValid;
	}

	/**
	 * Checks if the promotion of percentage input is valid
	 *
	 * @since    1.0.0
	 * @param    string    $data       The percentage input that should be validated.
	 * @return   boolean   $isValid    The status of the input if valid or not
	 * 
	 */

	public function checkPromotionPercentage( $data ) {
		$isValid=false;
		if (is_numeric($data)) {
			if ( $data >= 0 ) {
				if ($data<=100) {
					$isValid=true;
				}
			}
		}
		return $isValid;
	}

	/**
	 * Checks if the pharmacy is approved
	 *
	 * @since    1.0.0
	 * @param    string   $data    The percentage input that should be validated.
	 * @return   object   $pharmacies    Return the list of the pharmacies approved and the list of pharmacies not approved or null.
	 * 
	 */

	public function checkIfPharmacyApproved( $data ) {
		$pharmacies = new \stdClass;
		$pharmacies->pharmaciesApproved[]=null;
		$pharmacies->pharmaciesNotApproved=null;
		if ( $data != null ) {
			if (sizeof($data)>0) {
				foreach ($data as $key => $item) {
					if ($this->checkPositiveNumber( $item->id )) {
						if ($this->dbControllerAdmin->isPharmacyApproved($item->id)) {
							$pharmacies->pharmaciesApproved[]=$item;
						}else {
							$pharmacies->pharmaciesNotApproved=$item;
						}
					}else {
						$pharmacies->pharmaciesNotApproved=$item;
					}
				}
				return $pharmacies;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	/**
	 * Checks if the CSV file is valid and respecting the formal structure
	 *
	 * @since    1.0.0
	 * @param    string    $data       The file inserted that should be validated.
	 * @return   boolean   $isValid    The status of the file if valid or not.
	 * 
	 */
	public function checkValidityOfCsvFile( $fileCsv ) {
		$isValid=false;
		$insideError=false;
		$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
		if(in_array($fileCsv['type'],$mimes)){
			if (($handle = fopen($fileCsv["tmp_name"], "r")) !== FALSE) {
				fgetcsv($handle, 1000, ";");
				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
					
					if (!$this->checkPositiveNumber( $data[0] ) || !$this->checkPositiveNumber( $data[1] )) {
						$insideError=true;
					}
				}
				fclose($handle);    
				if (!$insideError) {
					$isValid=true;
				}
			}
		}
		return $isValid;
	}
    
	/**
	 * Checks if the group name is a valid text and doesn't exist already
	 *
	 * @since    1.0.0
	 * @param    string    $data       The percentage input that should be validated.
	 * @return   boolean   $isValid    The status of the input if valid or not.
	 * 
	 */
	public function checkValidityGroupName( $data,$isEdited=true ) {
		$isValid=false;
		$teststr=$data.' ';
		$teststr=str_replace(' ', '', $teststr);
		if (ctype_alnum($teststr)) {
			if ($isEdited) {
				if (!$this->dbControllerAdmin->existGroupName($data)) {
					$isValid=true;
				}else
					$isValid=true;
			}
			
		}
		return $isValid;
	}

	/**
	 * Checks if the Promotion ID exists.
	 *
	 * @since    1.0.0
	 * @param    string    $ID       The ID of the promotion.
	 * @return   boolean   $isValid  The status of the input if valid or not.
	 * 
	 */
	public function checkValidityPromotionID( $ID ) {
		$isValid=false;
		if ($this->checkPositiveNumber($ID)) {
			if ($this->dbControllerAdmin->existPromotion($ID)) {
				$isValid=true;
			}
		}
		return $isValid;
	}

	/**
	 * Checks if the group ID exists.
	 *
	 * @since    1.0.0
	 * @param    string    $ID       The ID of the group.
	 * @return   boolean   $isValid  The status of the input if valid or not.
	 * 
	 */
	public function checkValidityGroupID( $ID ) {
		$isValid=false;
		if ($this->checkPositiveNumber($ID)) {
			if ($this->dbControllerAdmin->existGroup($ID)) {
				$isValid=true;
			}
		}
		return $isValid;
	}
}