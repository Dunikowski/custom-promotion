<?php

/**
 * the display controller of the plugin.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/admin
 */

/**
 * class custom promotion display controller .
 *
 * Defines the plugin name, version, control the output to the admin display by using the class diplay and db-controller
 * by passing data into the html parts.
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 * @author     Cumulus Team <mustapha@agencjacumulus.pl>
 */
class Custom_Promotion_Display_Controller {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
    private $version;
    
    /**
	 * The admin display of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $adminDisplay    The admin display of this plugin.
	 */
    private $adminDisplay;
    
    /**
	 * The database controller of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbControlleAdmin    The database controller for admin of this plugin.
	 */
	private $dbControlleAdmin;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $dbControlleAdmin ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->dbControlleAdmin = $dbControlleAdmin;

		$this->load_dependencies();
		$this->adminDisplay = new Admin_Display( $this->plugin_name, $this->version );

	}
	
	private function load_dependencies() {

		/**
		 * The class responsible for display.
		 */

        require_once plugin_dir_path( dirname( __FILE__ ) ) .  'admin/partials/class-custom-promotion-admin-display.php';

	}


	/**
	 * This function display the main page of the plugin back en dashboard.
	 * @since    1.0.0
	 * @param      string    $tab       The activated tab in the main page.
	 */
	
    
    public function display_plugin( $tab = 'addNew' , $errors='', $success=''){
		if ($tab == 'addNew') {
			$data = new \stdClass;
			$data->pharmacies=$this->dbControlleAdmin->getAllPharmaciesWithGroupNames();
			$data->products=$this->dbControlleAdmin->getAllProducts();
			$data->categories=$this->dbControlleAdmin->getAllcategories();
			$data->groups=$this->dbControlleAdmin->getAllGroupNames();
			$data->giftProducts=$this->dbControlleAdmin->getproductsGifts();
		}elseif ($tab == 'listPromotion') {
			$data = new \stdClass;
			$data->pharmacies=$this->dbControlleAdmin->getAllPharmaciesWithGroupNames();
			$data->groups=$this->dbControlleAdmin->getAllGroupNames();
			$data->products=$this->dbControlleAdmin->getAllProducts();
			$data->categories=$this->dbControlleAdmin->getAllcategories();
			$data->checkSum=$this->dbControlleAdmin->getAllCheckSum();
			$data->productsPromotion=$this->dbControlleAdmin->getAllProductPurchased();
			$data->categoriesPromotion=$this->dbControlleAdmin->getAllCategoryPurchased();
			$data->listPrices=$this->dbControlleAdmin->getlistPrices();
			$data->giftProducts=$this->dbControlleAdmin->getproductsGifts();
		}elseif ($tab == 'settings') {
			$data = new \stdClass;
			$data->pharmaciesWithGroupName=$this->dbControlleAdmin->getAllPharmaciesWithGroupNames();
			$data->groups=$this->dbControlleAdmin->getAllGroupNames();
			$data->pharmacies=$this->dbControlleAdmin->getAllPharmacies();
			$data->groupSubmitted=$this->dbControlleAdmin->getAllGroups();
			$data->minimumSum=$this->dbControlleAdmin->getAllMinimumSum();
			$data->products=$this->dbControlleAdmin->getAllProducts();
			$data->exclusive=$this->dbControlleAdmin->getAllProductExcluded();
		}
	
        $this->adminDisplay->displayWrapper($tab,$data,$errors,$success);
    }
	

}