<?php

/**
 * Fired during plugin activation
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 * @author     Cumulus Team <mustapha@agencjacumulus.pl>
 */
class Custom_Promotion_Activator {

	/**
	 * Create necessary database tables .
	 *
	 * Creates table promotion that contains the diffrent types of promotions and the indexes 
	 * that optimizes the search and update queries and create the group names that classify 
	 * the farmacies per groups.
	 *
	 * @since    1.0.0
	 */
	public static function activate($wpdb) {
		$table_group_name='CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'cp_groupnames (
			`ID` bigint(10) NOT NULL PRIMARY KEY,
			`name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;';
		$indexes_group_name='CREATE INDEX   ix_group_name ON '.$wpdb->prefix.'cp_groupnames(name);';
		$table_group_user='CREATE TABLE IF NOT EXISTS  '.$wpdb->prefix.'cp_grouplist (
			`ID` bigint(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`ID_group` bigint(10) NOT NULL ,
			`ID_user` bigint(20) unsigned NOT NULL ,
			FOREIGN KEY (ID_group) REFERENCES '.$wpdb->prefix.'cp_groupnames(ID) ON DELETE CASCADE,
			FOREIGN KEY (ID_user) REFERENCES '.$wpdb->prefix.'users(ID) ON DELETE CASCADE
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;';
		$table_promotion='CREATE TABLE IF NOT EXISTS  '.$wpdb->prefix.'cp_promotions (
		`row_ID` bigint(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
		`ID` bigint(10) NOT NULL,
		`ID_user` bigint(20) unsigned NOT NULL,
		`type_promotion` bigint(10) NOT NULL,
		`number` bigint(10),
		`sku` varchar(20),
		`sku_gift` varchar(20),
		`promotion` bigint(10) NOT NULL, 
		`date_promotion` datetime,
		`dateStart` date,
		`dateEnd` date,
		FOREIGN KEY (ID_user) REFERENCES '.$wpdb->prefix.'users(ID) ON DELETE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;';
		$table_temporary_cart='CREATE TABLE IF NOT EXISTS  '.$wpdb->prefix.'cp_tmp_cart_prom (
			`ID` bigint(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			`ID_user` bigint(20) unsigned NOT NULL ,
			`ID_product` bigint(20) unsigned NOT NULL ,
			`ID_product_gift` bigint(20) unsigned NOT NULL ,
			`ID_promotion` bigint(10)  ,
			`product_category` tinyint(4) unsigned NOT NULL ,
			`quantity` int(10) unsigned NOT NULL , 
			`intentioned` int(10) unsigned NOT NULL ,
			`type_promotion` int(10) unsigned NOT NULL ,
			`promotion` bigint(10) , 
			FOREIGN KEY (ID_product) REFERENCES '.$wpdb->prefix.'posts(ID) ON DELETE CASCADE,
			FOREIGN KEY (ID_user) REFERENCES '.$wpdb->prefix.'users(ID) ON DELETE CASCADE
		  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;';
		$results=$wpdb->query($table_group_name);
		$results=$wpdb->query($table_group_user);
		$results=$wpdb->query($table_promotion);
		$results=$wpdb->query($table_temporary_cart);
	}
}