<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/includes
 * @author     Cumulus Team <mustapha@agencjacumulus.pl>
 */
class Custom_Promotion_Deactivator {

	/**
	 * Drop the tables and indexes related to the plugin.
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;
		$table_group='DROP TABLE IF EXISTS '.$wpdb->prefix.'cp_groupnames;';
		$table_group_user='DROP TABLE IF EXISTS '.$wpdb->prefix.'cp_grouplist;';
		$table_temporary_cart='DROP TABLE IF EXISTS  '.$wpdb->prefix.'cp_tmp_cart_prom';
		$table_promotion='DROP TABLE IF EXISTS '.$wpdb->prefix.'cp_promotions;';
		$wpdb->get_results($table_group_user);
		$wpdb->get_results($table_temporary_cart);
		$wpdb->get_results($table_group);
		$wpdb->get_results($table_promotion);
	}
}