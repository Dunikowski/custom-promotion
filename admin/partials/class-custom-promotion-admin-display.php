<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/admin/partials
 */

class Admin_Display {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Display the main page of the plugin
	 *
	 * @since    1.0.0
	 * @param      string    $tab       The tab activated at the moment.
	 * @param      array    $data    the data that should be displayed on the page.
	 */


	public function displayWrapper($tab,$data=null,$errors='',$success=''){
		# Testing which tab is activated

		echo'<div class="wrap">';

		echo'<div class="d-none js-modal-for-removing" >
		<div class="modal-dialog" role="document">
		  <div class="modal-content">
			<div class="modal-header">
			  <strong class="modal-title" id="exampleModalLabel">'.__('Potwierdź polecenie usunięcia','custom-promotion').'</strong>
			  <button type="button" class="close" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-danger btn-to-remove">'.__('Usuń','custom-promotion').'</button>
			</div>
		  </div>
		</div>
	  </div>';
		if ($errors!='') {
			# code...
			echo'<div class="alert alert-warning">
					<strong>'.__('Warning','custom-promotion').'!</strong> '.$errors.'
	  			 </div>';
		}
		if ($success!='') {
			# code...
			echo'<div class="alert alert-success">
					<strong>'.__('Success','custom-promotion').'!</strong> '.$success.'
	  			 </div>';
		}
		echo'	<h2>'.__('Promocje','custom-promotion').'</h2>';
		echo'		<h2 class="nav-tab-wrapper">';
		$tabs = '';

		$tabs .='<a href="?page=custom_promotion&tab=addNew" class="nav-tab ';

		if($tab == "addNew" ) {
			$tabs .= 'active-item';
		}

		$tabs .='">'.__('Dodaj nową','custom-promotion').'</a>';
		$tabs .='<a href="?page=custom_promotion&tab=listPromotion" class="nav-tab ';

		if($tab == "listPromotion" ) {
			$tabs .= 'active-item';
		}

		$tabs .='">'.__('Lista promocji','custom-promotion').'</a>';
		$tabs .='<a href="?page=custom_promotion&tab=settings" class="nav-tab ';

		if($tab == "settings" ) {
			$tabs .= 'active-item';
		}

		$tabs .='">'.__('Ustawienia','custom-promotion').'</a>';
		echo $tabs;

		echo'		</h2>';
		echo'	<form enctype="multipart/form-data" method="post">';
		echo' 		<div class="container clear-margin-left">';
		echo' 			<div class="row ml-0">';
			//Instructions for the adequat page to be displayed
			if ($tab=='addNew') {
				$this->displayAddNewForm($data);
				echo'	<div class="promotion-admin-inputs__item--button row ml-3 mt-2 ">';
				echo'		<input class="btn btn-success" type="submit" name="validateAllPromotion" value="'.__('Zatwierdź wszystkie promocje jednocześnie','custom-promotion').'" >';
				echo'	</div>';
			}elseif ($tab=='listPromotion') {
				# code...
				$this->displayListPromotion($data);
			}elseif ($tab=='settings') {
				# code...
				$this->displaySettings($data);
			}

		echo'			</div>';
		echo'	</form>';
		echo'</div>';
	}


	/**
	* Display the add new page
	*
	* @since   1.0.0
	* @param   array    $data    the data that should be displayed on the page.
	*/

	private function displayAddNewForm($data){

		if ($data!=null && sizeof(get_object_vars($data))>0) {
			echo'<h3 class="mt-3 ml-3">'.__('Wybierz aptekę','custom-promotion').'</h3>';
			echo' <button type="button" class="btn btn-xs btn-info ml-2 mt-2 h-25" data-toggle="popover"
					title="Wybór apteki"
					data-placement="bottom"
					data-content="Możliwe jest wybranie dowolnych aptek klikając na pozycje w liście lub zaznaczenie przycisku <i>Wszystkie</i> aby wybrać wszystkie. <br><strong>Uwaga</strong> <br>System zapamiętuje daty utworzenia promocji i nowych użytkowników domyślnie nie dodaje do listy wcześniej utworzonych promocji. Można to zmienić w ustawieniach na Liście promocji dla utworzonej już promocji."
					data-html="true"><i>i</i></button>';
			$this->displayInputSelectPharmacies($data->pharmacies,$data->groups);
			echo'<h3 class="mt-4 ml-3">'.__('Promocja na wartość rachunku','custom-promotion').'</h3>';
			echo' <button type="button" class="btn btn-xs btn-info ml-2 mt-3 h-25" data-toggle="popover"
					title="Promocja na wartość rachunku"
					data-placement="bottom"
					data-content="W polu <i>Wpisz sumę</i> należy podać wartość koszyka a następnie w polu obok podać rabat procentowy."
					data-html="true"><i>i</i></button>';
			$this->displayInputPromotionPerCheckOutSum();
			echo'<h3 class="mt-4 ml-3">'.__('Promocja na produkt','custom-promotion').'</h3>';
			echo' <button type="button" class="btn btn-xs btn-info ml-2 mt-3 h-25" data-toggle="popover"
			title="Promocja na produkt"
			data-placement="bottom"
			data-content="Po wybraniu jednego lub kilku produtów z listy należy w polu <i>Podaj ilość produktów</i> podać ile produtków jest wymaganych w koszyku, aby aktywować promocję. Można w tym miejscu stworzyć promocję <i>kup 3 a otrzymasz 4-ty produkt gratis</i> lub po zakupie określonej ilości otrzymasz zniżkę procentową na te produkty. Ilość gratisowych produktów lub rabat procentowy należy określić w polu <i>Ilość/Wartość</i>."
			data-html="true"><i>i</i></button>';
			$this->displayInputPromotionByProductPurchased($data->products,$data->giftProducts);
			echo'<h3 class="mt-4 ml-3">'.__('Promocja na produkt z kategorii','custom-promotion').'</h3>';
			echo' <button type="button" class="btn btn-xs btn-info ml-2 mt-3 h-25" data-toggle="popover"
			title="Promocja na produkt z kategorii"
			data-placement="bottom"
			data-content="Po wybraniu jednego lub kilku produtów z listy należy w polu <i>Podaj ilość produktów</i> podać ile produtków jest wymaganych w koszyku, aby aktywować promocję. Ten rodzaj promocji działa tak samo jak promocja na produkt, ale będzie dotyczyć wybranych kategorii produktów."
			data-html="true"><i>i</i></button>';
			$this->displayInputPromotionByCategoryPurchased($data->categories,$data->giftProducts);
			echo'<h3 class="mt-4 ml-3">'.__('Indywidualny cennik dla apteki','custom-promotion').'</h3>';
			echo' <button type="button" class="btn btn-xs btn-info ml-2 mt-3 h-25" data-toggle="popover"
			title="Indywidualny cennik"
			data-placement="bottom"
			data-content="W tym miejscu możliwe jest wgranie indywidualnego cennika dla wybranych wyżej użytkowników. Plik jakiego można użyć to csv, gdzie w pierszej lini jest podany sku oraz cena (price), a w kolejnych wierszach odpowiednie wartości. <br><b> Przykładowa konstrukcja pliku:</b><br>sku;price;<br>
			106453;100;"
			data-html="true"><i>i</i></button>';
			$this->displayInputListOfPricesPerPharmacies();

		}
	}

	/**
	* Display the list promotion page
	*
	* @since   1.0.0
	* @param   array    $data    the data that should be displayed on the page.
	*/

	private function displayListPromotion($data){
		if ($data!=null && sizeof(get_object_vars($data))>0) {
			echo' <div class="container"> ';
			echo'<h3 class="mt-3">'.__('Promocje na wartość koszyka','custom-promotion').'</h3>';
			echo'<p>
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseList1" aria-expanded="false" aria-controls="collapseList1">
			'.__('Pokaż Promocje na wartość koszyka','custom-promotion').'
			</button>
			</p>
			<div class="collapse" id="collapseList1">
			<div class="card card-body w-100">';
				$this->displayPromotionPerCheckOutSum($data->checkSum,$data->pharmacies,$data->groups);
			echo'</div>';
			echo'</div>';
			echo'<h3>'.__('Promocje na produkt','custom-promotion').'</h3>';
			echo'<p>
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseList2" aria-expanded="false" aria-controls="collapseList2">
			'.__('Pokaż promocje na produkt','custom-promotion').'
			</button>
			</p>
			<div class="collapse" id="collapseList2">
			<div class="card card-body w-100">';
				$this->displayPromotionByProductPurchased($data->productsPromotion,$data->pharmacies,$data->products,$data->giftProducts,$data->groups);
			echo'</div>';
			echo'</div>';
			echo'<h3>'.__('Promocje na produkty z kategorii','custom-promotion').'</h3>';
			echo'<p>
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseList3" aria-expanded="false" aria-controls="collapseList3">
			'.__('Pokaż Promocje na produkty z kategorii','custom-promotion').'
			</button>
			</p>
			<div class="collapse" id="collapseList3">
			<div class="card card-body w-100">';
				$this->displayPromotionByCategoriePurchased($data->categoriesPromotion,$data->pharmacies,$data->categories,$data->giftProducts,$data->groups);
			echo'</div>';
			echo'</div>';
			echo'<h3>'.__('Indywidualne cenniki','custom-promotion').'</h3>';
			echo'<p>
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseList4" aria-expanded="false" aria-controls="collapseList4">
			'.__('Pokaż indywidalne cenniki','custom-promotion').'
			</button>
			</p>
			<div class="collapse" id="collapseList4">
			<div class="card card-body w-100">';
				$this->displayListOfPricesPerPharmacies($data->listPrices,$data->pharmacies,$data->groups);
			echo'</div>';
			echo'</div>';
			echo'</div>';
		}
	}

	/**
	* Display the plugin settings.
	*
	* @since   1.0.0
	* @param   object    $data         data needed.
	*
	*/

	private function displaySettings($data){
		echo' <div class="container"> ';
		echo'<h3 class="mt-3 ml-3">'.__('Wybierz aptekę','custom-promotion').'</h3>';
			$this->displayInputSelectPharmacies($data->pharmaciesWithGroupName,$data->groups);
			echo'<h3 class="mt-3">'.__('Grupy aptek','custom-promotion').'</h3>';
			echo'<p>
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseGroup1" aria-expanded="false" aria-controls="collapseGroup1">
			'.__('Pokaż grupy aptek','custom-promotion').'
			</button>
			</p>
			<div class="collapse" id="collapseGroup1">
			<div class="card card-body w-100">';
				$this->displayGroupPharmacies($data);
			echo'</div>';
			echo'</div>';

			echo'<h3 class="mt-3">'.__('Minimum logistyczne (minimalna wartość koszyka do złożenia zamówienia)','custom-promotion').'</h3>';
			echo'<p>
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseGroup2" aria-expanded="false" aria-controls="collapseGroup2">
			'.__('Pokaż minimum logistyczne','custom-promotion').'
			</button>
			</p>
			<div class="collapse" id="collapseGroup2">
			<div class="card card-body w-100">';
				$this->displayMinimumSum($data);
			echo'</div>';
			echo'</div>';

			echo'<h3 class="mt-3">'.__('Products exclution','custom-promotion').'</h3>';
			echo'<p>
			<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseGroup3" aria-expanded="false" aria-controls="collapseGroup3">
			'.__('Pokaż Products excluded','custom-promotion').'
			</button>
			</p>
			<div class="collapse" id="collapseGroup3">
			<div class="card card-body w-100">';
				$this->displayExclutions($data);
			echo'</div>';
			echo'</div>';
		echo'</div>';
	}

	/**
	* Display the group pharmacies page
	*
	* @since   1.0.0
	* @param   object    $data         data needed.
	*
	*/

	private function displayGroupPharmacies($data){
		echo' 	<div > ';
					echo'	<div class="ml-3 mt-2">';
					echo'		<input type="text" name="groupName" placeholder="'.__('Wpisz nazwę grupy','custom-promotion').'">';
					echo'		<input class="pharmacy-group__button-send" type="submit" name="validategroupPharm" value="'.__('Zatwierdź','custom-promotion').'">';
					echo'	</div>';
		echo'			<br>---------------------------------------------------------';
					echo'<div class="container">';
					echo'<div class="row ml-0 mt-2">';
					$this->displayListGroupPharmacies($data->groupSubmitted,$data->pharmacies);
					echo'</div>';
					echo'</div>';
		echo'	</div>';
	}

	/**
	* Display Minimum sum list per pharmacies
	*
	* @since   1.0.0
	* @param   object    $data         data needed.
	*
	*/

	private function displayMinimumSum($data){
		echo' 	<div > ';
					echo'<div class="col-md-auto">
						<div class="input-group">
							<input type="number" name="sumlimit" placeholder="'.__('Wprowadź minimalną wartość','custom-promotion').'">
							<div class="input-group-append">
								<span class="input-group-text">zł</span>
							</div>
						</div>
						<div class="col-md-6 p-3">
							<input class="input-pharmacies-check-sum__button-send" type="submit" name="validateminimumsum"
								value="'.__('Zatwierdź','custom-promotion').'">
						</div>
					</div>';
		echo'			<br>---------------------------------------------------------';
					echo'<div class="container">';
					echo'<div class="row ml-0 mt-2">';
					$this->displayListMinimumSum($data->minimumSum,$data->pharmacies);
					echo'	</div>';
					echo'	</div>';
		echo'	</div>';
	}

	/**
	* Display products exclution
	*
	* @since   1.0.0
	* @param   string    $nameInput         name Of the input.
	* @param   string    $valueStart        value start Of the input.
	* @param   string    $valueEnd        value end Of the input.
	*
	*/

	private function displayDateTimePicker($nameInput,$valueStart='',$valueEnd=''){
		return'
			<div class="row ml-1 mt-2">
				<div class="col-md-auto pl-0" >
				<span> Od: </span>
				<input type="date" name="start'.$nameInput.'" value="'.$valueStart.'"  />
				</div>
				<div class="col-md-auto pl-0" >
				<span> Do: </span>
					<input type="date" name="end'.$nameInput.'" value="'.$valueEnd.'" />
				</div>
			</div>
					';
	}

	/**
	* Display products exclution
	*
	* @since   1.0.0
	* @param   object    $data         data needed.
	*
	*/

	private function displayExclutions($data){
		echo' 	<div > ';
					$this->displayInputProductExclution($data);
		echo'			<br>---------------------------------------------------------';
					echo'<div class="container">';
					echo'<div class="row ml-0 mt-2">';
					$this->displayListExclutions($data,$data->pharmacies);
					echo'	</div>';
					echo'	</div>';
		echo'	</div>';
	}

	/**
	 * Display the promotion of checkout sum exceeded.
	 *
	 * @since    1.0.0
	 * @param    object   $dataMinimumSum     the data of a certain promotion.
	 * @param    array    $pharmacies    the list of all pharmacies.
	 */

	private function displayListMinimumSum( $dataMinimumSum=null, $pharmacies=null ){
		# code...
		if ($dataMinimumSum!=null && sizeof($dataMinimumSum)>0) {
			# code...
			foreach ($dataMinimumSum as $key => $promotionItem) {
				# code...
				echo'<div class="container promotionperchecksum">';
				echo'<div class="row ">';
				if ($promotionItem->typePromotion==CHECKSUMPROMOTIONALL) {
					$this->displayEditSelectPharmacies($pharmacies->listPharmacies,$promotionItem->pharmacies,true,$promotionItem->ID);
				}else {
					$this->displayEditSelectPharmacies($pharmacies->listPharmacies,$promotionItem->pharmacies,false,$promotionItem->ID);
				}
				echo'
				</div>
				<div class="row">
				<div class="col-md-auto pl-0">
						<div class="input-group mt-2">
							<input type="number" name="sumlimit'.$promotionItem->ID.'" value="'.$promotionItem->sum.'">
							<div class="input-group-append">
								<span class="input-group-text">zł</span>
							</div>
						</div>
						</div>
					</div>
					<div class="row mt-2">
						<button class="btn btn-success mr-2" name="editminimumsum" type="submit" value="'.$promotionItem->ID.'" >'.__('Zaktualizuj','custom-promotion').'</button>
						<button class="btn btn-danger mr-2" name="deleteminimumsum" type="submit" value="'.$promotionItem->ID.'" >'.__('Usuń','custom-promotion').'</button>
					</div>
					</div>
					<br>---------------------------------------------------------';
			}

		}
	}

	/**
	 * Display the list of the products excluded from the user.
	 *
	 * @since    1.0.0
	 * @param    object   $data     the data of limited products per user.
	 * @param    array    $pharmacies    the list of all pharmacies.
	 */

	private function displayListExclutions( $data=null, $pharmacies=null ){
		# code...
		if ($data->exclusive!=null && sizeof($data->exclusive)>0) {
			# code...
			foreach ($data->exclusive as $key => $promotionItem) {
				# code...
				echo'<div class="container ">';
				echo'<div class="row ">';
				if ($promotionItem->typePromotion==PRODUCTLIMITATIONALL) {
					$this->displayEditSelectPharmacies($pharmacies->listPharmacies,$promotionItem->pharmacies,true,$promotionItem->ID);
				}else {
					$this->displayEditSelectPharmacies($pharmacies->listPharmacies,$promotionItem->pharmacies,false,$promotionItem->ID);
				}
				echo'
				</div>
				<div class="row">
					<select class="selectpicker mt-2 mr-2" name="productsexcluded'.$promotionItem->ID.'[]" multiple  data-live-search="true" title="'.__('Szukaj po nazwie','custom-promotion').'">';
							if ($data->products!=null && sizeof($data->products)>0) {
								//printing the existing pharmacies
								foreach ($data->products as $key => $item) {
									if (in_array(strval($item->id), $promotionItem->products) && !$default ) {
										echo'<option value="'.$item->id.'" data-tokens="selected" selected>'.__($item->name,'custom-promotion').'</option>';
									}else {
										echo'<option value="'.$item->id.'" >'.__($item->name,'custom-promotion').'</option>';
									}
								}
							}
				echo'		</select>
				</div>
				<div class="row mt-2">
						<button class="btn btn-success mr-2" name="editexcludedproduct" type="submit" value="'.$promotionItem->ID.'" >'.__('Zaktualizuj','custom-promotion').'</button>
						<button class="btn btn-danger mr-2" name="deleteexcludedproduct" type="submit" value="'.$promotionItem->ID.'" >'.__('Usuń','custom-promotion').'</button>
				</div>
				</div>
				<br>---------------------------------------------------------';
			}

		}
	}

	/**
	 * Display the pharmacies to be chosen by the client
	 *
	 * @since    1.0.0
	 * @param      array    $pharmacies    the pharmacies list.
	 * @param      array    $groups    the pharmacies list.
	 */

	private function displayInputSelectPharmacies($pharmacies=null,$groups=null){
		# Get the pharmacies existing in the database and show to the user to choose from for the promotion
		echo'<div class="input-pharmacies container clear-margin-left">';
		echo'	<div class="input-pharmacies__content-select row ml-0 mt-2">';
		echo'		<select name="customPharmacies[]" class="js-input-pharmacies selectpicker input-pharmacies__select" multiple  data-live-search="true" title="'.__('Szukaj (nazwie, adr. email)','custom-promotion').'" >';
					if ($pharmacies!=null && sizeof($pharmacies)>0) {
						//printing the existing pharmacies
						foreach ($pharmacies as $key => $item) {
							if ($item!=null) {
							$groupsNames='';

							if (sizeof($item->groups)>0) {
								foreach ($item->groups as $key => $name) {
									if ($name!=null) {
										$groupsNames=$groupsNames.','.$name->name;
									}
								}
							}
		echo'				<option value="'.$item->id.'" groupNames="'.substr($groupsNames, 1).'" data-tokens="'.$item->idAp.','.$item->company.','.$item->city.','.$item->street.','.$item->nip.','.$item->email.''.$groupsNames.'"> ID: '.$item->idAp.', '.$item->company.', '.$item->city.', '.$item->street.', NIP: '.$item->nip.' </option>';
							}
						}
					}
		echo'		</select>';
		if ($groups!=null && sizeof($groups)>0) {
			echo'	<select class="js-selected-group selectpicker input-pharmacies__select ml-2 " multiple  data-live-search="true" title="'.__('Wybierz grupę','custom-promotion').'" >';
				foreach ($groups as $key => $item) {
					echo'<option value="'.$item->id.'" data-tokens="'.$item->name.'">'.$item->name.'</option>';
				}
			echo'	</select>';
		}
		echo'	</div>';
		echo'	<div class="input-pharmacies__content-checbok ml-3">';
		echo'		<input type="checkbox" id="pharmacies" name="allPharmacies">';
		echo'		<label for="pharmacies">'.__('Wszystkie','custom-promotion').'</label>';
		echo'	</div>';
		echo'</div>';
	}

	/**
	 * Display the input of the promotion of checkout sum exceeded.
	 *
	 * @since    1.0.0
	 */

	private function displayInputPromotionPerCheckOutSum(){
		# code...
		echo'
		<div class="input-pharmacies-check-sum container clear-margin-left mt-2">
			<div class="row justify-content-start">
				<div class="col-md-auto">
					<div class="input-group">
						<input type="number" name="sumexceeded" placeholder="'.__('Wpisz sumę','custom-promotion').'">
						<div class="input-group-append">
							<span class="input-group-text">zł</span>
						</div>
					</div>
				</div>
				<div class="col-md-auto">
					<div class="input-group">
						<input type="number" name="percentageCheckSum" placeholder="'.__('Procent','custom-promotion').'">
						<div class="input-group-append">
							<span class="input-group-text">%</span>
						</div>
					</div>
				</div>
			</div>
			'.$this->displayDateTimePicker('checkSum').'
			<div class="row justify-content-start">
				</div>
				<div class="row justify-content-start">
				<div class="col-md-6 p-3">
					<input class="input-pharmacies-check-sum__button-send" type="submit" name="validatesumpromotion"
						value="'.__('Zatwierdź','custom-promotion').'">
				</div>
			</div>
		</div>
	  ';
	}

	/**
	 * Display the input of the promotion of buying certain number of product.
	 *
	 * @since    1.0.0
	 * @param      array    $dataProduct    the products list.
	 * @param      array    $giftProducts    the gift products list.
	 */

	private function displayInputPromotionByProductPurchased( $dataProduct=null,$giftProducts=null ){
		# code...
		echo'<div class=" container clear-margin-left">';
		echo'	<div class="row ml-0 mt-2">';
		echo'       <div class="col-md-auto p-0 pr-2">
						<div class="input-group">';
		echo'				<div class="promotion-admin-inputs__item">';
		echo'		<select name="productsPurchased[]" class="selectpicker" multiple  data-live-search="true" title="'.__('Wybierz produkt/y','custom-promotion').'">';
					if ($dataProduct!=null && sizeof($dataProduct)>0) {
						//printing the existing pharmacies
						foreach ($dataProduct as $key => $item) {
		echo'				<option value="'.$item->id.'" >'.$item->name.'</option>';
						}
					}
		echo'		</select>';
		echo'				</div>
		<div class="row col-md-auto">
				<select class="selectpicker ml-2" name="productGifted" data-live-search="true" title="'.__('Szukaj po nazwie produktu','custom-promotion').'">';
				if ($giftProducts!=null && sizeof($giftProducts)>0) {
					//printing the existing pharmacies
					foreach ($giftProducts as $key => $item) {
		echo'						<option value="'.$item->id.'" >'.$item->name.'</option>';
					}
				}
				echo'		</select>';
				echo'	</div>
						</div>
					</div>';
		echo'
					<div class="col-md-auto p-0 pr-2">
						<div class="input-group">
							<div class="promotion-admin-inputs__item">
								<select name="typePromotion" class="selectpicker js-type-promotion">
									<option value="product">'.__('Produkt extra','custom-promotion').'</option>
									<option value="percentage">'.__('Rabat procentowy','custom-promotion').'</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row justify-content-start  ml-0 mt-3">
					<div class="col-md-auto p-0 pr-3 ">
						<div class="input-group">
						<input name="numberProductPurchased" type="number" placeholder="'.__('Podaj ilość produktów','custom-promotion').'">
							<div class="input-group-append">
								<span class="input-group-text"> szt. </span>
							</div>
						</div>
					</div>
					<div class="col-md-auto p-0 pr-3 ">
						<div class="input-group">
								<input name="promotionProductPurchased" data-placeholder-values="{\'product\': \'Ilość\',\'percentage\':\'Wartość\'}" type="number" placeholder="">
								<div class="input-group-append">
									<span class="input-group-text">szt. lub %</span>
								</div>
						</div>
					</div>
				</div>
				'.$this->displayDateTimePicker('productPurchased').'
				<div class="row justify-content-start ml-0 mt-2">
					<div class="row ml-0 mt-2">
						<div class="col-md-auto ml-0 p-0">
							<div class="promotion-admin-inputs__item--send">
								<input type="submit" name="validatePromotionProduct" value="'.__('Zatwierdź','custom-promotion').'">
							</div>
						</div>
					</div>
				</div>
			</div>
		';
	}

	/**
	 * Display the input of the promotion of buying certain number of product from a certain category.
	 *
	 * @since    1.0.0
	 * @param      array    $dataCategory    the categories list.
	 * @param      array    $giftProducts    the gift products list.
	 */

	private function displayInputPromotionByCategoryPurchased( $dataCategory=null ,$giftProducts=null){
		# code...
		echo'<div class=" container clear-margin-left">
				<div class="promotion-admin-inputs row ml-0 mt-2 ">
					<div class="row col-md-auto">
						<select name="categoriesPurchased[]" class="selectpicker" multiple  data-live-search="true" title="'.__('Szukaj po nazwie','custom-promotion').'">';
							if ($dataCategory!=null && sizeof($dataCategory)>0) {
								//printing the existing pharmacies
								foreach ($dataCategory as $key => $item) {
		echo'						<option value="'.$item->id.'" >'.$item->name.'</option>';
								}
							}
		echo '
						</select>
						<select class="selectpicker ml-1 mr-1" name="productGiftedCategory" data-live-search="true" title="'.__('Szukaj po nazwie','custom-promotion').'">';
				if ($giftProducts!=null && sizeof($giftProducts)>0) {
					//printing the existing pharmacies
					foreach ($giftProducts as $key => $item) {
		echo'						<option value="'.$item->id.'" >'.$item->name.'</option>';
					}
				}
				echo'		</select>';
				echo'</div>
					<div class="promotion-admin-inputs__item">
						<select name="typePromotionCategory" class="selectpicker js-type-promotion">
							<option value="product">'.__('Produkt extra','custom-promotion').'</option>
							<option value="percentage">'.__('Rabat procentowy','custom-promotion').'</option>
						</select>
					</div>
				</div>
				<div class="promotion-admin-inputs row ml-0 mt-3">
				<div class="col-md-auto p-0 pr-3 ">
					<div class="input-group">
						<input name="numberCategoryPurchased" type="number" placeholder="'.__('Podaj ilość produktów','custom-promotion').'">
							<div class="input-group-append">
								<span class="input-group-text">szt.</span>
							</div>
						</div>
					</div>
					<div class="col-md-auto p-0 pr-3 ">
						<div class="input-group">
						<input name="promotionCategoryPurchased" data-placeholder-values="{\'product\': \'Ilość\',\'percentage\':\'Wartość\'}" type="number" placeholder="'.__('Ilość/Wartość','custom-promotion').'">
							<div class="input-group-append">
								<span class="input-group-text">szt. lub %</span>
							</div>
						</div>
					</div>
				</div>
				'.$this->displayDateTimePicker('categoryPurchased').'
				<div class="promotion-admin-inputs row ml-0 mt-3">
					<div class="promotion-admin-inputs__item--send">
						<input type="submit" name="validatePromotionCategory" value="'.__('Zatwierdź','custom-promotion').'">
					</div>
				</div>
			</div>'
		;
	}

	/**
	 * Display the input of the promotion list of prices per pharmacy.
	 *
	 * @since    1.0.0
	 */

	private function displayInputListOfPricesPerPharmacies(){
		# code...
		echo'
		<div class="promotion-admin-list-of-prices container ml-3">
			<div class="row">
				<span class="promotion-admin-list-of-prices__heading">'.__('Wgraj listę z pliku','custom-promotion').'</span>
			</div>
			<div class="row">
				<input name="listPrices" type="file" accept=".csv">
			</div>
			'.$this->displayDateTimePicker('listPrices').'
			<div class="row">
				<input type="submit" name="validateListPrices" value="'.__('Zatwierdź','custom-promotion').'">
			</div>
		</div>
		';
	}

	/**
	 * Display the pharmacies chosen for the given promotion.
	 *
	 * @since      1.0.0
	 * @param      array    $data       the pharmacies list selected in the promotion.
	 * @param      boolean  $default    true means all pharmacies were selected.
	 */

	private function displaySelectedPharmacies($default=true,$data=null){
		# Get the pharmacies existing in the database and show to the user to choose from for the promotion
		echo'<div>';
		if ($default) {
			# code...
			echo'	<div>';
			echo'		<span>'.__('Wszyskie','custom-promotion').'</span>';
			echo'	</div>';
		}else{
			echo'	<div>';
			echo'		<select class="selectpicker" multiple  data-live-search="true" title="'.__('Szukaj (nazwie, adr. email)','custom-promotion').'" >';
						if ($data!=null && sizeof($data)>0) {
							//printing the existing pharmacies
							foreach ($data as $key => $item) {
			echo'				<option idPharm="'.$item->id.'" data-tokens="'.$item->email.'">'.$item->name.'</option>';
							}
						}
			echo'		</select>';
			echo'	</div>';
		}
		echo'</div>';
	}

	/**
	 * Display the promotion of checkout sum exceeded.
	 *
	 * @since    1.0.0
	 * @param    object   $dataPromotion     the data of a certain promotion.
	 * @param    array    $dataPharmacies    the list of all pharmacies.
	 */

	private function displayPromotionPerCheckOutSum( $dataPromotion=null, $pharmacies=null, $groups=null ){
		# code...
		if ($dataPromotion!=null && sizeof($dataPromotion)>0) {
			# code...
			foreach ($dataPromotion as $key => $promotionItem) {
				# code...
				echo'<div class="container promotionperchecksum">';
				echo'<div class="row ">';
				if ($promotionItem->typePromotion==CHECKSUMPROMOTIONALL) {
					$this->displayEditSelectPharmacies($pharmacies,$promotionItem->pharmacies,true,$promotionItem->ID, $groups);
				}else {
					$this->displayEditSelectPharmacies($pharmacies,$promotionItem->pharmacies,false,$promotionItem->ID, $groups);
				}
				echo'
				</div>
				<div class="row">
				<div class="col-md-auto pl-0">
						<div class="input-group mt-2">
							<input type="number" name="sumexceeded'.$promotionItem->ID.'" value="'.$promotionItem->sum.'">
							<div class="input-group-append">
								<span class="input-group-text">zł</span>
							</div>
						</div>
						</div>
						<div class="col-md-auto pl-0">
						<div class="input-group mt-2">
							<input type="number" name="percentage'.$promotionItem->ID.'" value="'.$promotionItem->percentage.'">
							<div class="input-group-append">
								<span class="input-group-text">%</span>
							</div>
						</div>
						</div>
					</div>
					'.$this->displayDateTimePicker('editCheckSum'.$promotionItem->ID,$promotionItem->dateStart,$promotionItem->dateEnd).'
					<div class="row mt-2">
						<button class="btn btn-success mr-2" name="editsumpromotion" type="submit" value="'.$promotionItem->ID.'" >'.__('Zaktualizuj','custom-promotion').'</button>
						<button class="btn btn-danger mr-2" name="deletesumpromotion" type="submit" value="'.$promotionItem->ID.'" >'.__('Usuń','custom-promotion').'</button>
					</div>
					</div>
					<br>---------------------------------------------------------';
			}

		}
	}

	/**
	 * Display the list of prices per pharmacy.
	 *
	 * @since    1.0.0
	 * @param      object   $dataPromotion    the data of a certain promotion.
	 * @param      array    $pharmacies    the list of all pharmacies.
	 */

	private function displayListOfPricesPerPharmacies($dataPromotion=null, $pharmacies=null ,$groups=null){
		# code...
		if ($dataPromotion!=null && sizeof($dataPromotion)>0) {
			foreach ($dataPromotion as $key => $promotionItem) {
				echo'<div class"container">';
				echo'<div class"row">';
				if ($promotionItem->typePromotion==LISTPRICESALL) {
					$this->displayEditSelectPharmacies($pharmacies,$promotionItem->pharmacies,true,$promotionItem->ID, $groups);
				}else {
					$this->displayEditSelectPharmacies($pharmacies,$promotionItem->pharmacies,false,$promotionItem->ID, $groups);
				}
				echo'<h4 class="mt-2">'.__('Indywidalny cennik','custom-promotion').'</h4>';
				if(sizeof($promotionItem->prices)>0){
					echo'<p>
					<button class="btn btn-primary mt-2" type="button" data-toggle="collapse" data-target="#collapseListPrices'.$promotionItem->ID.'" aria-expanded="false" aria-controls="collapseListPrices'.$promotionItem->ID.'">
					'.__('Pokaż cennik','custom-promotion').'
					</button>
					</p>
					<div class="collapse" id="collapseListPrices'.$promotionItem->ID.'">
					<div class="card card-body">';
						echo'<table class="mb-2"><tr><td>SKU</td><td>'.__('Cena','custom-promotion').'</td></tr>';
					foreach ($promotionItem->prices as $key => $price) {
						echo'<tr><td>'.$price->sku.' </td><td>'.$price->price.'</td></tr>';
					}
					echo'</table>
					</div>
					</div>';
				}

				echo'<input class="mt-2" name="listPrices'.$promotionItem->ID.'" type="file" accept=".csv" />';
				echo''.$this->displayDateTimePicker('editListPrices'.$promotionItem->ID,$promotionItem->dateStart,$promotionItem->dateEnd).'
				<div class="row mt-2 ml-0">';
				echo'<button class="btn btn-success mr-2" name="editlistpricespromotion" type="submit" value="'.$promotionItem->ID.'" >'.__('Zaktualizuj','custom-promotion').'</button>';
				echo'<button class="btn btn-danger mr-2" name="deletelistpricespromotion" type="submit" value="'.$promotionItem->ID.'" >'.__('Usuń','custom-promotion').'</button>';
				echo'</div>';
				echo'</div>';
				echo'</div>';
				echo '<br>---------------------------------------------------------';
			}
		}
	}

	/**
	 * Display the input of the promotion of buying certain number of product.
	 *
	 * @since    1.0.0
	 * @param      array    $dataProduct    the products list.
	 * @param      array    $pharmacies     the list of all pharmacies.
	 * @param      array    $pharmacies     the list of all pharmacies.
	 * @param      array    $giftProducts   the list of products that can be gifted.
	 */

	private function displayPromotionByProductPurchased( $dataProduct=null, $pharmacies=null, $allProducts=null,$giftProducts=null,$groups=null){
		# code...
		if ($dataProduct!=null && sizeof($dataProduct)>0) {
			foreach ($dataProduct as $key => $groupItem) {

				echo'<div class="container promotionproductpurschased">';
				echo'	<div class="row">';
				if ($groupItem->typePromotion==PRODUCTPROMOTIONPRODUCTALL || $groupItem->typePromotion==PRODUCTPROMOTIONPERCENTAGEALL) {
					$this->displayEditSelectPharmacies($pharmacies,$groupItem->pharmacies,true,$groupItem->ID, $groups);
				}else {
					$this->displayEditSelectPharmacies($pharmacies,$groupItem->pharmacies,false,$groupItem->ID, $groups);
				}
				echo '</div>';
				echo'	<div class="row col-md-auto ml-n30 mt-2">
							<select class="selectpicker ml-0" name="productsEditPurchased'.$groupItem->ID.'[]" multiple  data-live-search="true" title="'.__('Szukaj po nazwie','custom-promotion').'">';
							if ($allProducts!=null && sizeof($allProducts)>0) {
								//printing the existing pharmacies
								foreach ($allProducts as $key => $item) {
									if (in_array(strval($item->id), $groupItem->products) && !$default ) {
										echo'<option value="'.$item->id.'" data-tokens="selected" selected>'.__($item->name,'custom-promotion').'</option>';
									}else {
										echo'<option value="'.$item->id.'" >'.__($item->name,'custom-promotion').'</option>';
									}
								}
							}
				echo'		</select>';
				echo'
							<select class="selectpicker ml-2" name="productGifted'.$groupItem->ID.'" data-live-search="true" title="'.__('Szukaj po nazwie','custom-promotion').'">';
							if ($giftProducts!=null && sizeof($giftProducts)>0) {
								//printing the existing pharmacies
								foreach ($giftProducts as $key => $item) {
									if (strval($item->id)==$groupItem->productGifted && !$default ) {
										echo'<option value="'.$item->id.'" data-tokens="selected" selected>'.__($item->name,'custom-promotion').'</option>';
									}else {
										echo'<option value="'.$item->id.'" >'.__($item->name,'custom-promotion').'</option>';
									}
								}
							}
				echo'		</select>';

				echo'		<select class="selectpicker ml-2 js-type-promotion" name="promotionProductSelectedEdit'.$groupItem->ID.'">';
				if ($groupItem->typePromotion==PRODUCTPROMOTIONPRODUCTALL||$groupItem->typePromotion==PRODUCTPROMOTIONPRODUCT) {
					echo'	<option value="product" selected>'.__('Produkt extra','custom-promotion').'</option>';
					echo'	<option value="percentage" >'.__('Rabat procentowy','custom-promotion').'</option>';
				}else {
					echo'	<option value="product" >'.__('Produkt extra','custom-promotion').'</option>';
					echo'	<option value="percentage" selected>'.__('Rabat procentowy','custom-promotion').'</option>';
				}
				echo'		</select>';
				echo'	</div>
				<div class="row">
				<div class="col-md-auto pl-0">
					<div class="input-group mt-2">
						<input name="editNumberProduct'.$groupItem->ID.'" type="number"	placeholder="'.__('Ilość produktów','custom-promotion').'" value="'.$groupItem->numberProducts.'">
						<div class="input-group-append">
							<span class="input-group-text">szt</span>
						</div>
					</div>
					</div>
					<div class="col-md-auto pl-0">
					<div class="input-group mt-2">
						<input name="editPromotionProduct'.$groupItem->ID.'" data-placeholder-values="{\'product\': \'Ilość\',\'percentage\':\'Wartość\'}" type="number"
						placeholder="'.__('Ilość/Wartość','custom-promotion').'" value="'.$groupItem->promotion.'">
						<div class="input-group-append">
							<span class="input-group-text">szt. lub %</span>
						</div>
					</div>
					</div>
				</div>
				'.$this->displayDateTimePicker('editProductPurchased'.$groupItem->ID,$groupItem->dateStart,$groupItem->dateEnd).'
				<div class="row">
					<div class="promotion-admin-inputs__item--button">
						<button class="btn btn-success mt-2 mr-2" name="editproductperchased" type="submit"
							value="'.$groupItem->ID.'">'.__('Zaktualizuj','custom-promotion').'</button>
					</div>
					<div class="promotion-admin-inputs__item--button">
					<button class="btn btn-danger mt-2 mr-2 js-confirmation-action" data-to-remove="to-remove-p-'.$groupItem->ID.'">'.__('Usuń','custom-promotion').'</button>
						<input type="submit" class="d-none to-remove-p-'.$groupItem->ID.'" value="'.$groupItem->ID.'" name="deleteproductperchased" />
					</div>
				</div>
				</div>
				<br>---------------------------------------------------------';
			}
		}
	}

	/**
	 * Display the promotion of buying certain number of product in a categorie.
	 *
	 * @since    1.0.0
	 * @param      array    $dataCategory    the categories list.
	 * @param      array    $pharmacies     the list of all pharmacies.
	 * @param      array    $allCategories     the list of all categories having ids of products.
	 * @param      array    $giftProducts    the gift products list.
	 */

	private function displayPromotionByCategoriePurchased( $dataCategory=null, $pharmacies=null, $allCategories=null ,$giftProducts=null,$groups=null){
		# code...
		if ($dataCategory!=null && sizeof($dataCategory)>0) {
			foreach ($dataCategory as $key => $groupItem) {
				echo'<div class="container">';
				echo'	<div class="row">';
				if ($groupItem->typePromotion==CATEGORYPROMOTIONPRODUCTALL || $groupItem->typePromotion==CATEGORYPROMOTIONPERCENTAGEALL) {
					$this->displayEditSelectPharmacies($pharmacies,$groupItem->pharmacies,true,$groupItem->ID, $groups);
				}else {
					$this->displayEditSelectPharmacies($pharmacies,$groupItem->pharmacies,false,$groupItem->ID, $groups);
				}

				echo'
				<div class="row col-md-auto">
				<select class="selectpicker ml-0" name="categoryEditPurchased'.$groupItem->ID.'[]"  multiple data-live-search="true" title="'.__('Szukaj po nazwie','custom-promotion').'">';
							if ($allCategories!=null && sizeof($allCategories)>0) {
								//printing the existing pharmacies
								foreach ($allCategories as $key => $item) {
									if (in_array(strval($item->id), $groupItem->categories) && !$default ) {
										echo'<option value="'.$item->id.'" data-tokens="selected" selected>'.__($item->name,'custom-promotion').'</option>';
									}else {
										echo'<option value="'.$item->id.'" >'.__($item->name,'custom-promotion').'</option>';
									}
								}
							}
				echo'		</select>';
				echo'
							<select class="selectpicker ml-2" name="productGiftedCategory'.$groupItem->ID.'" data-live-search="true" title="'.__('Szukaj po nazwie','custom-promotion').'">';
							if ($giftProducts!=null && sizeof($giftProducts)>0) {
								//printing the existing pharmacies
								foreach ($giftProducts as $key => $item) {
									if (strval($item->id)==$groupItem->productGifted && !$default ) {
										echo'<option value="'.$item->id.'" data-tokens="selected" selected>'.__($item->name,'custom-promotion').'</option>';
									}else {
										echo'<option value="'.$item->id.'" >'.__($item->name,'custom-promotion').'</option>';
									}
								}
							}
				echo'		</select>
							<select class="selectpicker ml-2 js-type-promotion" name="promotionCategoryEditSelected'.$groupItem->ID.'">';
				if ($groupItem->typePromotion==CATEGORYPROMOTIONPRODUCTALL||$groupItem->typePromotion==CATEGORYPROMOTIONPRODUCT) {
					echo'	<option value="product" selected>'.__('Produkt extra','custom-promotion').'</option>';
					echo'   <option value="percentage">'.__('Rabat procentowy','custom-promotion').'</option>';
				}else {
					echo'	<option value="product" >'.__('Produkt extra','custom-promotion').'</option>';
					echo'	<option value="percentage" selected>'.__('Rabat procentowy','custom-promotion').'</option>';
				}
				echo'		</select>
				</div>
				</div>
				<div class="row">
					<div class="col-md-auto pl-0">
						<div class="input-group mt-2">
							<input name="editNumberCategory'.$groupItem->ID.'" type="number"
								placeholder="'.__('Podaj ilość produktów','custom-promotion').'" value="'.$groupItem->numberProducts.'">
							<div class="input-group-append">
								<span class="input-group-text">szt.</span>
							</div>
						</div>
					</div>
					<div class="col-md-auto pl-0">
						<div class="input-group mt-2">
							<input name="editPromotionCategory'.$groupItem->ID.'" data-placeholder-values="{\'product\': \'Ilość\',\'percentage\':\'Wartość\'}" type="number"
								placeholder="'.__('Ilość/Wartość','custom-promotion').'" value="'.$groupItem->promotion.'">
							<div class="input-group-append">
								<span class="input-group-text">szt. lub %</span>
							</div>
						</div>
					</div>
				</div>
				'.$this->displayDateTimePicker('editCategoryPurchased'.$groupItem->ID,$groupItem->dateStart,$groupItem->dateEnd).'
				<div class="row">
					<div class="col-md-auto pl-0">
						<div class="promotion-admin-inputs__item--button">
							<button class="btn btn-success mt-2 mr-2" name="editcategoriepurchased" type="submit"
								value="'.$groupItem->ID.'">'.__('Zaktualizuj','custom-promotion').'</button>
						</div>
					</div>
					<div class="col-md-auto pl-0">
						<div class="promotion-admin-inputs__item--button">
							<button class="btn btn-danger mt-2 mr-2 js-confirmation-action" data-to-remove="to-remove-c-'.$groupItem->ID.'">'.__('Usuń','custom-promotion').'</button>
								<input type="submit" class="d-none to-remove-c-'.$groupItem->ID.'" value="'.$groupItem->ID.'" name="deletecategoriepurchased" />
						</div>
					</div>
				</div>
				</div>
				<br>---------------------------------------------------------';
			}
		}
	}

	/**
	 * Display the list of pharmacy groups.
	 *
	 * @since    1.0.0
	 * @param    array   $dataGroup    the list of groups submitted.
	 * @param    array   $pharmacies   the list of all pharmacies.
	 *
	 */

	private function displayListGroupPharmacies( $dataGroup=null, $pharmacies ){
		if ($dataGroup!=null && sizeof($dataGroup)>0) {
				foreach ($dataGroup as $key => $groupItem) {

					$this->displayEditSelectPharmacies($pharmacies->listPharmacies,$groupItem->pharmacies,false,$groupItem->id);
					echo'<div class="container"><div class="row mt-2">	<input name="groupName'.$groupItem->id.'" type="text" value="'.$groupItem->name.'"> </div>';
					echo'
					<div class="row">
					<button class="btn btn-success mt-2 mr-2" name="editgroupPharm" type="submit" value="'.$groupItem->id.'" >'.__('Zaktualizuj','custom-promotion').'</button>';
					echo'	<button class="btn btn-danger mt-2 mr-2" name="deletegroupPharm" type="submit" value="'.$groupItem->id.'" >'.__('usun','custom-promotion').'</button> </div></div>';

				}
		}
	}


/**
	 * Display the pharmacies that are selected.
	 *
	 * @since    1.0.0
	 * @param      array    $data    			   the all pharmacies list.
	 * @param      array    $pharmaciesSelected    the selected pharmacies list.
	 * @param      boolean  $default               true means all pharmacies are selected.
	 * @param      integer  $ID                    The ID of displayed list of pharmacies.
	 */

	private function displayEditSelectPharmacies($data=null,$pharmaciesSelected=null,$default=false,$ID='',$groups=null){
		# Get the pharmacies existing in the database and show to the user to choose from for the promotion
		echo'<h3 class="mt-2">Wybrane apteki</h3>';
		echo'<div class="container">';
		echo'	<div class="row ">';
		echo'		<select name="customPharmacies'.$ID.'[]" class="js-input-pharmacies selectpicker input-pharmacies__select" multiple  data-live-search="true" title="'.__('Szukaj (nazwie, adr. email)','custom-promotion').'" >';
					if ($data!=null && sizeof($data)>0) {
						//printing the existing pharmacies
						foreach ($data as $key => $item) {
							$groupsNames='';

							if (sizeof($item->groups)>0) {
								foreach ($item->groups as $key => $name) {
									if ($name!=null) {
										$groupsNames=$groupsNames.','.$name->name;
									}
								}
							}
							if (in_array(strval($item->id), $pharmaciesSelected)) {
								echo'<option value="'.$item->id.'" groupNames="'.substr($groupsNames, 1).'" data-tokens="'.$item->idAp.','.$item->company.','.$item->city.','.$item->street.','.$item->nip.','.$item->email.',selected" selected> ID: '.$item->idAp.', '.$item->company.', '.$item->city.', '.$item->street.', NIP: '.$item->nip.' </option>';
							}else {
								echo'<option value="'.$item->id.'" groupNames="'.substr($groupsNames, 1).'" data-tokens="'.$item->idAp.','.$item->company.','.$item->city.','.$item->street.','.$item->nip.','.$item->email.'" > ID: '.$item->idAp.', '.$item->company.', '.$item->city.', '.$item->street.', NIP: '.$item->nip.' </option>';
							}
						}
					}
		echo'		</select>';
		if ($groups!=null && sizeof($groups)>0) {
			echo'	<select class="js-selected-group selectpicker input-pharmacies__select ml-2 " multiple  data-live-search="true" title="'.__('Wybierz grupę','custom-promotion').'" >';
				foreach ($groups as $key => $item) {
					echo'<option value="'.$item->id.'" data-tokens="'.$item->name.'">'.$item->name.'</option>';
				}
			echo'	</select>';
		}
		echo'	</div>';

		echo'	<div class="input-pharmacies__content-checbok_wrap container mt-2">';
		if ($default) {
			echo'
			<div class="row d-flex align-items-baseline">
				<input type="checkbox" id="pharmacies-all-'.$ID.'" name="allPharmaciesedit'.$ID.'" checked>
				<label for="pharmacies-all-'.$ID.'"> '.__('Wszystkie','custom-promotion').'</label>
			</div>
			<div class="row d-flex align-items-baseline">
				<input type="checkbox" id="pharmacies-time-'.$ID.'" name="allPharmaciesedit'.$ID.'" checked>
				<label for="pharmacies-time-'.$ID.'"> '.__('Bierz pod uwagę datę utworzenia	promocji','custom-promotion').'</label>
				<button type="button" class="btn btn-xs btn-info ml-2 h-25" data-toggle="popover"
					title="'.__('Bierz pod uwagę datę utworzenia promocji','custom-promotion').'" data-placement="bottom"
					data-content="'.__('Zaznaczenie tej opcji spowoduje, że zarejestrowani użytkownicy po dacie dodania tej promocji nie będą automatycznie do niej dodawani.','custom-promotion').'"
					data-html="true"><i>i</i></button>
			</div>
			';

		}else {

			echo'<div class="row d-flex align-items-baseline"><input type="checkbox" id="pharmacies-all-'.$ID.'" name="allPharmaciesedit'.$ID.'" >';
			echo'		<label for="pharmacies-all-'.$ID.'">  '.__('Wszystkie','custom-promotion').'</label></div>';
		}
		echo'	</div>';
		echo'</div>';
	}

	/**
	 * Display the input of the product exclution.
	 *
	 * @since    1.0.0
	 * @param      array    $data    the products list.
	 */

	private function displayInputProductExclution( $data=null ){
		echo'<div class=" container clear-margin-left">';
		echo'	<div class="row ml-0 mt-2">';
		echo'   <div class="col-md-auto p-0 pr-2">
				<div class="input-group">';
		echo'	<div class="promotion-admin-inputs__item">';
		echo'		<select name="productsexcluded[]" class="selectpicker" multiple  data-live-search="true" title="'.__('Wybierz produkt/y','custom-promotion').'">';
					if ($data->products!=null && sizeof($data->products)>0) {
						//printing the existing pharmacies
						foreach ($data->products as $key => $item) {
		echo'				<option value="'.$item->id.'" >'.$item->name.'</option>';
						}
					}
		echo'		</select>';
		echo'	</div>
				</div>
				</div>';

		echo'	<div class="row ml-0 mt-2">
					<div class="col-md-auto ml-0 p-0">
						<div class="promotion-admin-inputs__item--send">
							<input type="submit" name="validateProductExclution" value="'.__('Zatwierdź','custom-promotion').'">
						</div>
					</div>
				</div>
			</div>
		</div>
		';
	}
}