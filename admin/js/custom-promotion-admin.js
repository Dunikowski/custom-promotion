
	(function( $ ) {
		'use strict';
		$(function(){
			const group = $('.wp-admin select.js-selected-group');
			let pharmacies = [];
			const urlParams = new URLSearchParams(window.location.search);
			const tab_name = urlParams.get('tab');

			const setingsGroupForListPromotion = () => {
				const colection_pharmacies = $('.wp-admin select.js-input-pharmacies');
				colection_pharmacies.each(function() {
					const options = $(this).find('>option');
					let group_name = Array.prototype.filter
					.call( options, function(i){	
					
						return $(i).attr('groupnames') !== '' && $(i).is(':selected');
					})
					.map(function(i) {

						return $(i).attr('groupnames');
					});

					group_name = [...new Set(group_name)];

					const group = $(this).closest('.row').find('select.js-selected-group');
					const option_group = group.find('>option');
					let select_group_values = Array.prototype.filter
					.call(option_group,function(i){
					
						return group_name.includes($(i).attr('data-tokens'));
					})
					.map(function(i) {

						return $(i).val();
					});

					select_group_values = [...new Set(select_group_values)];
					group.val(select_group_values);
					
				});
			}

			const setingsSesionStorage = (group_name=[],group_item=null) => {
				const session = sessionStorage.getItem("235125Fq3cvv2345v13_group_name");
				const data_object = {};
				data_object[group_item] = group_name.join(',');

				if(!session) {
					sessionStorage.setItem("235125Fq3cvv2345v13_group_name",JSON.stringify(data_object));
					
				} else {
					
					if(group_name.join(',') !== JSON.parse(session)[group_item] && group_name.length > 0) {
						sessionStorage.setItem("235125Fq3cvv2345v13_group_name",JSON.stringify(data_object));

					} 
				}
			}

			const pharmaciesItems = (pharmacies=[],group_name=[],remove_group=false) => {

				if(pharmacies.length > 0 ) {
					let pharmacies_items = [];
					pharmacies.find('option').each(function(){
						const _this = $(this);
						let is_there_group_name = false;

						if(_this.attr('groupnames') !== '') {
							const temporary_array_group_name = _this.attr('groupnames').split(',');

							temporary_array_group_name.forEach( i => {
								if(group_name.includes(i)) {
									is_there_group_name = true;
								}
							});
						}
						
						if(!remove_group) {

							if(is_there_group_name || ($(this).attr('groupnames') === ''  && $(this).is(':selected'))) {
								pharmacies_items.push($(this).val());
							}	
						} else {
							if(is_there_group_name ) {
								pharmacies_items.push($(this).val());
							}	
						}
					});	
				
					return pharmacies_items;
				} else {
					return [];
				}
			}

			const dropdownToggleText = (dropdown_toggle=[],pharmacies_name=[]) => {

				if(dropdown_toggle.length === 0) return;

				if(pharmacies_name.length > 0) {

					dropdown_toggle
					.prop('title',pharmacies_name.join(", "))
					.find('.filter-option-inner-inner')
					.text(pharmacies_name.join(", "));

				} else {

					dropdown_toggle
					.prop('title','Szukaj (nazwie, adr. email)')
					.find('.filter-option-inner-inner')
					.text('Szukaj (nazwie, adr. email)');
				}
			}

			if(tab_name == 'listPromotion') {
				setingsGroupForListPromotion();
			}

			if (group.length > 0) {

				group.each(function(i) {
						$(this).attr("data-group",`${i}`);
				});

				group.on('change', function(e) {
					
					e.stopPropagation();
					pharmacies = $(this).closest('.row').find('select.js-input-pharmacies');
					let group_name;
					
					const _val = $(this).val();
					const group_item = $(this).data('group');
					const _o = group.find('option');
					const dropdown_toggle = $(this).closest('.row').find('.js-input-pharmacies >.dropdown-toggle');
					const _po = pharmacies.find('option');
					const session = sessionStorage.getItem("235125Fq3cvv2345v13_group_name");

					group_name = Array.prototype.filter
					.call(_o, function(i){	

						return _val.includes($(i).val());
					})
					.map(function(i) {

						return $(i).attr('data-tokens');
					});

					/**
					 * Removing repeating values by new Set
					*/
					group_name = [...new Set(group_name)];

					setingsSesionStorage(group_name,group_item);
		
					const pharmacies_items = pharmaciesItems(pharmacies,group_name,false,session);
				
					if(pharmacies_items.length > 0) {

					/**
					 * Setting pharmacies from selected group + elier clicked items
					*/
					
						pharmacies.val(pharmacies_items);
					
						const pharmacies_name = Array.prototype.filter
						.call(_po, function(i){	
	
							return pharmacies_items.includes($(i).val());
						})
						.map(function(i) {
	
							return $(i).text();
						});

						dropdownToggleText(dropdown_toggle,pharmacies_name);

					} else if(pharmacies_items.length === 0 && session ) {
						const name = JSON.parse(session)[group_item];
						const removing_group_name = [name];
						const pharmacies_items_to_remove = pharmaciesItems(pharmacies,removing_group_name,true);
						const pharmacies_items = Array.prototype.filter
							.call(_po, function(i){	
								return  !pharmacies_items_to_remove.includes($(i).val());
							})
							.filter(function(i) {
	
								return $(i).is(':selected');
							})
							.map(function(i) {
	
								return $(i).val();
							});

							pharmacies.val(pharmacies_items);

							const pharmacies_name = Array.prototype.filter
							.call(_po, function(i){	
		
								return pharmacies_items.includes($(i).val());
							})
							.map(function(i) {
		
								return $(i).text();
							});

							if(pharmacies_name.length > 0) {
								dropdownToggleText(dropdown_toggle,pharmacies_name);
							} else {
								dropdownToggleText(dropdown_toggle);
							}

							sessionStorage.removeItem("235125Fq3cvv2345v13_group_name");
					} else { 

						pharmacies.val([]);
						dropdownToggleText(dropdown_toggle);
				    }
				});
			}
		});

		$(function(){
			const _select = $('.wp-admin select.js-type-promotion');
			const product_promiotion = _select.closest('.container').find('[name="promotionProductPurchased"]');
			const category_product_promiotion = _select.closest('.container').find('[name="promotionCategoryPurchased"]');
			const product_promiotion_2 = _select.closest('.container').find('[name="editPromotionProduct2"]');
			const category_product_promiotion_3 = _select.closest('.container').find('[name="editPromotionCategory3"]');

			const callEventChangeOnSelects = function(_this, element) {
				_this.on('change', function(e) {
					e.stopPropagation();
					changesPlaceholder($(this),element);
				});
				changesPlaceholder(_select,element);
			}
			
			const changesPlaceholder = function (_this,_i) {
				const j = _i.next('.input-group-append').find('.input-group-text');
				const _val = _this.val();
				if(_i.length > 0 && _val !== undefined) {
					let y = _i.data('placeholderValues');
					y = y.replaceAll("'","\"");
					_i.attr('placeholder',JSON.parse(y)[_val]);
				}
				
				if(j.length > 0 && _val !== undefined) {

					switch(_val) {
						case 'product':
							j.text('szt.')
							break;
						case 'percentage':
							j.text('%')
							break;	
					}
				}
			}

			if (_select.length > 0) {
				
				_select.each(function () {
					const _this = $(this);
					if(_this.attr('name') === "typePromotion" ) {
						callEventChangeOnSelects(_this, product_promiotion);
					}

					if(_this.attr('name') === "typePromotionCategory" ) {
						callEventChangeOnSelects(_this, category_product_promiotion);
					}
					
					if(_this.attr('name') === "promotionProductSelectedEdit2" ) {
						callEventChangeOnSelects(_this, product_promiotion_2);
					}

					if(_this.attr('name') === "promotionCategoryEditSelected3" ) {
						callEventChangeOnSelects(_this, category_product_promiotion_3);
					}
				})
			}
		})

		$(function(){

			const _remove = $('.wp-admin .js-confirmation-action');
			const _modal = $('.wp-admin .js-modal-for-removing');
			const _close_modal = _modal.find('.close');
			const btn_to_remove = _modal.find('.btn-to-remove');

			_remove.on('click',function(e) {
				e.stopPropagation();
				e.preventDefault();
				_modal.removeClass('d-none');
				const to_remove = $(this).data('toRemove');
				btn_to_remove.attr('data-to-remove',to_remove);
			})

			_close_modal.on('click',function(e){
				e.stopPropagation();
				$(this).closest('.js-modal-for-removing').addClass('d-none');
			})

			btn_to_remove.on('click', function(e){
				e.stopPropagation();
				const i = $(this).data('toRemove');
				_remove.next(`.${i}`).click();	
			})
		})

		
	})( jQuery );

	jQuery(function () {
		$('[data-toggle="popover"]').popover()
	})
