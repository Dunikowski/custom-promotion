<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://agencjacumulus.pl/o-nas/
 * @since      1.0.0
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Custom_Promotion
 * @subpackage Custom_Promotion/admin
 * @author     Cumulus Team <mustapha@agencjacumulus.pl>
 */
class Custom_Promotion_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The Menu settings of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $settingsMenu    The current version of this plugin.
	 */
	private $settingsMenu;

	/**
	 * The Display controller of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $displayController    The display controller of this plugin.
	 */
	private $displayController;

	/**
	 * The database controller for admin of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dbControllerAdmin    The database controller for admin of this plugin.
	 */
	private $dbControllerAdmin;

	/**
	 * The request handler of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $requestHandler  the request handler of this plugin.
	 */
	private $requestHandler;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $dbControllerAdmin ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->dbControllerAdmin = $dbControllerAdmin;

		$this->load_dependencies();
	
		$this->displayController = new Custom_Promotion_Display_Controller( $plugin_name, $version , $this->dbControllerAdmin );
		$this->requestHandler = new Custom_Promotion_Requests_Handler( $this->plugin_name, $this->version, $this->displayController , $this->dbControllerAdmin );
		
	}

	private function load_dependencies() {

		/**
		 * The class responsible for settings and menu.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) .  'includes/class-custom-promotion-display-controller.php';
		/**
		 * The class responsible for handling the Post and Get requests.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-promotion-requests-handler.php';
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Promotion_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Promotion_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */


		// Check screen base and page
		$screen = get_current_screen();

		if ( 'toplevel_page_custom_promotion' === $screen->base && $_GET['page'] ==='custom_promotion' ) {
			wp_enqueue_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
			wp_enqueue_style( 'bootstrap-datetimepicker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css');
			wp_enqueue_style( 'bootstrap-select', 'https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css');
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/custom-promotion-admin.css', array(), $this->version, 'all' );
		}

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Promotion_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Promotion_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		 
		// Check screen base and page
		$screen = get_current_screen();

		if ( 'toplevel_page_custom_promotion' === $screen->base && $_GET['page'] ==='custom_promotion' ) {
			wp_enqueue_script( 'bootstrapjs-jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js', array('jquery'), '', true );
			wp_enqueue_script( 'bootstrapjs-datetimepicker', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js', array('jquery'), '', true );
			wp_enqueue_script( 'bootstrapjs-popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), '', true );
			wp_enqueue_script( 'bootstrapjs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array('jquery'), '', true );
			wp_enqueue_script( 'bootstrap-selectjs', 'https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js', array('jquery'), '', true );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/custom-promotion-admin.js', array(  ), $this->version, 'all' );
		}
	}

	/**
	 * Returns the menu settings private object for use in other object.
	 *
	 * @since    1.0.0
	 */
	public function getMainPage(){
		# returning the object settings
		return $this->requestHandler;
	}
 
}